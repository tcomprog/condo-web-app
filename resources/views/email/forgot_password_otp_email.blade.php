@extends("includes.email_theme")
@section("content")

<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;">
    Thank you for choosing {{env("APP_NAME")}}. Use the following OTP to complete your change password procedures. OTP is valid for 10 minutes
</p>

<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;margin-top:10px;">
    <strong>Your OTP is: {{$otp}}</strong> 
 </p>


@endsection