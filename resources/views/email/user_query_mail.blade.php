@extends("includes.email_theme")
@section("content")

@php
$name = "CondoApp Admin";    
@endphp

<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;">
    You have new query from user send from CondoApp home screen.
</p>

<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;margin-top:10px;">
    <strong>First name: </strong> {{$firstName}}
 </p>
<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;">
    <strong>Last name: </strong> {{$lastName}}
 </p>
<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;">
    <strong>Sender email: </strong> {{$email}}
 </p>
<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;">
    <strong>Phone no: </strong> {{$phoneNumber}}
 </p>
<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;">
    <strong>Subject: </strong> {{$subject}}
 </p>
<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;margin-top:10px;">
    <strong>Message: </strong> {{$messages}} 
 </p>


@endsection