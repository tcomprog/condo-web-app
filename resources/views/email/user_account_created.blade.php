@extends("includes.email_theme")
@section("content")

<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px">
   {{$messages}}
</p>

<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;margin-top:20px">
   <strong>Society Name:</strong> <span>{{$society_name}}</span>
</p>

<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;margin-top:5px">
   <strong>Add By:</strong> <span>{{$created_by}}</span>
</p>

<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;margin-top:5px">
   <strong>Flat Details:</strong> <span>{{$flat_details}}</span>
</p>

<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;margin-top:5px">
   <strong>Username:</strong> <span>{{$username}}</span>
</p>

<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;margin-top:5px">
   <strong>Password:</strong> <span>{{$password}}</span>
</p>

@endsection