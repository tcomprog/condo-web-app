@extends("includes.email_theme")
@section("content")

<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;margin-top:10px;">
    Your account request has been accepted and you can now use the system. Your account credentials are mentioned below.
   <br><br> <b>Username: </b>{{$email}}
    <br> <b>Password: </b>{{$password}}
</p>


@endsection