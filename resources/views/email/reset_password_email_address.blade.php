@php
    $url = env("WEBSITE_LINK")."change-password/".$id."/".$token;
@endphp

@extends("includes.email_theme")
@section("content")

<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;margin-top:10px;">
    If you`ve lost your password or wish to reset it, use the link below to get started.
</p>


<p style="text-align: center">
    <span class="es-button-border" style="border-style:solid;border-color:#2980D9;background:#2980D9;border-width:0px;display:inline-block;border-radius:5px;width:auto;margin-top:15px;">
      <a href="{!! $url !!}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none !important;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;padding:10px 40px;display:inline-block;background:#2980D9;border-radius:5px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;letter-spacing:0;mso-padding-alt:0;mso-border-alt:10px solid #2980D9">Reset Password</a>
    </span>
</p>

<p style="Margin:0;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;letter-spacing:0;color:#666666;font-size:14px;margin-top:25px;margin-bottom:8px;">
    If you have problems, please paste the below URL into your web browser.
</p>

<a href="" style="color:blue;margin-top:10px">{!! $url !!}</a>

@endsection