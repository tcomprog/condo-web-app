<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('dashboard')}}">
        <div class="sidebar-brand-icon">
            <img class="rounded-pill" width="35px" src="{{asset('images/logo.png')}}" alt="">
        </div>
        <div class="sidebar-brand-text mx-3">Condo<sup class="ps-1">App</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    @php
        $route = Route::currentRouteName();
    @endphp

    <!-- Nav Item - Dashboard -->
    <li class="nav-item @if ($route == 'dashboard') {{"active"}} @endif">
        <a class="nav-link" href="{{route('dashboard')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        User Section
    </div>

    @php
      $society_user = DB::select("SELECT COUNT(id) as total FROM `tbl_society_user` WHERE status = 'pending' AND society_id = '25';");
    @endphp

    <li class="nav-item @if ($route == 'user-list' || $route == "add-user" || $route == "pending-user" ||
    $route == "user-profile" || $route == "update-user-page" || $route == "send-user-email") {{"active"}} @endif">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#userDropDown"
            aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-user"></i>
            <span>Society members</span>
        </a>
        <div id="userDropDown" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{route('add-user')}}">Add member</a>
                <a class="collapse-item" href="{{route('user-list')}}">Member list</a>
                <a class="collapse-item" href="{{route('pending-user')}}">Pending requests 
                    @if($society_user[0]->total > 0)
                     <span class="badge text-bg-primary">{{$society_user[0]->total}}</span>
                    @endif
                </a>
            </div>
        </div>
    </li>


    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Admin Section
    </div>

    <li class="nav-item @if ($route == 'addevents' || $route == 'event-list' || $route == 'eventDetails') {{"active"}} @endif">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#eventsSection"
            aria-expanded="true" aria-controls="eventsSection">
            <i class="fas fa-fw fa-calendar"></i>
            <span>Events</span>
        </a>
        <div id="eventsSection" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{route('addevents')}}">Add event</a>
                <a class="collapse-item" href="{{route('event-list')}}">Event list</a>
            </div>
        </div>
    </li>

    <li class="nav-item @if ($route == 'add-voting' || $route == 'voting-list' || $route == "voting-details") {{"active"}} @endif">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
            aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-bars"></i>
            <span>Voting</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{route('add-voting')}}">Add Voting</a>
                <a class="collapse-item" href="{{route('voting-list')}}">Voting List</a>
            </div>
        </div>
    </li>

    {{-- <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
            aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>Pages</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Login Screens:</h6>
                <a class="collapse-item" href="login.html">Login</a>
                <a class="collapse-item" href="register.html">Register</a>
            </div>
        </div>
    </li> --}}

    <li class="nav-item @if ($route == 'request-list' || $route == 'request-details') {{"active"}} @endif">
        <a href="{{url("request-list")}}" class="nav-link">
            <i class="fas fa-fw fa-clipboard"></i>
            <span>Member Request</span></a>
    </li>

    <li class="nav-item @if ($route == 'petition-list') {{"active"}} @endif">
        <a href="{{url("petition-list")}}" class="nav-link">
            <i class="fas fa-fw fa-file"></i>
            <span>File Petition</span></a>
    </li>

    <li class="nav-item @if ($route == 'send-alert-page') {{"active"}} @endif">
        <a href="{{url("send-alert-page")}}" class="nav-link">
            <i class="fas fa-fw fa-bell"></i>
            <span>Send Alert</span></a>
    </li>

    <li class="nav-item @if ($route == 'society-policy') {{"active"}} @endif">
        <a href="{{url("society-policy")}}" class="nav-link">
            <i class="fas fa-fw fa-keyboard"></i>
            <span>Society Policy</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>