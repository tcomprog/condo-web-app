<script>

    // **************** Models Variables Section *****************
    var option = {backdrop:'static',focus:true};
    const changePassword = new bootstrap.Modal(document.getElementById('changePasswordModal'),option);


    // =============== CHANGE PASSWORD =============== 
     $("#changePassword").on('click',function(){
         changePassword.show();
     });


    //  ############ Close All Models #############
    $(".btn-close").on('click',function(){
        changePassword.hide();
    });


    // ---------------- Button Events ----------------------
    $("#savePasswordChangeBtn").on("click",function(){
        let oldPassword = $("#oldPassword");
        let newPassword = $("#newPassword");
        let repeatPassword = $("#repeatPassword");
        let error = 0;

        let errorClassName = 'class="text-danger ps-2 errorMessage" style="font-size:14px;"';

        $(".errorMessage").remove();
        $(".inputField").removeClass("border-danger");

        if(isEmpty(oldPassword.val())){
            oldPassword.addClass("border-danger");
            oldPassword.after('<span '+errorClassName+'>Enter old password</span>');
            error = 1;
        }

        if(isEmpty(newPassword.val())){
            newPassword.addClass("border-danger");
            newPassword.after('<span '+errorClassName+'>Enter new password</span>');
            error = 1;
        }

        if(isEmpty(repeatPassword.val())){
            repeatPassword.addClass("border-danger");
            repeatPassword.after('<span '+errorClassName+'>Enter repeat password</span>');
            error = 1;
        }

        if(!isEmpty(newPassword.val())){
        let em = isPassword(newPassword.val());
        if(em != null){
            newPassword.addClass("border-danger");
            newPassword.after('<span '+errorClassName+'>'+em+'</span>');
            error = 1;
        }
        }

        if(!isEmpty(repeatPassword.val()) && newPassword.val() != repeatPassword.val()){
           repeatPassword.addClass("border-danger");
           repeatPassword.after('<span '+errorClassName+'>Password does not match</span>');
           error = 1;
        }

        if(error == 0){
            showLoader();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
            type: "POST",
            url: '{{url("changeAdminPassword")}}',
            data: {
              oldPassword:oldPassword.val(),
              newPassword:newPassword.val(),
              rePassword:repeatPassword.val()
            },
            dataType: 'json',
            success: function (data) {
                hideLoader();
                if(data.status == "success"){
                    showMessage(data.message,0);
                    $(".inputField").removeClass("border-danger");
                    $("#registerUserForm").trigger("reset");
                    oldPassword.val('');
                    newPassword.val('');
                    repeatPassword.val('');
                }else{
                    showMessage(data.message,1);
                }
            },
            error: function (data) {
                hideLoader();
                showMessage("Something went wrong try again",1);
            }
           });

        }
        else{
            setTimeout(() => {
                $(".errorMessage").remove();
                $(".inputField").removeClass("border-danger"); 
            }, 3000);
        }

    });

    // Submit Privacy Policy Section
       document.querySelector( '#submitForm' )?.addEventListener( 'click', () => {
           const editorData = editor.getData();
           if(!isEmpty(editorData)){
             $("#privacyPolicyForm").submit();
           }
           else{
              showMessage("Fill the form correctly!",1);
           }
        } );


    $(document).on('click','.delete-event-row',function() {
        let eventID = $(this).data('id');
       // showLoader();

       $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
            type: "POST",
            url: '{{url("deleteEvent")}}',
            data: {
              id:eventID,
            },
            dataType: 'json',
            success: function (data) {
                hideLoader();
                console.log(data)
                if(data.status == "success"){
                    showMessage(data.message,0);
                    $("#event-row-"+eventID).remove();
                }else{
                    showMessage(data.message,1);
                }
            },
            error: function (data) {
                hideLoader();
                showMessage("Something went wrong try again",1);
            }
           });

    });



    // Add voting new option
    $(document).on('click','#addVotingOption',function(){
       let option_no = $("#number_of_option").val();
       option_no = option_no < 0 ? 0 : option_no;
       option_no = parseInt(option_no);
       option_no += 1;
      
       if(option_no <= 5){
       let option = `<div class="row mt-3" id="option_row_${option_no}">
            <label class="form-label">Option ${option_no}<sup class="text-danger">*</sup></label>
            <div class="col-md-6 col-sm-10">
                <input name="options[]" required maxlength="200" type="text" class="form-control">
             </div>
             <div class="col-md-6 col-sm-2 pt-1">
                <button type="button" data-rowno="${option_no}" class="btn btn-outline-danger ms-2 btn-sm delete-voting-row"><span class="fa fa-trash"></span></button>
             </div>
            </div>`;
        
        $("#votingOptionContainer").append(option);
        $("#number_of_option").val(option_no);
        $("#votingSubmitFormBtn").show();
       }else{
         showMessage("You can`t add more than five options",1)
       }
    });

    // Delete voting row
    $(document).on('click','.delete-voting-row',function(){
       let option_no = $("#number_of_option").val();
       option_no = option_no < 0 ? 0 : option_no;
       option_no = parseInt(option_no);
       option_no -= 1;

       if(option_no <= 0){
         $("#votingSubmitFormBtn").hide()
       }

        let row_no = $(this).data("rowno");
        $("#option_row_"+row_no).remove();
        $("#number_of_option").val(option_no);
    });
    

</script>