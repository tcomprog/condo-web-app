<!-- Bootstrap core JavaScript-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.7.0.js"></script>
<script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('js/sb-admin-2.min.js')}}"></script>


<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
<script src="{{asset('js/common.js')}}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.13.5/js/dataTables.bootstrap5.min.js"></script>

<script>
    let table = new DataTable('#myTable', {
      responsive: true,
      scrollX: true
    });

    $(document).ready(function(){
        $('input[type=search]').attr('autocomplete', 'off');
    });
</script>

@if (isset($_GET["status"]) && isset($_GET['message']))
  <script>
    showMessage("{{$_GET['message']}}","{{$_GET['toastType']}}")
  </script>
@endif

@php
  $route = Route::currentRouteName();
@endphp

@if($route == 'addevents')

<script src="{{asset('js/jquery-ui.js')}}"></script>

<script>
    $( function() {
      let dateToday = new Date(); 

    $( "#eventDatePicker" ).datepicker({
      altFormat: "dd-mm-Y",
      timeFormat: 'hh:mm',
      minDate:dateToday,
      inline: true

    });
  } );
</script>
@endif