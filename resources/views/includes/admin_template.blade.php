@include('includes.header')
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
         @include('includes.sidebar')
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                 @include('includes.nav_bar')
                <!-- End of Topbar -->
                <div class="container-fluid">

                   {{-- MAIN CONTENT HERE --}}
                    @yield('content')

                </div>

            </div>
        </div>
    </div>

    @include('includes.footer')
    @include('includes.model')
    @include('includes.admin_action')

</body>

</html>