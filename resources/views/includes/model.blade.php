{{-- =============== CHANGE PASSWORD =================== --}}
<div class="modal fade" id="changePasswordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="changePasswordModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content border-left-primary">
        <div class="modal-header">
          <h1 class="modal-title fs-5" id="staticBackdropLabel">Change Password</h1>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body px-5">
          
            <div class="mb-3">
                <label for="formGroupExampleInput" class="form-label ps-1">Old Password</label>
                <input type="password" class="form-control rounded-pill inputField" id="oldPassword">
            </div>

            <div class="mb-3">
                <label for="formGroupExampleInput" class="form-label ps-1">New Password</label>
                <input type="password" class="form-control rounded-pill inputField" id="newPassword">
            </div>

            <div class="mb-3">
                <label for="formGroupExampleInput" class="form-label ps-1">Repeat Password</label>
                <input type="password" class="form-control rounded-pill inputField" id="repeatPassword">
            </div>

            <div class="text-end py-2">
                <button id="savePasswordChangeBtn" type="button" class="btn btn-outline-primary rounded-pill">Save Changes</button>
            </div>

        </div>
      </div>
    </div>
  </div>