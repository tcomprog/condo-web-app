<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <meta name="description" content="Introducing Condo App: Your All-in-One Solution for Seamless Society Management. Join the Future of Community Living Today!">
    <meta name="keywords" content="Condo, CondoApp, Condo App, Society Management, Community, Community communication, Video calling, Audio calling">
    <meta name="author" content="Abdul Basit">
   
    @include('includes.favicon')


    <link rel="stylesheet" href="{{asset('css/home_screen.css?111')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body>


   {{-- ************ Header Section Start ************ --}}
    <header class="headerContainer">
           <img class="headerBackImg img-fluid" src="{{asset('images/home/top_image.jpg')}}" alt="Top Header Image">
           <div class="contentContaine d-flex flex-column justify-content-between">

            <nav class="navbar navbar-expand-lg bg-transparent px-5 mt-3">
                <div class="container-fluid">
                  <a class="navbar-brand" href="{{url("/")}}">
                    <img src="{{asset('images/home/logo.png')}}" alt="Logo" width="50" height="50" class="d-inline-block align-text-top">
                  </a>

                  <button class="navbar-toggler bg-white" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>

                  <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                    <ul class="navbar-nav gap-4">
                      <li class="nav-item cust-active">
                        <a class="nav-link text-white" aria-current="page" href="#communctionSection">Community</a>
                      </li>

                      <li class="nav-item cust-active">
                        <a class="nav-link nav-cus-link" href="#featureSection">Features</a>
                      </li>

                      <li class="nav-item cust-active">
                        <a class="nav-link nav-cus-link" href="#contactUsSection">Send Query</a>
                      </li>

                      <li class="nav-item cust-active">
                        <a class="nav-link nav-cus-link" href="#gallerySection">Gallery</a>
                      </li>

                      <li class="nav-item cust-active">
                        <a class="nav-link nav-cus-link" href="#newContSection">News & Stories</a>
                      </li>

                      <li class="nav-item cust-active">
                        <a class="nav-link nav-cus-link" href="#contactUsSection">Contact US</a>
                      </li>
                     
                    </ul>
                  </div>
                </div>
              </nav>
            
               <div class="centerContentContainer d-flex align-items-center flex-column mx-auto">
                   <h1 class="text-white text-center mb-4 mt-5 LatoBold">JOIN OUR CONDO COMMUNITY</h1>
                   <p class="text-white text-center px-5 lh-lg RalewayRegular">Introducing Condo App: Your All-in-One Solution for Seamless Society Management. Join the Future of Community Living Today!"</p>
                   <img class="header-group-image" src="{{asset('images/home/top_group_img.png')}}" alt="Group Image">
               </div>

           </div>

           <div class="wrapper">
            <div><span class="dot"></span></div>
            <div><span class="dot"></span></div>
            <div><span class="dot"></span></div>
            <div><span class="dot"></span></div>
            <div><span class="dot"></span></div>
            <div><span class="dot"></span></div>
            <div><span class="dot"></span></div>
            <div><span class="dot"></span></div>
            <div><span class="dot"></span></div>
            <div><span class="dot"></span></div>
            <div><span class="dot"></span></div>
            <div><span class="dot"></span></div>
            <div><span class="dot"></span></div>
            <div><span class="dot"></span></div>
            <div><span class="dot"></span></div>
          </div>
    </header>


    {{-- ************ Second Tab Start ************ --}}
    <section class="container-fluid" id="communctionSection">
        <div class="row">
           <div class="col-6 px-0">
              <img class="w-100 object-fit-cover" style="max-height: 700px;" src="{{asset('images/home/side-one.jpg')}}"/>
           </div>

           <div class="col-6 pt-5 d-flex flex-column align-items-center px-0" style="background: #D4E6E8;">
              <h1 class="text-center px-5 lh-base LatoRegular building-txt mb-2">Building Strong <br> Communities Together</h1>
              <h3 class="engagment-text">Engagement Events</h3>
              <p class="text-center px-5 mx-5 lh-base mt-3 slide1-text fs-5">Regularly organized events foster a sense of belonging among residents, providing opportunities for social interaction and community bonding</p>

              <div class="container-fluid h-100 d-flex align-items-end">
                <div class="row">
                   <div class="col px-0">
                     <img src="{{asset('images/home/slide-1/img-1.jpg')}}" class="img-fluid imageZoomIn"/>
                   </div>
                   <div class="col px-0">
                     <img src="{{asset('images/home/slide-1/img-2.jpg')}}" class="img-fluid imageZoomIn"/>
                   </div>
                   <div class="col px-0">
                     <img src="{{asset('images/home/slide-1/img-3.jpg')}}" class="img-fluid imageZoomIn"/>
                   </div>
                   <div class="col px-0">
                     <img src="{{asset('images/home/slide-1/img-4.jpg')}}" class="img-fluid imageZoomIn"/>
                   </div>
                </div>
              </div>

           </div>

        </div>
    </section>


    {{-- ************** Image Show Section Start ************* --}}
    <section class="imageGalleryContainer">

      <section class="container mb-5" id="featureSection">
          <div class="row">
            <div class="col-6 d-flex flex-column justify-content-center ps-4 pt-5">
                <h1 class="enhancedTitleTxt pe-3 pt-5">Enhanced Connectivity <br> within Your Community</h1>
                <p class="mt-4 enhancedDesTxt fs-5">With our application, connecting with fellow community members has never been easier. Whether you're reaching out to neighbors or engaging with property owners, our platform facilitates seamless communication and interaction. Stay informed, stay connected, and build stronger bonds within your community today</p>
            </div>
            <div class="col-1"></div>
            <div class="col-5">
               <img class="img-fluid" src="{{asset("images/home/mobile_in_hands.png")}}" alt="">
            </div>
          </div>
      </section>

      <section class="container">
          <div class="row">
            <div class="col-1"></div>
            <div class="col-10 px-5">
                <h1 class="enhancedTitleTxt text-center">Streamlined Community <br> Management Portal</h1>
                <p class="mt-4 enhancedDesTxt fs-5 text-center px-5">Simplify your administrative tasks and empower effective community management with our dedicated portal. From creating and managing societies to overseeing member interactions and handling administrative duties, our platform offers a comprehensive suite of tools to streamline your workflow. Take control of your community management responsibilities with ease and efficiency.</p>
            </div>
            <div class="col-1"></div>
          </div>
      </section>

      <section class="container mt-5">
        <img class="img-fluid" src="{{asset('images/home/dashboard_img.png')}}" alt="">
     </section>


     <section class="container mt-5" id="gallerySection">
        <div class="row">
            <div class="col">
              <img class="img-fluid" src="{{asset("images/home/image_gallery.png")}}" alt="">
            </div>
        </div>
     </section>


     <section class="container mt-5">
        <div class="row">
          <div class="col">
            <img class="img-fluid" src="{{asset("images/home/user_group_images.png")}}" alt="">
          </div>
        </div>
     </section>

     <section class="container" id="newContSection">
        <div class="row">
            <div class="col-1"></div>
            <div class="col-10 px-5">
                <h1 class="enhancedTitleTxt text-center">News & Stories</h1>
                <p class="mt-4 enhancedDesTxt fs-5 text-center px-5">Stay informed and inspired with our vibrant News & Stories section. Explore the latest updates and captivating tales from within your community, keeping you connected and engaged like never before.</p>
            </div>
            <div class="col-1"></div>
        </div>
     </section>


     <section class="container my-5">
        <div class="row">

           <div class="col-4">
            <div class="card bg-transparent" style="border: none !important;">
                <img src="{{asset("images/home/cards/card_1.jpg")}}" class="card-img-top rounded" alt="...">
                <div class="card-body customCards mx-3 position-relative">

                  <div class="intro-banner-vdo-play-btn pinkBg">
                    <span class="ripple pinkBg"></span>
                    <span class="ripple pinkBg"></span>
                    <span class="ripple pinkBg"></span>
                  </div>

                    <h6 class="px-2 cardTitleTxt">Community Spotlight: Celebrating Neighborly Acts</h6>
                    <p class="mt-3 cardDesTxt">Discover heartwarming stories of kindness and generosity within our community, showcasing the incredible spirit of unity and compassion among residents</p>
                </div>
            </div>
           </div>

           <div class="col-4">
            <div class="card bg-transparent" style="border: none !important;">
                <img src="{{asset("images/home/cards/card_2.jpg")}}" class="card-img-top rounded" alt="...">
                <div class="card-body customCards mx-3 position-relative">
                  <div class="intro-banner-vdo-play-btn pinkBg">
                    <span class="ripple pinkBg"></span>
                    <span class="ripple pinkBg"></span>
                    <span class="ripple pinkBg"></span>
                  </div>

                    <h6 class="px-2 cardTitleTxt">Exclusive Event Recap: Building Bonds Beyond Borders</h6>
                    <p class="mt-3 cardDesTxt">Relive the excitement and camaraderie of our recent community event, where neighbors came together to forge new connections and create lasting memories.</p>
                </div>
            </div>
           </div>

           <div class="col-4">
            <div class="card bg-transparent" style="border: none !important;">
              
                <img src="{{asset("images/home/cards/card_3.jpg")}}" class="card-img-top rounded" alt="...">
                <div class="card-body customCards mx-3">
                    <h6 class="px-2 cardTitleTxt">Inspirational Member Feature: Making a Difference</h6>
                    <p class="mt-3 cardDesTxt">Meet one of our outstanding community members who is actively contributing to positive change, inspiring others with their dedication and commitment to building a better neighborhood.</p>
                </div>
            </div>
           </div>

        </div>
     </section>


     <section class="container mt-2">
      <div class="row">
          <div class="col-1"></div>
          <div class="col-10 px-5">
              <h1 class="enhancedTitleTxt text-center">Capturing Moments of Togetherness</h1>
              <p class="mt-4 enhancedDesTxt fs-5 text-center px-5">Immerse yourself in a visual journey through our community's vibrant moments, where laughter, friendship, and unity are beautifully captured in every photograph</p>
          </div>
          <div class="col-1"></div>
      </div>
     </section>

     <section class="container my-4">
       <div class="row">
           <div class="col">
              <img style="max-height: 300px;" class="w-100 object-fit-cover rounded imageZoomIn zoom-in-out-box" src="{{asset("images/home/shows/tab_1.jpg")}}" />
              <img style="max-height: 300px;" class="w-100 object-fit-cover rounded mt-3 imageZoomIn zoom-in-out-box" src="{{asset("images/home/shows/tab_2.jpg")}}" />
           </div>
           <div class="col">
              <img style="max-height: 610px;" class="w-100 object-fit-cover rounded imageZoomIn" src="{{asset("images/home/shows/center_img.jpg")}}" />
           </div>
           <div class="col">
            <img style="max-height: 300px;" class="w-100 object-fit-cover rounded imageZoomIn" src="{{asset("images/home/shows/tab_3.jpg")}}" />
            <img style="max-height: 300px;" class="w-100 object-fit-cover rounded mt-3 imageZoomIn" src="{{asset("images/home/shows/tab_4.jpg")}}" />
         </div>
       </div>

       <div class="row">
          <div class="col d-flex gap-2 py-5 justify-content-center">
             <div class="dotsDesign" style="background: #252B5C;"></div>
             <div class="dotsDesign"></div>
             <div class="dotsDesign"></div>
             <div class="dotsDesign"></div>
          </div>
       </div>
     </section>

    </section>


    {{-- **************** App QR code Section Start **************** --}}
    <section>
       <img class="img-fluid" src="{{asset('images/home/mobile_qr.jpg')}}" alt="">
    </section>


    {{-- ************ Footer Section Start ************ --}}
    <footer id="contactUsSection">
        <div class="d-flex flex-column" style="background:#EBF3F3;">
            <div class="footerdesign"></div>

            <div class="container z-1 w-100 px-5 pt-5">

                <div class="row">
                  <div class="col d-flex flex-column align-items-center">
                     <h1 class="text-center footerTitle mb-2">Let’s talk about</h1>
                     <p class="text-center fs-6 footersubTitle">Introducing Condo App: Your All-in-One Solution for Seamless Society Management. Join the Future of Community Living Today!"</p>
                  </div>
                </div>

                <div class="row">
                  <div id="messageContainer" class="alert alert-success" style="display:none;" role="alert">
                    A simple success alert—check it out!
                  </div>
                </div>

                 <div class="row pb-5 pt-4">

                     <div class="col-4 pr-0">
                         <div class="infoCard">

                          <div class="intro-banner-vdo-play-btn pinkBg">
                            <span class="ripple pinkBg"></span>
                            <span class="ripple pinkBg"></span>
                            <span class="ripple pinkBg"></span>
                          </div>
                          

                            <div class="outerCircle">
                               <div class="innerCircle"></div>
                            </div>
                            <div class="w-100 h-100 z-1 d-flex px-4 py-4 flex-column">
                               <h2 class="informationTxt">Contact Information</h2>
                               <p class="cardSubTxt fs-6">Say something to start a live chat!</p>

                               <div class="d-flex flex-column gap-5 justify-content-center h-100">
                               <div class="d-flex gap-4 align-items-center">
                                   <i class="fa-solid fa-phone-volume custIcons"></i>
                                   <p class="fs-6 mb-0 contactTxt">+1012 3456 789</p>
                               </div>

                               <div class="d-flex gap-4 align-items-center">
                                   <i class="fa-solid fa-envelope custIcons"></i>
                                   <p class="fs-6 mb-0 contactTxt">demo@gmail.com</p>
                               </div>

                               <div class="d-flex gap-4 align-items-center">
                                   <i class="fa-solid fa-location-dot custIcons"></i>
                                   <p class="fs-6 mb-0 contactTxt">132 Dartmouth Street Boston, Massachusetts 02156 United States</p>
                               </div>
                              </div>

                            </div>
                         </div>
                     </div>

                     <div class="col-8">
                         <div class="container">

                          <div class="row g-5">
                            <div class="col-2"></div>
                            <div class="col-5 inputGroup">
                              <label for="firstName" class="labelText fs-6">First Name<sup class="text-danger">*</sup></label>
                              <input id="firstName" type="text" maxlength="60" class="customerInput">
                            </div>
                            <div class="col-5 inputGroup">
                              <label for="lastName" class="labelText fs-6">Last Name</label>
                              <input id="lastName" maxlength="60" type="text" class="customerInput">
                            </div> 
                          </div>

                          <div class="row g-5 mt-5">
                            <div class="col-2 mt-0"></div>
                            <div class="col-5 inputGroup mt-0">
                              <label for="userEmail" class="labelText fs-6">Email<sup class="text-danger">*</sup></label>
                              <input id="userEmail" type="email" maxlength="60" class="customerInput">
                            </div>
                            <div class="col-5 inputGroup mt-0">
                              <label for="phoneNumber" class="labelText fs-6">Phone Number</label>
                              <input id="phoneNumber" maxlength="20" type="text" class="customerInput">
                            </div>
                          </div>

                          <div class="row g-5 mt-5">
                            <div class="col-2 mt-0"></div>
                            <div class="col-10 inputGroup mt-0">
                              <label for="userSubject" class="labelText fs-6">Subject<sup class="text-danger">*</sup></label>
                              <input id="userSubject" type="text" maxlength="100" class="customerInput">
                            </div>
                          </div>

                          <div class="row g-5 mt-5">
                            <div class="col-2 mt-0"></div>
                            <div class="col-10 inputGroup mt-0">
                              <label for="userMessage" class="labelText fs-6">Message<sup class="text-danger">*</sup></label>
                              <input id="userMessage" type="text" maxlength="1000" class="customerInput" placeholder="Write your message..">
                            </div>
                          </div>

                          <div class="row">
                            <div class="col d-flex mt-5 justify-content-end align-items-center gap-2">

                              <div id="loader" class="spinner-border text-success" style="display: none;" role="status">
                                 <span class="visually-hidden">Loading...</span>
                              </div>

                                <button id="submitContactForm" class="btn btn-success btn-lg customBtn px-4">Send Message</button>
                            </div>
                          </div>

                         </div>
                     </div>

                 </div>
            </div>
        </div>


        <div class="hero_area">
          <svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28" preserveAspectRatio="none" shape-rendering="auto">
              <defs>
                  <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z" />
              </defs>
              <g class="parallax">
                  <use xlink:href="#gentle-wave" x="48" y="0" fill="rgba(255,255,255,0.7)" />
                  <use xlink:href="#gentle-wave" x="48" y="3" fill="rgba(255,255,255,0.5)" />
                  <use xlink:href="#gentle-wave" x="48" y="5" fill="rgba(255,255,255,0.3)" />
                  <use xlink:href="#gentle-wave" x="48" y="7" fill="#B7D6D9" />
              </g>
          </svg>
          <p class="copyright-txt py-3 fs-6">Copyrights reserved </p>
      </div>
     
    </footer>


    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <script src="{{asset('js/home_screen.js')}}"></script>
    <script src="https://kit.fontawesome.com/28542ede70.js" crossorigin="anonymous"></script>




  <script>
    $(document).on("click","#submitContactForm",function(){
      hideMessage();
      let firstName = $("#firstName").val();
      let lastName = $("#lastName").val();
      let userEmail = $("#userEmail").val();
      let phoneNumber = $("#phoneNumber").val();
      let userSubject = $("#userSubject").val();
      let userMessage = $("#userMessage").val();

      if(!isEmpty(firstName) && !isEmpty(userEmail) && !isEmpty(userSubject) && !isEmpty(userMessage)){

          if(isEmail(userEmail)){
              $("#loader").show();
              $(this).prop("disabled", true);

             $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
             });

              $.ajax({
                  url : "{{ url('contact_us') }}",
                  data : {
                    firstName:firstName,
                    lastName:lastName,
                    email:userEmail,
                    phoneNumber:phoneNumber,
                    subject:userSubject,
                    message:userMessage
                  },
                  type : 'POST',
                  dataType : 'json',
                  success : function(result){
                    $("#loader").hide();
                    $(this).removeAttr('disabled');

                    if(result.status == 'success'){
                       $("#firstName").val('');
                       $("#lastName").val('');
                       $("#userEmail").val('');
                       $("#phoneNumber").val('');
                       $("#userSubject").val('');
                       $("#userMessage").val('');
                       showMessage("Query submitted successfully!",1);
                    }
                    else{
                      showMessage("Somthing went wrong try again");
                    }                     
                  }
              });
          }
          else{
            showMessage("Enter valid email address!");
          }
       }
        else{
            showMessage("Please fill the form correctly!");
        }
    });
  </script>




  </body>
</html>