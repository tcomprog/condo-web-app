<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    @include('includes.favicon')

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="{{asset('css/response_page.css')}}" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body>

	<div id="notfound">
		<div class="notfound">
			
            <div style="margin-bottom:30px">
                <img width="300px" src="{{asset('images/response_image.png')}}" alt="">
            </div>

            @if ($status == 'error')
               <h2 style="color:red">Link expired</h2>
            @elseif ($status == 'success')
               <h2>You email has been verified successfully</h2>
            @endif
			
            <a href="{!! env('WEBSITE_LINK') !!}" class="go_homeBtn">Go Home</a>

		</div>
	</div>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
