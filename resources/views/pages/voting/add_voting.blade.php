@extends('includes.admin_template')
@section('content')

<div class="row g-3 bg-white my-3 rounded border-left-primary py-4 px-5 mx-2 mb-4 shadows">
 
    <form id="votingFormSub" method="POST" action="{{route('addVoting')}}">
        <h4>Add new voting</h4>

        <input id="number_of_option" type="hidden" name="number_op" value="0">

        @csrf
        <div class="col-12 mt-4 px-0">
          <label class="form-label">Voting question<sup class="text-danger">*</sup></label>
          <input name="question" required maxlength="400" type="text" class="form-control">
        </div>

        <div id="votingOptionContainer" class="mt-4">
            
        </div>

        <button id="addVotingOption" type="button" class="btn btn-outline-info mt-4">Add option</button>
        <button id="votingSubmitFormBtn" type="submit" class="btn btn-outline-success ml-3 mt-4" style="display:none">Submit</button>

    </form>

</div>

@endsection