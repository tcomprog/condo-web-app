@extends('includes.admin_template')
@section('content')

<style>
    .tbody, td, tfoot, th, thead, tr{
        border-style: none !important;
    }
    div.dataTables_wrapper div.dataTables_length select{
        width: 80px !important;
    }
    td{
        padding-top: 10px !important;
        padding-bottom: 10px !important;
    }
    .pagination > li{
        padding: 0px !important;
        padding-top: 10px;
    }
</style>

<div class="g-3 bg-white my-3 rounded border-left-primary py-4 px-5 mx-2 mb-4 shadows">

   @if($voting->status == 0)
     <a class="btn btn-outline-success ms-2 btn-sm" href="{{url("change-status")."/".$voting->id."/1"}}">
        <span class="fa fa-check me-1"></span>
        Accept poll
     </a>

     <a class="btn btn-outline-secondary ms-2 btn-sm" href="{{url("change-status")."/".$voting->id."/3"}}">
        <span class="fa fa-minus me-1"></span>
        Reject poll
     </a>
   @endif


   @if($voting->status == 1)
    <a class="btn btn-outline-success ms-2 btn-sm" href="{{url("complete-poll")."/".$voting->id}}">
        <span class="fa fa-check me-1"></span>
        Complete poll
    </a>
    @endif

    <a class="btn btn-outline-danger ms-2 btn-sm" href="{{url("delete-poll")."/".$voting->id}}">
        <span class="fa fa-trash me-1"></span>
        Delete poll
    </a>
    
    
    <div class="row g-2 mt-3">
        <div class="col-md-12 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1" style="height: 95%">
                <p class="fw-bold mb-0">Poll Question</p>
                <p class="mb-0">{{$voting->question}}</p>
            </div>
        </div>
    </div>

    <div class="row g-2 mt-1">
        <div class="col-md-12 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1" style="height: 95%">
                <p class="fw-bold mb-0">Status</p>
                @if($voting->status == 0)
                    <p class="mb-0 text-info">Pending</p>
                @elseif($voting->status == 1)
                    <p class="mb-0 text-success">Active</p>
                @elseif($voting->status == 2)
                    <p class="mb-0 text-warning">Complete</p>
                @elseif($voting->status == 3)
                    <p class="mb-0 text-danger">Reject</p>
                @endif
            </div>
        </div>
    </div>

    <div class="row g-2 mt-1">
        <div class="col-md-12 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1 pb-3" style="height: 95%">
                <p class="fw-bold mb-0">Options</p>
                @foreach($options as $ind => $op)
                    <p class="mb-0">{{$ind+1}} - {{$op["title"]}} - <span class="text-primary">(votes: {{$op['votes']}})</span></p>
                @endforeach
            </div>
        </div>
    </div>

    @if($total_votes > 0)
    <div class="row g-2 mt-1">
        <div class="col-md-12 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1 pb-3" style="height: 95%">
                <p class="fw-bold mb-0">Percentages</p>
                @foreach($options as $ind => $op)
                    <p class="mb-0">{{$ind+1}} - {{$op["title"]}} - <span class="text-primary">@if($total_votes > 0) {{floor($op['votes'] / $total_votes * 100)}}% @else 0% @endif</span></p>
                @endforeach
            </div>
        </div>
    </div>
    @endif

    @if($total_votes > 0)
    <div class="row mt-3">
        <div class="col-12">
            <div class="chart-pie">
                <canvas id="myPieChart"></canvas>
            </div>
        </div>
    </div>
    @endif


  @if(count($voter_list) > 0) 
   <div class="mt-4">
    <table id="myTable" class="table table-striped dataTable" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Voted for</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($voter_list as $row)
                <tr>
                    <td width="30%">{{$row->name}}</td>
                    <td width="30%">{{$row->email}}</td>
                    <td width="30%">{{$row->title}}</td>
                    <td width="15%">{{date('d-m-Y',strtotime($row->created_at))}}</td>
                    <td width="10%" class="text-nowrap">
                        <a href="{{url("user-profile")."/".$row->id}}" href="" class="btn btn-outline-success btn-sm" title="View user profile">
                            <span class="fa fa-eye"></span>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
  </div>
  @endif


</div>


@if($total_votes > 0)
<script src="{{asset('vendor/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('js/demo/chart-area-demo.js')}}"></script>
<script src="{{asset('js/demo/chart-bar-demo.js')}}"></script>
<script src="{{asset('js/demo/chart-pie-demo.js')}}"></script>

<script>
var ctx = document.getElementById("myPieChart");
let requests = @json($grap_votes);
let tags = @json($graph_tags);
let graph_colors = @json($graph_colors);

var myPieChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: tags,
    datasets: [{
      data: requests,
      backgroundColor: graph_colors,
      hoverBackgroundColor: graph_colors,
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
    },
    legend: {
      display: true
    },
    cutoutPercentage: 80,
  },
});
</script>
@endif

@endsection