@extends('includes.admin_template')
@section('content')

<div class="row g-3 bg-white my-3 rounded border-left-primary py-4 px-5 mx-2 mb-4 shadows">
    <h4 class="text-dark">{{$event->name}}</h4>

    <div class="row mt-3">
        <div class="col-12 col-md-4">
            @if(!empty($event->image))
            <img width="180" class="img-thumbnail" src="{{asset('upload/event/'.$event->image)}}" alt="">
            @else
            <img width="180" class="img-thumbnail" src="{{asset('images/no_image_2.jpg')}}" alt="">
            @endif

            <div class="mt-1">
                <a href="{{url("addevents")."/".$event->id}}" style="height:33px" class="btn btn-outline-danger btn-sm px-2" title="Edit">
                    <span class="fa fa-pen pe-1"></span> Edit
                </a>
            </div>
        </div>

        <div class="col-12 col-md-8">
            
            <div class="row g-2">
                <div class="col-md-6 col-sm-12">
                    <div class="bg-body-secondary rounded px-3 py-1">
                        <p class="fw-bold mb-0">Event date</p>
                        <p class="mb-0">{{date('d/m/Y',strtotime($event->event_date))}}</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="bg-body-secondary rounded px-3 py-1">
                        <p class="fw-bold mb-0">Event time</p>
                        <p class="mb-0">{{$event->eventTime}}</p>
                    </div>
                </div>
            </div>

            <div class="row g-2 mt-2">
                <div class="col-md-6 col-sm-12">
                    <div class="bg-body-secondary rounded px-3 py-1">
                        <p class="fw-bold mb-0">Created at</p>
                        <p class="mb-0">{{date('d/m/Y',strtotime($event->created_at))}}</p>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <div class="bg-body-secondary rounded px-3 py-1">
                        <p class="fw-bold mb-0">Updated at</p>
                        <p class="mb-0">{{date('d/m/Y',strtotime($event->updated_at))}}</p>
                    </div>
                </div>
            </div>

            <div class="row g-2 mt-2">
                <div class="col-12">
                    <div class="bg-body-secondary rounded px-3 py-1">
                        <p class="fw-bold mb-0">Short description</p>
                        <p class="mb-0">{{$event->short_desc}}</p>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class='mt-3 border-bottom border-secondary pb-2'>
        <p class="fw-bold mb-0">Description</p>
        {!! htmlspecialchars_decode($event->des) !!}
    </div>
   
</div>

@endsection