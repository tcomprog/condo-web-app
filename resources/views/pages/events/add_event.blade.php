@extends('includes.admin_template')
@section('content')

<style>
    .ck-editor__editable_inline {
      min-height: 220px;
    }
</style>

@php
    $user = Auth::user();
@endphp

<form enctype="multipart/form-data" method="post" action="{{route('saveEvent')}}" class="row g-3 bg-white my-3 rounded border-left-primary py-4 px-5 mx-2 mb-4" style="box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 12px;">
    @csrf
    <h4>Add event</h4>

    @isset($event)
      <input type="hidden" name="eventID" value="{{$event->id}}">
    @endisset

    <div class="col-md-4">
      <label class="form-label">Name<sup class="text-danger">*</sup></label>
      <input maxlength="255" required name="name" maxlength="150" type="text" class="form-control @error('name') {{'border-danger'}} @enderror" value="@isset($event){{$event->name}}@endisset">
      @error('name')
        <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>

    <div class="col-md-4">
      <label class="form-label">Date<sup class="text-danger">*</sup></label>
      <input id='eventDatePicker' required name="eventdate"  type="text" class="form-control @error('eventdate') {{'border-danger'}} @enderror" value="@isset($event){{date('d/m/Y',strtotime($event->event_date))}}@endisset">
      @error('eventdate')
        <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>

    <div class="col-md-4">
      <label class="form-label">Time<sup class="text-danger">*</sup></label>
      <input id='eventTimePicker' required name="eventTime"  type="time" class="form-control @error('eventTime') {{'border-danger'}} @enderror" value="@isset($event){{$event->eventTime.':00'}}@endisset">
      @error('eventTime')
        <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>

    <div class="col-md-12">
      <label class="form-label">Short Description<sup class="text-danger">*</sup></label>
      <input maxlength="255" required id='shortDes' name="shortDes"  type="text" class="form-control @error('shortDes') {{'border-danger'}} @enderror" value="@isset($event){{$event->short_desc}}@endisset">
      @error('shortDes')
        <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>


    <div class="col-md-12">
      <label class="form-label">Description</label>
      <textarea id="editor" name="description" style="width: 100%;height:220px" class="@error('description') {{'border-danger'}} @enderror"></textarea>
      @error('description')
       <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>


    <div class="col-md-12">
        <label class="form-label">Event Image</label>
        <input name="event_image" type="file" class="form-control @error('event_image') border-danger @enderror">
        @error('event_image')
         <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
        @enderror
    </div>

    @isset($event)
    <div>
      <img width="130" class="img-thumbnail" src="{{asset('upload/event/'.$event->image)}}" />
    </div>
    @endisset

    <div class="col-12 pt-3">
      <button type="submit" class="btn btn-outline-primary w-auto">@if(isset($event)) Update @else Add @endif Event</button>
    </div>
  </form>

  <script src="{{asset('vendor/ckeditor5/build/ckeditor.js')}}"></script>
  @if(isset($event))
   <script>
        let data =  `<?php echo htmlspecialchars_decode($event->des); ?>`;
   </script>
  @else
  <script>
    let data = '';
  </script>
  @endif

  <script>
      let editor;
      ClassicEditor
      .create( document.querySelector( '#editor' ) )
      .then( newEditor => {
          editor = newEditor;
          editor.setData(data)
      } )
      .catch( error => {
          console.error( error );
      } );
  </script>

@endsection

