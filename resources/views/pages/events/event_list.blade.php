@extends('includes.admin_template')
@section('content')

@php
    $user = Auth::user();
@endphp

<style>
    .tbody, td, tfoot, th, thead, tr{
        border-style: none !important;
    }
    div.dataTables_wrapper div.dataTables_length select{
        width: 80px !important;
    }
    td{
        padding-top: 10px !important;
        padding-bottom: 10px !important;
    }
    .pagination > li{
        padding: 0px !important;
        padding-top: 10px;
    }
</style>

<div class="g-3 bg-white my-3 rounded border-left-primary py-4 px-4 mx-2 mb-4 shodows">

  
   <div class="mt-4">
    {{-- <table id="myTable" class="stripe"> --}}
    <table id="myTable" class="table table-striped dataTable" style="width:100%">
        <thead>
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Date/time</th>
                <th>Created on</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($events as $row)
               
                <tr id="event-row-{{$row->id}}">
                    <td>
                        <img height="40px"  src="@if(empty($row->image))
                        {{asset('images/no_image_2.jpg')}}
                        @else
                        {{asset('upload/event/'.$row->image)}}
                         @endif"/>

                    </td>
                    <td>{{$row->name}}</td>
                    <td>{{date('d-m-Y',strtotime($row->event_date))}} : {{$row->eventTime}}</td>
                    <td>{{date('d-m-Y',strtotime($row->created_at))}}</td>
                    <td class="text-nowrap">
                     
                        <a href="{{url("eventDetails")."/".$row->id}}" class="btn btn-outline-success btn-sm" title="View details">
                            <span class="fa fa-eye"></span>
                        </a>

                        <button type="button" data-id='{{$row->id}}' class="btn btn-outline-danger btn-sm delete-event-row" title="Delete">
                            <span class="fa fa-trash"></span>
                        </button>

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
  </div>

</div>


@endsection