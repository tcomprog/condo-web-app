@extends('includes.admin_template')
@section('content')

@php
    $user = Auth::user();
   
@endphp

<form enctype="multipart/form-data" method="post" action="{{route('addNewUser')}}" class="row g-3 bg-white my-3 rounded border-left-primary py-4 px-5 mx-2 mb-4" style="box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 12px;">
    @csrf
    <div class="col-md-6">
      <label class="form-label">Email<sup class="text-danger">*</sup></label>
      <input name="email" required maxlength="100" type="email" class="form-control @error('email') {{'border-danger'}} @enderror">
      @error('email')
        <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>

    <div class="col-md-6">
      <label class="form-label">Name<sup class="text-danger">*</sup></label>
      <input name="name" required maxlength="200" type="text" class="form-control @error('name') {{'border-danger'}} @enderror">
      @error('name')
        <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>

    <div class="col-md-6">
      <label class="form-label">Mobile No<sup class="text-danger">*</sup></label>
      <input name="mobile" required maxlength="20" type="text" class="form-control @error('mobile') {{'border-danger'}} @enderror">
      @error('mobile')
        <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>

    <div class="col-md-6">
      <label class="form-label">Flat Details <sup class="text-danger">*</sup></label>
      <input name="flat_details" required maxlength="200" type="text" class="form-control  @error('flat_details') {{'border-danger'}} @enderror">
      @error('flat_details')
       <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>

    <div class="col-md-6">
      <label class="form-label">User Type <sup class="text-danger">*</sup></label>
      <select name="user_type" required class="form-select @error('user_type') {{'border-danger'}} @enderror" aria-label="Default select example">
        <option value="1">Owner</option>
        <option value="2">Renter</option>
      </select>
      @error('user_type')
       <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>

    <div class="col-md-6">
        <label class="form-label">Profile Image</label>
        <input name="profile_image" type="file" class="form-control @error('profile_image') border-danger @enderror">
        @error('profile_image')
         <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-12">
      <button type="submit" class="btn btn-primary">Add User</button>
    </div>
  </form>

@endsection
