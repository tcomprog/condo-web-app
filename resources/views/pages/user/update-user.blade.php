@extends('includes.admin_template')
@section('content')

<form enctype="multipart/form-data" method="post" action="{{route('updateUser')}}" class="row g-3 bg-white my-3 rounded border-left-primary py-4 px-5 mx-2 mb-4" style="box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 12px;">
    @csrf
    <div class="col-md-6">
      <label class="form-label">Email<sup class="text-danger">*</sup></label>
      <input name="email" required maxlength="100" type="email" class="form-control @error('email') {{'border-danger'}} @enderror" value="{{$user->email}}">
      @error('email')
        <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>

    <input type="hidden" name="id" value="{{$user->id}}">

    <div class="col-md-6">
      <label class="form-label">Name<sup class="text-danger">*</sup></label>
      <input name="name" required maxlength="200" type="text" class="form-control @error('name') {{'border-danger'}} @enderror" value="{{$user->name}}">
      @error('name')
        <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>

    <div class="col-md-6">
      <label class="form-label">Mobile No<sup class="text-danger">*</sup></label>
      <input name="mobile" required maxlength="20" type="text" class="form-control @error('mobile') {{'border-danger'}} @enderror" value="{{$user->mobile}}">
      @error('mobile')
        <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>

    <div class="col-md-6">
      <label class="form-label">Flat Details <sup class="text-danger">*</sup></label>
      <input name="flat_details" required maxlength="200" type="text" class="form-control  @error('flat_details') {{'border-danger'}} @enderror" value="{{$user->flat_details}}">
      @error('flat_details')
       <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>

    <div class="col-md-6">
      <label class="form-label">User Type <sup class="text-danger">*</sup></label>
      <select name="user_type" required class="form-select @error('user_type') {{'border-danger'}} @enderror" aria-label="Default select example">
        <option @if ($user->user_type == "1") selected @endif value="1">Owner</option>
        <option @if ($user->user_type == "2") selected @endif value="2">Renter</option>
      </select>
      @error('user_type')
       <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
      @enderror
    </div>

    <div class="col-md-6">
        <label class="form-label">Profile Image</label>
        <input name="profile_image" type="file" class="form-control @error('profile_image') border-danger @enderror">
        @error('profile_image')
         <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
        @enderror
    </div>

    @if (!empty($user->image))
        <div class="col-12">
            <img class="img-thumbnail" width="80px" src="{{asset('upload/user/'.$user->image)}}" alt="">
        </div>
    @endif

    <div class="col-12">
      <button type="submit" class="btn btn-primary">Update User</button>
    </div>
  </form>

@endsection
