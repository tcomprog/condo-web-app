@extends('includes.admin_template')
@section('content')

@php
    $user = Auth::user();
@endphp

<style>
    .tbody, td, tfoot, th, thead, tr{
        border-style: none !important;
    }
    div.dataTables_wrapper div.dataTables_length select{
        width: 80px !important;
    }
    td{
        padding-top: 10px !important;
        padding-bottom: 10px !important;
    }
    .pagination > li{
        padding: 0px !important;
        padding-top: 10px;
    }
</style>

<div class="g-3 bg-white my-3 rounded border-left-primary py-4 px-4 mx-2 mb-4 shodows">
   {{-- <a href="{{route('add-user')}}" class="btn btn-outline-primary btn-sm">
    <span class="fa fa-user me-1"></span>
     Add User
   </a> --}}

   @if ($type == 'active' || empty($type))
    <a class="btn btn-outline-danger ms-2 btn-sm" href="{{url("user-list")."/inactive"}}">
        <span class="fa fa-user-minus me-1"></span>
        Show Deleted User
    </a>
   @else
    <a class="btn btn-outline-success ms-2 btn-sm" href="{{url("user-list")."/active"}}">
        <span class="fa fa-user-plus me-1"></span>
        Show active user
    </a>
   @endif

  
   <div class="mt-4">
    {{-- <table id="myTable" class="stripe"> --}}
    <table id="myTable" class="table table-striped dataTable" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Flat</th>
                <th>Type</th>
                <th>Status</th>
                <th>Join Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($user_list as $row)
                <tr>
                    <td>{{$row->name}}</td>
                    <td>{{$row->email}}</td>
                    <td>{{$row->mobile}}</td>
                    <td>{{$row->flat_details}}</td>
                    <td>
                        @if ($row->user_type == 1)
                         <span class="badge text-bg-success">Owner</span>
                        @else   
                         <span class="badge text-bg-warning">Renter</span>
                        @endif
                    </td>
                    <td>
                        @if (in_array($row->id,$active_user))
                         <span class="badge text-bg-info">Online</span>
                        @else   
                         <span class="badge text-bg-secondary">Inactive</span>
                        @endif
                    </td>
                    <td>{{date('d-m-Y',strtotime($row->created_at))}}</td>
                    <td class="text-nowrap">
                     
                        <a href="{{url("user-profile")."/".$row->id}}" href="" class="btn btn-outline-success btn-sm" title="View profile">
                            <span class="fa fa-user"></span>
                        </a>

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
  </div>

</div>


@endsection