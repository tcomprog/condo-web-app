@extends('includes.admin_template')
@section('content')

<style>
    .tbody, td, tfoot, th, thead, tr{
        border-style: none !important;
    }
    div.dataTables_wrapper div.dataTables_length select{
        width: 80px !important;
    }
    td{
        padding-top: 10px !important;
        padding-bottom: 10px !important;
    }
    .pagination > li{
        padding: 0px !important;
        padding-top: 10px;
    }
</style>

<div class="bg-white my-3 rounded @if($user->status == 'active') border-left-success @else border-left-danger @endif py-3 px-3 mx-2 mb-4 shadows">

    <div class="row my-3 mt-4 col-sm-12">
         <div class="col-md-4 border-end-success">
            <div class="d-flex gap-1 align-items-center">
                @if (empty($user->image))
                    <img width="150px" src="{{asset('images/no_image.jpg')}}" class="img-thumbnail" alt="...">
                @else
                    <img width="150px" src="{{asset('upload/user/'.$user->image)}}" class="img-thumbnail" alt="...">
                @endif

                <div class="mt-0 ps-2 d-flex flex-column gap-1">
                    <a href="{{url("update-user-page")."/".$user->id}}" class="btn btn-outline-success btn-sm" style="width: 37px; height:33px" title="Edit profile">
                        <span class="fa fa-pen"></span>
                    </a>
            
                    <a href="{{url("re-send-auth-email")."/".$user->id}}" style="width: 37px; height:33px" class="btn btn-outline-primary btn-sm" title="Resend username & password">
                        <span class="fa fa-key"></span>
                    </a>

                    @if ($user->status == 'active')
                        <a href="{{url("delete-user")."/".$user->id}}" class="btn btn-outline-danger btn-sm" style="width: 37px; height:33px" title="Delete this user">
                            <span class="fa fa-trash"></span>
                        </a>
                    @else
                        <a href="{{url("reactive-user")."/".$user->id}}" class="btn btn-outline-danger btn-sm" style="width: 37px; height:33px" title="Reactive user">
                            <span class="fa fa-ban"></span>
                        </a>
                    @endif

                    <a href="{{url("send-user-email")."/".$user->id}}" style="width: 37px; height:33px" class="btn btn-outline-info btn-sm" title="Send mail">
                        <span class="fa fa-paper-plane"></span>
                    </a>
                </div>
            </div>

            <h5 class="ps-2 pt-2 mb-1">{{$user->name}}</h5>
            <p class="ps-2 fs-6 mb-2">{{$user->email}}</p>
         </div>


         <div class="col-md-8 col-sm-12">
            
            <div class="row g-2">
                <div class="col-md-6 col-sm-12">
                    <div class="bg-body-secondary rounded px-3 py-1">
                        <p class="fw-bold mb-0">Mobile</p>
                        <p class="mb-0">{{$user->mobile}}</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="bg-body-secondary rounded px-3 py-1">
                        <p class="fw-bold mb-0">Flat Details</p>
                        <p class="mb-0">{{$user->flat_details}}</p>
                    </div>
                </div>
            </div>

            <div class="row g-2 mt-2">
                <div class="col-md-6 col-sm-12">
                    <div class="bg-body-secondary rounded px-3 py-1">
                        <p class="fw-bold mb-0">Type</p>
                        <p class="mb-0">
                            @if($user->user_type == 1)
                              <span class="text-success">Owner</span>
                            @else
                              <span class="text-danger">Renter</span>
                            @endif
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="bg-body-secondary rounded px-3 py-1">
                        <p class="fw-bold mb-0">Status</p>
                        <p class="mb-0">{{ucfirst($user->status)}}</p>
                    </div>
                </div>
            </div>

            <div class="row g-2 mt-2">
                <div class="col-md-6 col-sm-12">
                    <div class="bg-body-secondary rounded px-3 py-1">
                        <p class="fw-bold mb-0">Join Date</p>
                        <p class="mb-0">{{date("d/m/Y H:i",strtotime($user->created_at))}}</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="bg-body-secondary rounded px-3 py-1">
                        <p class="fw-bold mb-0">Society</p>
                        <p class="mb-0">{{Auth::user()->society_name}}</p>
                    </div>
                </div>
            </div>
           
         </div>
    </div>

    <hr>

    <div class="row mb-3">
        <div class="col-12 mx-2">
            <a href="" class="btn btn-success btn-sm">
                <span class="fa fa-dollar-sign"></span> Generate Bill
            </a>
        </div>
    </div>

    @if($isMobileLogin)
    <div class="mb-3">
      <hr>
      <h5 class="ps-2 pt-2 mb-1">Mobile Login Data</h5>

      <div class="row g-2 mt-2">
        <div class="col-md-4 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1">
                <p class="fw-bold mb-0">Society</p>
                <p class="mb-0">{{ucfirst($mobileLoginData->platform)}}</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1">
                <p class="fw-bold mb-0">Login at</p>
                <p class="mb-0">{{date("d/m/Y H:i",strtotime($mobileLoginData->created_at))}}</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1">
                <p class="fw-bold mb-0">Updated at</p>
                <p class="mb-0">{{date("d/m/Y H:i",strtotime($mobileLoginData->updated_at))}}</p>
            </div>
        </div>
    </div>
   </div>

    @endif


    @if(count($request_data) > 0)
    <div class="mb-3 mx-2">
        <hr>
        <h5 class="ps-2 pt-2 mb-1">Requests</h5>
  
        <div class="row g-2 mt-2">
          <div class="col-md-12 col-sm-12">
            
            <table class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Type</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($request_data as $row)
                        <tr>
                            <td width="30%">{{$row->title}}</td>
                            <td width="30%">{{$row->type}}</td>
                            <td width="15%">{{date('d-m-Y',strtotime($row->created_at))}}</td>
                            <td width="15%">
                                @if ($row->status == 0)
                                 <span class="badge text-bg-warning">Pending</span>
                                @elseif($row->status == 1)  
                                 <span class="badge text-bg-success">Approved</span>
                                @elseif($row->status == 2)  
                                 <span class="badge text-bg-danger">Rejected</span>
                                @endif
                            </td>
                            <td width="10%" class="text-nowrap">
                                <a href="{{url("request-details")."/".$row->id}}" href="" class="btn btn-outline-success btn-sm" title="View profile">
                                    <span class="fa fa-eye"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

          </div>
      </div>
     </div>
      @endif


</div>

@endsection