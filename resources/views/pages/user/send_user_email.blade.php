@extends('includes.admin_template')
@section('content')

<form method="POST" action="{{route('sendUserEmail')}}" class="row g-3 bg-white my-3 rounded border-left-primary py-4 px-5 mx-2 mb-4 shadows">
    @csrf
    <div class="row g-2">
        <div class="col-md-6 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1">
                <p class="fw-bold mb-0">To</p>
                <p class="mb-0">{{$user->name}}</p>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1">
                <p class="fw-bold mb-0">Email</p>
                <p class="mb-0">{{$user->email}}</p>
            </div>
        </div>
    </div>
    <input type="hidden" value="{{$user->id}}" name="user_id">
    <div class="row">
    <div class="mb-3 mt-5">
        <label for="subject" class="form-label fw-bold">Subject</label>
        <input  type="text" class="form-control @error('subject') {{'border-danger'}} @enderror" id="subject" name="subject" placeholder="Enter subject...">
        @error('subject')
         <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
        @enderror
      </div>
      <div class="mb-3">
        <label for="message" class="form-label fw-bold">Message</label>
        <textarea name="message" class="form-control @error('message') {{'border-danger'}} @enderror" id="message" rows="3" placeholder="Message"></textarea>
        @error('message')
          <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
        @enderror
    </div>
  </div>
  
  <div>
    <button class="btn btn-primary" type="submit">Send</button>
  </div >

</form>

@endsection