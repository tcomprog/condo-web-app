@extends('includes.admin_template')
@section('content')

@php
    $user = Auth::user();
@endphp

<style>
    .tbody, td, tfoot, th, thead, tr{
        border-style: none !important;
    }
    div.dataTables_wrapper div.dataTables_length select{
        width: 80px !important;
    }
    td{
        padding-top: 10px !important;
        padding-bottom: 10px !important;
    }
    .pagination > li{
        padding: 0px !important;
        padding-top: 10px;
    }
</style>

<div class="row g-3 bg-white my-3 rounded border-left-primary py-4 px-5 mx-2 mb-4 shadows">
    <h4 class="mt-0 text-primary">Pending society member request</h4>
    <table id="myTable" class="table table-striped dataTable" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Flat</th>
                <th>Type</th>
                <th>Request Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($user_list as $row)
                <tr>
                    <td>{{$row->name}}</td>
                    <td>{{$row->email}}</td>
                    <td>{{$row->mobile}}</td>
                    <td>{{$row->flat_details}}</td>
                    <td>
                        @if ($row->user_type == 1)
                         <span class="badge text-bg-success">Owner</span>
                        @else   
                         <span class="badge text-bg-warning">Renter</span>
                        @endif
                    </td>
                    <td>{{date('d-m-Y',strtotime($row->created_at))}}</td>
                    <td class="text-nowrap">
                     
                  
                        <a href="{{url("acceptAccountRequest")."/".$row->id}}" class="btn btn-outline-success btn-sm" title="Accept request">
                            <span class="fa fa-check"></span>
                        </a>

                        <a href="{{url("deletePendingRequest")."/".$row->id}}" class="btn btn-outline-danger btn-sm" title="Delete request">
                            <span class="fa fa-trash"></span>
                        </a>

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection