@extends('includes.admin_template')
@section('content')

@php
    $user = Auth::user();
@endphp

<form enctype="multipart/form-data" method="post" action="{{route('updateProfile')}}" class="row g-3 bg-white my-3 rounded border-left-primary py-4 px-5 mx-2 mb-4" style="box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 12px;">
    @csrf
    <div class="col-md-6">
      <label class="form-label">Latitude<sup class="text-danger">*</sup><sub class="text-primary">e.g 31.4554732</sub></label>
      <input name="latitude" type="text" maxlength="50" required class="form-control" value="{{$user->latitude}}">
    </div>

    <div class="col-md-6">
      <label class="form-label">Longitude<sup class="text-danger">*</sup><sub class="text-primary">e.g 31.4554732</sub></label>
      <input name="longitude" type="text" required class="form-control" value="{{$user->longitude}}">
    </div>

    <div class="col-md-6">
      <label class="form-label">Email<sup class="text-danger">*</sup></label>
      <input name="email" type="email" disabled required class="form-control" value="{{$user->email}}">
    </div>

    <div class="col-md-6">
      <label class="form-label">Name<sup class="text-danger">*</sup></label>
      <input name="name" type="text" required class="form-control" value="{{$user->name}}">
    </div>

    <div class="col-md-6">
      <label class="form-label">Society Name<sup class="text-danger">*</sup></label>
      <input name="society_name" type="text" required class="form-control" value="{{$user->society_name}}">
    </div>

    <div class="col-md-6">
      <label class="form-label">Society Phone No<sup class="text-danger">*</sup></label>
      <input name="phone_no" type="text" required class="form-control" value="{{$user->phone_no}}">
    </div>

    <div class="col-12">
      <label class="form-label">Society Address<sup class="text-danger">*</sup></label>
      <input name="address" type="text" required class="form-control" placeholder="Complete address" value="{{$user->address}}">
    </div>

    <div class="col-md-6">
        <label class="form-label">Profile Image</label>
        <input name="profile_image" type="file" class="form-control @error('profile_image') border-danger @enderror">
        @error('profile_image')
         <span class="text-danger ps-2 errorMessage" style="font-size:14px;">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-md-6">
       @if (!empty(Auth::user()->profile_image))
         <img src="{{asset('upload/admin/'.Auth::user()->profile_image)}}" height="80px" width="80px" class="rounded" alt="">
       @endif
    </div>

    <div class="col-12">
      <button type="submit" class="btn btn-primary">Update</button>
    </div>
    
    <div class="col-12 mt-5">
       <h4>How to latitude and longitude?</h4>
       <h5 class="mt-3">Using your phone:</h5>
       <ol>
         <li><b>Android:</b> On Google Maps, tap and hold anywhere on the map to drop a pin. The information card at the bottom will show your current location's coordinates. On other apps, look for a "Share location" or "Get coordinates" option.</li>
         <li><b>iOS:</b> On Apple Maps, tap the blue dot representing your location. Then, swipe the information card at the bottom up to reveal your coordinates. Similar methods may apply to other apps.</li>
       </ol>

       <h5 class="mt-3">Using your computer:</h5>
       <ol>
         <li><b>GPS devices:</b> If you have a dedicated GPS device, it will directly display your current location's coordinates. <a href="https://www.google.com/maps" target="_blank">Open map</a></li>
         <li><b>Online tools:</b> Several websites like <a href="https://www.latlong.net/" target="_blank">latlong.net</a>, <a href="https://www.gpscoordinates.net" target="_blank">gpscoordinates.net</a>, etc., allow you to enter your address or click on a map to find the corresponding latitude and longitude.</li>
       </ol>

    </div>
  </form>

@endsection