@extends('includes.admin_template')
@section('content')

<style>
    .ck-editor__editable_inline {
      min-height: 250px;
    }
</style>

<div class="row g-3 bg-white my-3 rounded border-left-primary py-4 px-5 mx-2 mb-4 shadows">
    <form id="privacyPolicyForm" action="{{route("savePrivacyPolicy")}}" method="POST">
     @csrf
    <div class="col-12">
        <textarea id="editor" name="ckeditorData" style="width: 100%;height:300px"></textarea>
    </div>
    <div class="col-12 mt-3">
        <button id="submitForm" type="button" class="btn btn-outline-primary w-auto">Submit</button>
    </div>
   </form>
</div>

<script src="{{asset('vendor/ckeditor5/build/ckeditor.js')}}"></script>
<script>
    let editor;
    ClassicEditor
    .create( document.querySelector( '#editor' ) )
    .then( newEditor => {
        editor = newEditor;
        let data = `<?php echo htmlspecialchars_decode($policy); ?>`;
        editor.setData(data)
    } )
    .catch( error => {
        console.error( error );
    } );
</script>
@endsection