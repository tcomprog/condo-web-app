@extends('includes.admin_template')
@section('content')


<form enctype="multipart/form-data" method="post" action="{{route('saveAlert')}}" class="row g-3 bg-white my-3 rounded border-left-primary py-4 px-5 mx-2 mb-4" style="box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 12px;">
    @csrf
    <div class="col-md-12">
      <label class="form-label">Alert title<sup class="text-danger">*</sup></label>
      <input name="title" type="text" required class="form-control">
    </div>

    <div class="col-md-12">
      <label class="form-label">Description<sup class="text-danger">*</sup></label>
      <textarea class="form-control" required name="body" placeholder="Alert description" rows="3" id="floatingTextarea"></textarea>
    </div>

    <div class="col-12">
      <button type="submit" class="btn btn-primary">Send</button>
    </div>
  </form>

@endsection