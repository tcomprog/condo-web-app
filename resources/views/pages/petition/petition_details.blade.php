@extends('includes.admin_template')
@section('content')

<div class="row g-3 bg-white my-3 rounded border-left-primary py-4 px-5 mx-2 mb-4 shadows">
 
    <div class="row g-2">
        <div class="col-md-6 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1" style="height: 95%">
                <p class="fw-bold mb-0">Member Name</p>
                <p class="mb-0">{{$member->name}}</p>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1" style="height: 95%">
                <p class="fw-bold mb-0">Member Email</p>
                <p class="mb-0">{{$member->email}}</p>
            </div>
        </div>
    </div>

    <div class="row g-2">
        <div class="col-md-6 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1" style="height: 95%">
                <p class="fw-bold mb-0">Petition Title</p>
                <p class="mb-0">{{$request_detail->title}}</p>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1" style="height: 95%">
                <p class="fw-bold mb-0">Petition Type</p>
                <p class="mb-0">{{$request_detail->type}}</p>
            </div>
        </div>
    </div>

    <div class="row g-2">
        <div class="col-md-6 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1" style="height: 95%">
                <p class="fw-bold mb-0">Date</p>
                <p class="mb-0">{{date('d/m/Y',strtotime($request_detail->created_at))}}</p>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1" style="height: 95%">
                <p class="fw-bold mb-0">Status</p>
                <p class="mb-0">
                    @if($request_detail->status == 0)
                      <span class="fw-bold text-warning">Pending</span>
                    @elseif($request_detail->status == 1)
                      <span class="fw-bold text-success">Approved</span>
                    @elseif($request_detail->status == 2)
                      <span class="fw-bold text-danger">Rejected</span>
                    @endif
                </p>
            </div>
        </div>
    </div>

    <div class="row g-2">
        <div class="col-md-12 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1">
                <p class="fw-bold mb-0">Description</p>
                <p class="mb-0">{{$request_detail->desc}}</p>
            </div>
        </div>
    </div>

    @if($request_detail->status != 0)
    <div class="row g-2">
        <div class="col-md-12 col-sm-12">
            <div class="bg-body-secondary rounded px-3 py-1">
                <p class="fw-bold mb-0">Your comment</p>
                <p class="mb-0">{{$request_detail->adminComment}}</p>
            </div>
        </div>
    </div>
    @endif

    @if(!empty($request_detail->images))
    <div class="d-flex gap-2 flex-wrap mt-3">
        @php
          $images = json_decode($request_detail->images,true);
        @endphp
        @foreach ($images as $item)
          <a href="{{asset('upload/petition/'.$item)}}" target="_blank">
          <img style="width:170px;height:170px" src="{{asset('upload/petition/'.$item)}}" class="img-thumbnail"/>
          </a>
        @endforeach
    </div>
    @endif

    @if($request_detail->status == 0)
    <div class="row g-2 mt-4">
        <form method="POST" action="{{route('changePetitionStatus')}}">
            @csrf
            <input name="request_id" type="hidden" value="{{$request_detail->id}}">
            <div class="form-floating">
                <textarea maxlength="900" class="form-control" name="comment"  placeholder="Comments" style="height: 100px"></textarea>
                <label for="floatingTextarea">Comments</label>
            </div>

            <div class="mt-3 d-flex gap-1">
                <button type="submit" name="status" value="approve" class="btn btn-outline-success">Approve</button>
                <button type="submit" name="status" value="reject" class="btn btn-outline-danger ms-2">Reject</button>
            </div>
        </form>
    </div>
    @endif

</div>

@endsection