@extends('includes.admin_template')
@section('content')

@php
    $user = Auth::user();
@endphp

<style>
    .tbody, td, tfoot, th, thead, tr{
        border-style: none !important;
    }
    div.dataTables_wrapper div.dataTables_length select{
        width: 80px !important;
    }
    td{
        padding-top: 10px !important;
        padding-bottom: 10px !important;
    }
    .pagination > li{
        padding: 0px !important;
        padding-top: 10px;
    }
</style>

<div class="g-3 bg-white my-3 rounded border-left-primary py-4 px-4 mx-2 mb-4 shodows">

    <a class="btn btn-outline-info ms-2 btn-sm" href="{{url("petition-list")}}">
        <span class="fa fa-bars me-1"></span>
        All
    </a>

    <a class="btn btn-outline-secondary ms-2 btn-sm" href="{{url("petition-list")."/0"}}">
        <span class="fa fa-arrow-down me-1"></span>
        Pending
    </a>

    <a class="btn btn-outline-success ms-2 btn-sm" href="{{url("petition-list")."/1"}}">
        <span class="fa fa-check me-1"></span>
        Approved
    </a>

    <a class="btn btn-outline-danger ms-2 btn-sm" href="{{url("petition-list")."/2"}}">
        <span class="fa fa-minus me-1"></span>
        Rejected
    </a>
  
   <div class="mt-4">
    {{-- <table id="myTable" class="stripe"> --}}
    <table id="myTable" class="table table-striped dataTable" style="width:100%">
        <thead>
            <tr>
                <th>Title</th>
                <th>Type</th>
                <th>Date</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($request_data as $row)
                <tr>
                    <td width="30%">{{$row->title}}</td>
                    <td width="30%">{{$row->type}}</td>
                    <td width="15%">{{date('d-m-Y',strtotime($row->created_at))}}</td>
                    <td width="15%">
                        @if ($row->status == 0)
                         <span class="badge text-bg-warning">Pending</span>
                        @elseif($row->status == 1)  
                         <span class="badge text-bg-success">Approved</span>
                        @elseif($row->status == 2)  
                         <span class="badge text-bg-danger">Rejected</span>
                        @endif
                    </td>
                    <td width="10%" class="text-nowrap">
                        <a href="{{url("petition-details")."/".$row->id}}" href="" class="btn btn-outline-success btn-sm" title="View profile">
                            <span class="fa fa-eye"></span>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
  </div>

</div>


@endsection