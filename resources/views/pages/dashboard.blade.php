@extends('includes.admin_template')
@section('content')

@php
 $user = Auth::user();
@endphp

<div class="container-fluid">
  @if(empty($user->latitude) || $user->latitude == 0)
  <div class="alert alert-danger" role="alert">
    Please update your location (Latitude, Longitude) without it building map feature will not work correctly. <a href="{{route("admin-profile")}}" class="alert-link">Click here to update</a>.
  </div>
  @endif



    <div class="row mb-4 mt-3">

        <div class="col-xl-8 col-lg-7">
    
        <div class="card shadow">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Voting Poll Chart</h6>
            </div>
            <div class="card-body">
                <div class="chart-bar">
                    <canvas id="myBarChart"></canvas>
                </div>
                {{-- <hr>
                Styling for the bar chart can be found in the --}}
            </div>
          </div>
        </div>
    
    
         <!-- Donut Chart -->
         <div class="col-xl-4 col-lg-5">
            <div class="card shadow h-100">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Society Member Requests</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-pie pt-4">
                        <canvas id="myPieChart"></canvas>
                    </div>
                    {{-- <hr>
                    A Pie chart of society member request --}}
                </div>
            </div>
        </div>
    
       </div>


    <div class="row mt-4">

        <a href="{{route('pending-user')}}" class="col-xl-3 col-md-6 mb-4 cursor-pointer text-decoration-none">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Pending members</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$pending}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-user-injured fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>

        <a href="{{route('user-list')}}" class="col-xl-3 col-md-6 mb-4 cursor-pointer text-decoration-none">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Active members</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$active}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-user-check fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>

        <a href="{{url('/user-list/inactive')}}"  class="col-xl-3 col-md-6 mb-4 cursor-pointer text-decoration-none">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                Inactive members</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$inactive}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-user-minus fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>


        <div class="col-xl-3 col-md-6 mb-4 ">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                Total members</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$active+$inactive+$pending}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-user fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>



  
          <a href="{{url("petition-list")."/0"}}" class="col-xl-3 col-md-6 mb-4 cursor-pointer text-decoration-none">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Pending petition</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$p_pending}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-file fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>

         <a href="{{url("petition-list")."/1"}}" class="col-xl-3 col-md-6 mb-4 cursor-pointer text-decoration-none">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Approved petition</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$p_active}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-paste fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>

         <a href="{{url("petition-list")."/2"}}" class="col-xl-3 col-md-6 mb-4 cursor-pointer text-decoration-none">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                                Rejected petition</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$p_reject}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-paste fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>


    </div>


</div>

<script src="{{asset('vendor/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('js/demo/chart-area-demo.js')}}"></script>
<script src="{{asset('js/demo/chart-bar-demo.js')}}"></script>
<script src="{{asset('js/demo/chart-pie-demo.js')}}"></script>

<script>
var ctx = document.getElementById("myPieChart");
let requests = @json($requests);

var myPieChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: ["Pending", "Approved", "Rejected"],
    datasets: [{
      data: requests,
      backgroundColor: ['#F1C40F', '#1cc88a', '#E74C3C'],
      hoverBackgroundColor: ['#F1C40F', '#17a673', '#E74C3C'],
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
    },
    legend: {
      display: false
    },
    cutoutPercentage: 80,
  },
});



let voting_data = @json($voting_grp);
let max_no = voting_data.filter((x) => !isNaN(x)).reduce((a, b) => a + b)
var ctx = document.getElementById("myBarChart");
var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["Pending", "Active", "Complete", "Reject"],
    datasets: [{
      label: "Voting",
      backgroundColor: ["#3B3B3B","#7ca751","#f9b405","#d44d4d"],
      hoverBackgroundColor: "#0f3457",
      borderColor: "#4e73df",
      data: voting_data,
    }],
  },
  options: {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'month'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 6
        },
        maxBarThickness: 25,
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: max_no,
          maxTicksLimit: 10,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return number_format(value);
          }
        },
        gridLines: {
          color: "rgb(234, 236, 244)",
          zeroLineColor: "rgb(234, 236, 244)",
          drawBorder: false,
          borderDash: [2],
          zeroLineBorderDash: [2]
        }
      }],
    },
    legend: {
      display: false
    },
    tooltips: {
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
        }
      }
    },
  }
});
</script>

@endsection