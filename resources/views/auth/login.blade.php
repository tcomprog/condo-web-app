<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

     @include('includes.favicon')

    <!-- Custom fonts for this template-->
    <link href="{{asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset('css/sb-admin-2.min.css')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
    <link href="{{asset('css/common.css')}}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">


</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center align-items-center vh-100">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image" style="opacity: 0.5"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                    </div>
                                    <form autocomplete="off" class="user" method="POST">
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-user inputField"
                                                id="emailAddress" aria-describedby="emailHelp" value="@isset($_COOKIE["condo_email"]) {{Encryption::decrypt($_COOKIE["condo_email"])}} @endisset"
                                                placeholder="Enter Email Address..." autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user inputField" id="password" value="@isset($_COOKIE["condo_password"]) {{Encryption::decrypt($_COOKIE["condo_password"])}} @endisset"
                                                 placeholder="Password" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck" autocomplete="off" @isset($_COOKIE["condo_password"]) {{"checked"}} @endisset >
                                                <label class="custom-control-label" for="customCheck">Remember
                                                    Me</label>
                                            </div>
                                        </div>

                                        <div class="text-end mt-0 mb-3" style="display: none;" id="resend_verification_email_btn">
                                            <a id="resendLink" class="small" href="#">Resend Verification Email</a>
                                        </div>

                                        <button type="button" id="loginBtnClick" class="btn btn-primary btn-user btn-block">
                                            Login
                                        </button>
                                        <hr>
                                     
                                    </form>
                                    
                                    <input type="hidden" id="u_email">
                                    <input type="hidden" id="u_password">
                                 
                                    <div class="text-center">
                                        <a class="small" href="{{url("/forgot-pas")}}">Forgot Password?</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="{{url("/register")}}">Create an Account!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
      
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('js/sb-admin-2.min.js')}}"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
    <script src="{{asset('js/common.js')}}"></script>

</body>

</html>

<script>
   $("#loginBtnClick").on('click',function(){
     let email = $("#emailAddress");
     let password = $("#password");
     let remember_me = $("#customCheck");
     let errorClassName = 'class="text-danger ps-2 errorMessage" style="font-size:14px;"';

     $(".errorMessage").remove();
     $(".inputField").removeClass("border-danger");
     $("#resend_verification_email_btn").hide();

     remember_me = remember_me.is(':checked') ? 1 : 0;
     let error = 0;
   
     if(isEmpty(email.val())){
        email.addClass("border-danger");
        email.after('<span '+errorClassName+'>Enter email</span>');
        error = 1;
     }

     if(!isEmpty(email.val()) && !isEmail(email.val())){
        email.addClass("border-danger");
        email.after('<span '+errorClassName+'>Invalid email address</span>');
        error = 1;
    }

     if(isEmpty(password.val())){
        password.addClass("border-danger");
        password.after('<span '+errorClassName+'>Enter password</span>');
        error = 1;
     }

     if(error == 0){
       // good to go
       showLoader();
       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: '{{url("login-user")}}',
            data: {
               email:email.val(),
               password:password.val(),
               remember_me:remember_me
            },
            dataType: 'json',
            success: function (data) {
                hideLoader();
                if(data.status == 'success'){
                   location.href = '{{route("dashboard")}}';
                }
                else if(data.status == 'inactive'){
                    showMessage(data.message,1);
                    $("#u_email").val(data.u_email);
                    $("#u_password").val(data.u_password);
                    $("#resend_verification_email_btn").show();
                }
                else{
                   showMessage(data.message,1);
                }
            },
            error: function (data) {
                hideLoader();
                showMessage("Something went wrong try again",1);
            }
        });

     }
     else{
        setTimeout(()=>{
            $(".errorMessage").remove(); 
            $(".inputField").removeClass("border-danger");
        },3000)
     }

   });

   $("#resendLink").on('click',function() {
      let u_email = $("#u_email").val();
      let u_password = $("#u_password").val();

      if(!isEmpty(u_email) && !isEmpty(u_password)){
         $("#resend_verification_email_btn").hide();
         showLoader();

         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

         $.ajax({
            type: "POST",
            url: '{{url("resend-verif-email")}}',
            data: {
               email:u_email,
               password:u_password
            },
            dataType: 'json',
            success: function (data) {
                hideLoader();
                if(data.status == 'already' || data.status == 'success'){
                    $("#resend_verification_email_btn").hide();
                }
                else{
                    $("#resend_verification_email_btn").show();
                }
               
                if(data.status == 'success'){
                   showMessage(data.message);
                }else{
                    showMessage(data.message,1);
                }

            },
            error: function (data) {
                $("#resend_verification_email_btn").show();
                hideLoader();
                showMessage("Something went wrong try again",1);
            }
        });
      }
      else{
        showMessage("Invalid access",1);
      }

   });
</script>