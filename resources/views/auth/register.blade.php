<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('includes.favicon')

    <!-- Custom fonts for this template-->
    <link href="{{asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset('css/sb-admin-2.min.css')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <link href="{{asset('css/common.css')}}" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block bg-register-image" style="opacity: 0.5"></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                            </div>
                            <form class="user" method="POST" id="registerUserForm">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input name="your_name" type="text" class="form-control form-control-user inputField" 
                                            placeholder="Your Name">
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="society_name" type="text" class="form-control form-control-user inputField" 
                                            placeholder="Society Name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input name="address" type="text" class="form-control form-control-user inputField" 
                                        placeholder="Society Address">
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input name="phone_no" type="text" class="form-control form-control-user inputField" 
                                            placeholder="Society Phone No">
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="email" type="text" class="form-control form-control-user inputField" 
                                            placeholder="Email Address">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input name="password" type="password" class="form-control form-control-user inputField"
                                            id="exampleInputPassword" placeholder="Password">
                                    </div>
                                    <div class="col-sm-6">
                                        <input name="re-password" type="password" class="form-control form-control-user inputField"
                                            id="exampleRepeatPassword" placeholder="Repeat Password">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Register Account
                                </button> 
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="{{url("/forgot-pas")}}">Forgot Password?</a>
                            </div>
                            <div class="text-center">
                                <a class="small" href="{{url("/")}}">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('js/sb-admin-2.min.js')}}"></script>

    <script type="text/javascript" src="https://unpkg.com/validator.tool/dist/validator.min.js"></script>

    <script src="{{asset('js/common.js')}}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>

</body>

</html>


<script>
    // =============== Register User Api Call ==================

    $(document).on('submit','#registerUserForm',function(form){
    form.preventDefault();
    let formData = $("#registerUserForm").serializeArray()
    let errorClassName = 'class="text-danger ps-2 errorMessage" style="font-size:14px;"';

    $(".errorMessage").remove();
    $(".inputField").removeClass("border-danger");
    
    let your_name = $("input[name='your_name']").val(); 
    let society_name = $("input[name='society_name']").val();
    let address = $("input[name='address']").val();
    let phone_no = $("input[name='phone_no']").val();
    let email = $("input[name='email']").val();
    let password = $("input[name='password']").val();
    let re_password = $("input[name='re-password']").val();

    let error = 0;

    if(isEmpty(your_name)){
        $("input[name='your_name']").addClass("border-danger");
        $("input[name='your_name']").after('<span '+errorClassName+'>Enter your name</span>');
        error = 1;
    }

    if(isEmpty(society_name)){
        $("input[name='society_name']").addClass("border-danger");
        $("input[name='society_name']").after('<span '+errorClassName+'>Enter society name</span>');
        error = 1;
    }

    if(isEmpty(address)){
        $("input[name='address']").addClass("border-danger");
        $("input[name='address']").after('<span '+errorClassName+'>Enter address</span>');
        error = 1;
    }

    if(isEmpty(phone_no)){
        $("input[name='phone_no']").addClass("border-danger");
        $("input[name='phone_no']").after('<span '+errorClassName+'>Enter phone no</span>');
        error = 1;
    }

    if(isEmpty(email)){
        $("input[name='email']").addClass("border-danger");
        $("input[name='email']").after('<span '+errorClassName+'>Enter email</span>');
        error = 1;
    }

    if(!isEmpty(email) && !isEmail(email)){
        $("input[name='email']").addClass("border-danger");
        $("input[name='email']").after('<span '+errorClassName+'>Invalid email address</span>');
        error = 1;
    }

    if(isEmpty(password)){
        $("input[name='password']").addClass("border-danger");
        $("input[name='password']").after('<span '+errorClassName+'>Enter password</span>');
        error = 1;
    }

    if(!isEmpty(password)){
        let em = isPassword(password);
        if(em != null){
            $("input[name='password']").addClass("border-danger");
            $("input[name='password']").after('<span '+errorClassName+'>'+em+'</span>');
            error = 1;
        }
    }

    if(isEmpty(re_password)){
        $("input[name='re-password']").addClass("border-danger");
        $("input[name='re-password']").after('<span '+errorClassName+'>Enter repeat password</span>');
        error = 1;
    }

    if(!isEmpty(re_password) && password != re_password){
        $("input[name='re-password']").addClass("border-danger");
        $("input[name='re-password']").after('<span '+errorClassName+'>Password does not match</span>');
        error = 1;
    }

    if(error == 0){
        // You are good to go ===============
        showLoader();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: '{{url("register-user")}}',
            data: {
               your_name:your_name,
               society_name:society_name,
               address:address,
               phone_no:phone_no,
               email:email,
               password:password
            },
            dataType: 'json',
            success: function (data) {
                hideLoader();
                if(data.status == "success"){
                    showMessage(data.message,0);
                    $(".inputField").removeClass("border-danger");
                    $("#registerUserForm").trigger("reset")
                }else{
                    showMessage(data.message,1);
                }
            },
            error: function (data) {
                hideLoader();
                showMessage("Something went wrong try again",1);
            }
        });

    }
    else{
        setTimeout(()=>{
            $(".errorMessage").remove(); 
            $(".inputField").removeClass("border-danger");
        },3000)
    }

 });
</script>