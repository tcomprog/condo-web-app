<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

     @include('includes.favicon')

    <!-- Custom fonts for this template-->
    <link href="{{asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset('css/sb-admin-2.min.css')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
    <link href="{{asset('css/common.css')}}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center align-items-center vh-100">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-password-image" style="opacity: 0.5"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-2">New Password</h1>
                                    </div>
                                    <form class="user">
                                        <div class="form-group">
                                            <input type="text" maxlength="100" class="form-control form-control-user"
                                                id="userEmail" placeholder="Enter Email Address...">
                                        </div>
                                        
                                        <div class="form-group">
                                            <input type="password" maxlength="150" class="form-control form-control-user"
                                                id="userPassword" placeholder="Password">
                                        </div>

                                        <input type="hidden" id="token_id" value="{{$token}}">

                                        <div class="form-group">
                                            <input type="password" maxlength="150" class="form-control form-control-user"
                                                id="userPasswordRe" placeholder="Repeat Password">
                                        </div>

                                        <button id="submitforgotPassword" type="button" class="btn btn-primary btn-user btn-block">
                                            Save Changes
                                        </button>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="{{url("/forgot-pas")}}">Forgot Password?</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="{{url("/register")}}">Create an Account!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

  <!-- Bootstrap core JavaScript-->
  <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{asset('js/sb-admin-2.min.js')}}"></script>

  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
  <script src="{{asset('js/common.js')}}"></script>

</body>

</html>

<script>
    $("#submitforgotPassword").on('click',function(){
       let email = $("#userEmail");
       let password = $("#userPassword");
       let token_id = $("#token_id");
       let re_password = $("#userPasswordRe");
       
       let errorClassName = 'class="text-danger ps-2 errorMessage" style="font-size:14px;"';
       let error = 0;

       $(".errorMessage").remove();
       email.removeClass("border-danger");

       if(isEmpty(email.val())){
        email.addClass("border-danger");
        email.after('<span '+errorClassName+'>Enter email</span>');
        error = 1;
       }

      if(!isEmpty(email.val()) && !isEmail(email.val())){
        email.addClass("border-danger");
        email.after('<span '+errorClassName+'>Invalid email address</span>');
        error = 1;
      }

        if(isEmpty(password.val())){
            password.addClass("border-danger");
            password.after('<span '+errorClassName+'>Enter password</span>');
            error = 1;
        }

        if(!isEmpty(password.val())){
            let em = isPassword(password.val());
            if(em != null){
                password.after('<span '+errorClassName+'>'+em+'</span>');
                password.addClass("border-danger");
                error = 1;
            }
        }

        if(isEmpty(re_password.val())){
            re_password.addClass("border-danger");
            re_password.after('<span '+errorClassName+'>Enter repeat password</span>');
            error = 1;
        }

        if(!isEmpty(re_password.val()) && password.val() != re_password.val()){
            re_password.addClass("border-danger");
            re_password.after('<span '+errorClassName+'>Password does not match</span>');
            error = 1;
        }


      if(error == 0){
        showLoader();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: '{{url("update_new_password")}}',
            data: {
               email:email.val(),
               password:password.val(),
               token:token_id.val()
            },
            dataType: 'json',
            success: function (data) {
                hideLoader();
               if(data.status == 'success'){
                 showMessage(data.message);
                 email.val('');
                 password.val('');
                 re_password.val('');
               }
               else{
                 showMessage(data.message,1);
               }
            },
            error: function (data) {
                hideLoader();
                showMessage("Something went wrong try again",1);
            }
        });
      }
      else{
        setTimeout((error)=>{
            $(".errorMessage").remove();
            email.removeClass("border-danger");
        },3000);
      }

    });
</script>