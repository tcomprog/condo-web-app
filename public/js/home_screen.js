const showMessage = (message,status = 0) => {
    // status 0 => Error, 1 => success
    let messageContainer = $("#messageContainer");

    if(status == 0){
      messageContainer.removeClass("alert-success");
      messageContainer.addClass("alert-danger");
    }else{
        messageContainer.removeClass("alert-danger");
        messageContainer.addClass("alert-success");
    }

    messageContainer.text(message);
    messageContainer.fadeIn();
}

const hideMessage = () => {
    $("#messageContainer").fadeOut();
}

function isEmpty(variable) {
    const type = typeof variable
    if (variable === null) return true
    if (type === 'undefined') return true
    if (type === 'boolean') return false
    if (type === 'string') return !variable
    if (type === 'number') return false
    if (Array.isArray(variable)) return !variable.length
    if (type === 'object') return !Object.keys(variable).length
    return !variable
}

function isEmail(mail)
{
    return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()\.,;\s@\"]+\.{0,1})+([^<>()\.,;:\s@\"]{2,}|[\d\.]+))$/.test(mail);
}



$('a[href^="#"]').click(function(){

    var the_id = $(this).attr("href");
    
        $('html, body').animate({
            scrollTop:$(the_id).offset().top
        }, 'slow');
    
    return false;});