// **************** Show Toast Message Start *********************
let showMessage = (message,status = 0) => {
   // 0 - success message, 1 - error message
   let background = status == 0 ? "linear-gradient(to right, #17B169, #1CAC78)" 
   : "linear-gradient(to right, #ff512f, #dd2476)";

   Toastify({
    text: message,
    duration: 4000,
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "right", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: background,
      color:'white'
    },
    onClick: function(){} // Callback after click
  }).showToast();
}

// **************** Show Toast Message End ***********************

// ################# Validation Functions Start ###################
  let isEmpty = (value) => {
    switch (value) {
      case "":
      case 0:
      case "0":
      case null:
      case false:
      case undefined:
        return true;
      default:
        return false;
    }
  }

  let isEmail = (value) => {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value))
    {
      return (true)
    }
    else{
       return false;
    }
  }

  let isPassword = (value) => {
      const isNonWhiteSpace = /^\S*$/;
      if (!isNonWhiteSpace.test(value)) {
          return "Password must not contain whitespaces.";
      }
      const isContainsUppercase = /^(?=.*[A-Z]).*$/;
      if (!isContainsUppercase.test(value)) {
        return "Password must have at least one uppercase character.";
      }

      const isContainsLowercase = /^(?=.*[a-z]).*$/;
      if (!isContainsLowercase.test(value)) {
        return "Password must have at least one lowercase character.";
      }

      const isContainsNumber = /^(?=.*[0-9]).*$/;
      if (!isContainsNumber.test(value)) {
        return "Password must contain at least one digit.";
      }

      const isContainsSymbol =
        /^(?=.*[~`!@#$%^&*()--+={}\[\]|\\:;"'<>,.?/_₹]).*$/;
      if (!isContainsSymbol.test(value)) {
        return "Password must contain at least one special symbol.";
      }

      const isValidLength = /^.{8,15}$/;
      if (!isValidLength.test(value)) {
        return "Password must be 8-15 characters long.";
      }
      return null;
  }
// ################# Validation Functions End   ###################


// ---------------- Show Loader Start ---------------------
const showLoader = () => {
   const loader = `<div class="loadingContainer">
     <div class="loading">
       <div class="dot"></div>
       <div class="dot"></div>
       <div class="dot"></div>
       <div class="dot"></div>
       <div class="dot"></div>
     </div>
     <h2 class="mt-3 text-white">Loading.....</h2>
    </div>`;

    $("body").after(loader);
    $("body").addClass("overflow-hidden");
}

let hideLoader = () => {
   $(".loadingContainer").remove();
   $("body").removeClass("overflow-hidden");
}


