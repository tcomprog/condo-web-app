<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\RequestController;
use App\Http\Controllers\VotingController;
use App\Http\Controllers\PetitionController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;


Route::get('clear-cache',function(){
    Cache::flush();
});

// ***************** Auth Routes ********************
Route::get('/', function(){
    $detect = new \Detection\MobileDetect();
    try{
        $isMobile = $detect->isMobile();
        $isTablet = $detect->isTablet(); 

        if($isMobile){
            return view('m_home');
        }
        elseif($isTablet){
            return view('home');
        }
        else{
            return view('home');
        }
    }
    catch(Exception $e){
        return view('home');
    }
})->name("homepage");

Route::get('/login', [AuthController::class,'indexPage'])->name("login");
Route::get('/logout', [AuthController::class,'logoutUser'])->name('logout');
Route::get('/register', [AuthController::class,'registerPage']);
Route::get('/forgot-pas', [AuthController::class,'forgotPasswordPage']);
Route::get("verify-mail/{id}/{hash}",[AuthController::class,'verifyUserEmail']);
Route::get("change-password/{id}/{hash}",[AuthController::class,'changePasswordPage']);

Route::post('/register-user', [AuthController::class,'registerUser'])->name('register-user');
Route::post('/login-user', [AuthController::class,'loginUser'])->name('login-user');
Route::post('/forgot-password', [AuthController::class,'forgotPassword'])->name('forgot-password');
Route::post('/resend-verif-email', [AuthController::class,'resendVerificationEmail'])->name('resend-verif-email');
Route::post('/update_new_password', [AuthController::class,'updateUserPassword'])->name('update_new_password');

Route::post('/contact_us', [AuthController::class,'contactUs'])->name("contact_us");


// ################### Routes For Loged In Users ######################
Route::middleware(["auth"])->group(function(){

    Route::get('/dashboard', [AdminController::class,'index'])->name("dashboard");
    Route::get('/admin-profile', [AdminController::class,'adminProfile'])->name("admin-profile");
    Route::get('/society-policy', [AdminController::class,'societyPolicy'])->name("society-policy");
    Route::get('/send-alert-page', [AdminController::class,'sendAlertPage'])->name("send-alert-page");
    Route::get('/user-list/{type?}', [UserController::class,'userList'])->name("user-list");
    Route::get('/add-user', [UserController::class,'addUser'])->name("add-user");
    Route::get('/pending-user', [UserController::class,'pendingList'])->name("pending-user");
    Route::get('/user-profile/{id}', [UserController::class,'userProfile'])->name("user-profile");
    Route::get('/re-send-auth-email/{id}', [UserController::class,'reSendUserAuthEmail'])->name("re-send-auth-email");
    Route::get('/delete-user/{id}', [UserController::class,'deleteUser'])->name("delete-user");
    Route::get('/reactive-user/{id}', [UserController::class,'reactiveUser'])->name("delete-user");
    Route::get('/update-user-page/{id}', [UserController::class,'updateUserPage'])->name("update-user-page");
    Route::get('/send-user-email/{id}', [UserController::class,'sendEmailToUserPage'])->name("send-user-email");
    Route::get('/deletePendingRequest/{id}', [UserController::class,'deletePendingRequest'])->name("deletePendingRequest");
    Route::get('/acceptAccountRequest/{id}', [UserController::class,'acceptAccountRequest'])->name("acceptAccountRequest");
    Route::get('/addevents/{id?}', [EventController::class,'addEvent'])->name("addevents");
    Route::get('/eventDetails/{id?}', [EventController::class,'eventDetails'])->name("eventDetails");
    Route::get('/event-list', [EventController::class,'eventList'])->name("event-list");
    Route::get('/request-list/{status?}', [RequestController::class,'index'])->name("request-list");
    Route::get('/request-details/{id}', [RequestController::class,'showRequestDetailsPage'])->name("request-details");
    Route::get('/add-voting', [VotingController::class,'index'])->name("add-voting");
    Route::get('/voting-list/{status?}', [VotingController::class,'votingList'])->name("voting-list");
    Route::get('/voting-details/{id}', [VotingController::class,'showVotingDetailsPage'])->name("voting-details");
    Route::get('/delete-poll/{id}', [VotingController::class,'deleteVotingPoll'])->name("delete-poll");
    Route::get('/complete-poll/{id}', [VotingController::class,'completeVotingPoll'])->name("complete-poll");
    Route::get('/change-status/{id}/{status}', [VotingController::class,'changeVotingPollStatus'])->name("change-status");
    Route::get('/petition-list/{status?}', [PetitionController::class,'index'])->name("petition-list");
    Route::get('/petition-details/{id}', [PetitionController::class,'showPetitionDetailsPage'])->name("petition-details");


    Route::post('/addNewUser', [UserController::class,'addNewUser'])->name("addNewUser");
    Route::post('/updateUser', [UserController::class,'updateUser'])->name("updateUser");
    Route::post('/sendUserEmail', [UserController::class,'sendUserEmail'])->name("sendUserEmail");
    Route::post('/changeAdminPassword', [AdminController::class,'changeAdminPassword'])->name("changeAdminPassword");
    Route::post('/updateProfile', [AdminController::class,'updateProfile'])->name("updateProfile");
    Route::post('/savePrivacyPolicy', [AdminController::class,'savePrivacyPolicy'])->name("savePrivacyPolicy");
    Route::post('/saveAlert', [AdminController::class,'saveAlert'])->name("saveAlert");
    Route::post('/saveEvent', [EventController::class,'saveEvent'])->name("saveEvent");
    Route::post('/deleteEvent', [EventController::class,'deleteEvent'])->name("deleteEvent");
    Route::post('/changeRequestStatus', [RequestController::class,'changeRequestStatus'])->name("changeRequestStatus");
    Route::post('/addVoting', [VotingController::class,'addVoting'])->name("addVoting");
    Route::post('/changePetitionStatus', [PetitionController::class,'changePetitionStatus'])->name("changePetitionStatus");

});


// =============== Testing Section ===============
Route::get("/testing",function(){
   // return view("email.user_account_created");
    return view('m_home');
});

Route::get("/404-error",function(){
   // return view("email.user_account_created");
    return view('pages.page_404');
})->name('404-error');