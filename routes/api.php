<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\App_UserController;
use App\Http\Controllers\App_DropDownList_For_Mobile;
use App\Http\Controllers\App_DashboardController;
use App\Http\Controllers\App_ProfileController;
use App\Http\Controllers\App_SocietyMemberController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\App_MessageController;
use App\Http\Controllers\App_RequestController;
use App\Http\Controllers\App_VotingController;
use App\Http\Controllers\App_PetitionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// CALL WITHOUT AUTH
Route::post('/login',[App_UserController::class,"loginUser"]);
Route::post('/register',[App_UserController::class,"registerUser"]);
Route::post('/forgotPassword',[App_UserController::class,"forgotPassword"]);
Route::post('/changePassword',[App_UserController::class,"changePassword"]);

Route::post('/streamCallResponse',[App_UserController::class,"streamCallResponse"]);

// CALL WITHOUT AUTH [DROP DOWN DATA]
Route::get('/society_list',[App_DropDownList_For_Mobile::class,"getSocietyList"]);


// CALL WITH AUTH
Route::middleware(["check_access_token"])->group(function(){
    
    // GET USER CALL
    Route::get('/logout',[App_UserController::class,"logoutUser"]);
    Route::get('/deleteUserAccount',[App_UserController::class,"deleteUserAccount"]);
    Route::get('/getProfileData',[App_ProfileController::class,"getProfileData"]);
    Route::get('/membersList',[App_SocietyMemberController::class,"membersList"]);
    Route::get('/getSocietyPlicy',[App_DashboardController::class,"getSocietyPlicy"]);
    Route::get('/getOwnVotingList',[App_VotingController::class,"getOwnVotingList"]);
    Route::get('/getPetitionList',[App_PetitionController::class,"getPetitionList"]);
    Route::get('/getMemberLocationList',[App_SocietyMemberController::class,"getMemberLocationList"]);


    // POST USER CALL
    Route::post('/dashboard',[App_DashboardController::class,"index"]);
    Route::post('/updatePassword',[App_ProfileController::class,"updatePassword"]);
    Route::post('/updateProflieData',[App_ProfileController::class,"updateProflieData"]);
    Route::post('/updateProfileImage',[App_ProfileController::class,"updateProfileImage"]);
    Route::post('/getEventDetails',[EventController::class,"getEventDetails"]);
    Route::post('/getEventList',[EventController::class,"getEventList"]);
    Route::post('/deleteSingleRow',[App_MessageController::class,"deleteSingleRow"]);
    Route::post('/deleteAllRows',[App_MessageController::class,"deleteAllRows"]);
    Route::post('/getTabScreenListing',[App_MessageController::class,"getTabScreenListing"]);
    Route::post('/submitRequest',[App_RequestController::class,"submitRequest"]);
    Route::post('/getRequestDetails',[App_RequestController::class,"getRequestDetails"]);
    Route::post('/getVotingList',[App_VotingController::class,"getVotingList"]);
    Route::post('/submitVoting',[App_VotingController::class,"submitVoting"]);
    Route::post('/addVotingPoll',[App_VotingController::class,"addVotingPoll"]);
    Route::post('/getVotingDetails',[App_VotingController::class,"getVotingDetails"]);
    Route::post('/completePoll',[App_VotingController::class,"completePoll"]);
    Route::post('/submitPetition',[App_PetitionController::class,"submitPetition"]);
    Route::post('/getPetitionDetails',[App_PetitionController::class,"getPetitionDetails"]);
    Route::post('/getRequestList',[App_RequestController::class,"getRequestList"]);
    Route::post('/sendMessage',[App_MessageController::class,"sendMessage"]);
    Route::post('/getMessageDetailsListing',[App_MessageController::class,"getMessageDetailsListing"]);
    Route::post('/submitFileMessage',[App_MessageController::class,"submitFileMessage"]);
    Route::post('/getMemberProfileData',[App_SocietyMemberController::class,"getMemberProfileData"]);

});
