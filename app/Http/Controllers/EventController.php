<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\EventModel;
use App\Models\SocietyUser;
use App\Models\AccessToken;
use Auth;
use Helper;
use App\Helper\ApiResponse;
use Constants;

class EventController extends Controller
{
  use ApiResponse;

    public function addEvent($id = -1) : View{
      if($id > 0){
        $event = EventModel::where('id',$id)->where('society_id',Auth::id())->get();
        if(count($event) > 0){
          return view('pages.events.add_event',['event'=>$event[0]]);
        }
        else{
         return redirect()->route('404-error'); 
        }

      }else{
        return view('pages.events.add_event');
      }
    }

    public function saveEvent(Request $request){
      
        $validated = $request->validate([
            'name' => 'required|max:150',
            'shortDes' => 'required',
            'eventdate' => 'required',
            'eventTime' => 'required',
            'event_image' => 'image|mimes:jpg,png,jpeg,gif|max:2048',
          ],[
            "name.required" => "Event name field is required",
            "eventTime.required" => "Event time field is required",
            "shortDes.required" => "Event short description field is required",
            "eventdate.required" => "Event date field is required",
            "name.max" => "Event name length shouldn`t be greater than 150 characters",
          ]);

          $name = $request->name;
          $eventdate = date('Y-m-d',strtotime($request->eventdate));
          $shortDes = $request->shortDes;
          $description = $request->description;
          $eventTime = $request->eventTime;
          $event_image = $request->event_image;
          $imagePath =  '';

          if($request->has('eventID')){
            //update
            $eventID = $request->eventID;
            $event = EventModel::where('id',$eventID)->where('society_id',Auth::id())->get();

            if(count($event) <= 0){
              return redirect()->route('404-error'); 
            }

            $event = $event[0];

            if($request->has("event_image") && !empty($event_image)){  
              if(!empty($event->image)){
                $deltePath = public_path('upload/event/'.$event->image);
 
                if(file_exists($deltePath)){
                  unlink($deltePath);
                }
             }

              $imagePath =  Auth::id().'_'.Helper::randomString(30).'_'.time().'.'.$request->event_image->extension();
              $request->event_image->move(public_path('upload/event'), $imagePath);
              $event->image = $imagePath;
            }

            $eventTime =  explode(":",$eventTime);
            $eventTime = $eventTime[0].":".$eventTime[1];

            $event->name = $name;
            $event->des = $description;
            $event->eventTime = $eventTime;
            $event->event_date = $eventdate;
            $event->short_desc = $shortDes;
            $event->update();

            return redirect()->route('addevents', ['id'=>$event->id,'status' => "success",'toastType' => '0','message'=>"Event updated successfully"]); 

          }else{

          if($request->has("event_image") && !empty($event_image)){    
            $imagePath =  Auth::id().'_'.Helper::randomString(30).'_'.time().'.'.$request->event_image->extension();
            $request->event_image->move(public_path('upload/event'), $imagePath);
          }
      
          $event = new EventModel();
          $event->name = $name;
          $event->des = $description;
          $event->image = $imagePath;
          $event->eventTime = $eventTime;
          $event->event_date = $eventdate;
          $event->short_desc = $shortDes;
          $event->society_id = Auth::id();
          $event->save();

          //Alert should be send to all society member

          $society_member = SocietyUser::select('id')->where('society_id',Auth::id())->where('status','active')->get();
          foreach($society_member as $row){
                $notif_data = [
                  'title' => 'New event!', 
                  'body' => 'Your society admin has added new event you might be interested.', 
                  'page' => 'eventdetails', 
                  'page_payload' => '{"id":'.$event->id.'}',
                  'from_type' => '0', 
                  'society_id' => Auth::id(), 
                  'to_id' => $row->id, 
                  'type' => '2', 
                  'from_id' => '0', 
              ];
    
              Helper::sendNotification($notif_data);
          }
    
          $active_member = AccessToken::select('user_id','fcmToken')->where('society_id',Auth::id())->get();
          $fcmToken = [];
          foreach($active_member as $row){
              $fcmToken[] = $row->fcmToken;
          }
    
          $data = [
            'notification' => 'true',
            'action' => 'event_added',
            'title' => "New event!",
            'body' => 'Your society admin has added a new event you might be interested.',
            'type' => '2'
          ];
          Helper::sendFCM($data,$fcmToken);

          return redirect()->route('addevents', ['status' => "success",'toastType' => '0','message'=>"Event added successfully"]);
        }
    }
    
    public function eventList():View{
      $events = EventModel::where('society_id',Auth::id())->orderBy('id','desc')->get();
      return view('pages.events.event_list',[
        'events' => $events
      ]);
    }

    public function deleteEvent(Request $request){
       $id = $request->id;
       EventModel::where('id',$id)->where('society_id',Auth::id())->delete();
       return response()->json(['status'=>'success','message'=>'Event deleted successfully']);
    }

    public function eventDetails($id){
       $event = EventModel::where('id',$id)->where('society_id',Auth::id())->get();
       if(count($event) > 0){
        return view('pages.events.event_details',['event'=>$event[0]]); 
       }
       else{
        return redirect()->route('404-error'); 
       }
    }

    public function getEventDetails(Request $request){
      $eventID = $request->eventID;
      $society_id = $request->society_id;

      $event = EventModel::where('id',$eventID)->where('society_id',$society_id)->get();
      if(count($event) > 0){
        $event = $event[0];
        $image = asset("upload/event/".$event->image);
 
        if(!file_exists(public_path()."/upload/event/".$event->image) || empty($event->image)){
          $image = asset("images/no_event_image.jpg");
        }

        $eventDate = explode('-',$event->event_date);
        $eventDate = $eventDate[2]." ".Helper::getMonths($eventDate[1])." ".$eventDate[0]." ".$event->eventTime;

        return $this->success([
          'id' => $event->id,
          'desc' => $event->des,
          'name' => $event->name,
          'short_des' => $event->short_desc,
          'date' => $eventDate,
          'image' => $image,
      ],'Successfull');
      }
      else{
        return $this->error([],Constants::constant("INVALID_DATA"),200);
      }
    }

    public function getEventList(Request $request){
       $society_id = $request->society_id;
       $orderBy = $request->orderBy; // 1 - All, 2 - New, 3 - Old

       $date = date("Y-m-d");
       if($orderBy == 3){
         $events = EventModel::where('society_id',$society_id)->where('event_date','>=',$date)->orderBy('id','asc')->get();
       }
       elseif($orderBy == 2){
         $events = EventModel::where('society_id',$society_id)->where('event_date','>=',$date)->orderBy('id','desc')->get();
       }
       else{
         $events = EventModel::where('society_id',$society_id)->where('event_date','>=',$date)->get();
       }

       $event_array = [];

       foreach($events as $row){
        $image = asset("upload/event/".$row->image);

        if(!file_exists(public_path()."/upload/event/".$row->image) || empty($row->image)){
          $image = asset("images/no_event_image.jpg");
        }

        $eventDate = explode('-',$row->event_date);
        $eventDate = $eventDate[2]." ".Helper::getMonths($eventDate[1])." ".$eventDate[0]." ".$row->eventTime;

       array_push($event_array,[
         'id' => $row->id,
         'name' => $row->name,
         'short_des' => $row->short_desc,
         'date' => $eventDate,
         'image' => $image,
       ]);
     }

     return $this->success($event_array,'Successfull');
    }
}
