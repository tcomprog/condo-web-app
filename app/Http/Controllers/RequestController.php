<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\Models\RequestModel;
use App\Models\AccessToken;
use Auth;
use Helper;
use Constants;

class RequestController extends Controller
{
    public function index($status = -1) : View{
      $request_data = RequestModel::where('society_id',Auth::id());
      if($status >= 0 && $status <= 2){
        $request_data = $request_data->where('status',$status);
      }
      $request_data = $request_data->select('id','title','type','status','created_at','requestType')->orderBy("id","desc")->get();

      return view('pages.request.request_list',['request_data'=>$request_data]); 
    }
    
    public function showRequestDetailsPage($id = 0){
        $request_detail = RequestModel::where('society_id',Auth::id())->where("id",$id)->get();

        if(count($request_detail) > 0){
            return view('pages.request.request_details',['request_detail'=>$request_detail[0]]); 
        }else{
            return redirect()->route('404-error'); 
        }
    }

    public function changeRequestStatus(Request $request){
       $request_id = $request->request_id;
       $comment = $request->comment;
       $status = $request->status;
       $status = $status == "approve" ? '1' : '2';
       $status_text = $status == "1" ? 'approved' : 'rejected';

       $request_detail = RequestModel::where('society_id',Auth::id())->where("id",$request_id)->get();

       if(count($request_detail) > 0){
          $request_detail = $request_detail[0];
          $request_detail->status = $status;
          $request_detail->adminComment = $comment;
         $request_detail->update();

          $mobile_login = AccessToken::where('user_id',$request_detail->user_id)->where("society_id",Auth::id())->get();
          if(count($mobile_login) > 0){
           $mobile_login = $mobile_login[0];
           $fcmToken = [$mobile_login->fcmToken];
           $data = [
              'notification' => 'true',
              'action' => 'request_status_changed',
              'title' => "Your request status changeed",
              'body' => 'Your request '.$status_text.' by admin.',
              'type' => '2'
           ];
           Helper::sendFCM($data,$fcmToken);
         } 
         
         $notif_data = [
            'title' => 'Your request status changed!', 
            'body' => 'Your request '.$status_text.' by admin.', 
            'page' => 'requestdetailscreen',  // petitiondetailscreen
            'from_type' => '0', 
            'society_id' => Auth::id(), 
            'to_id' => $request_detail->user_id, 
            'type' => '2', 
            'from_id' => '0', 
            'page_payload' => '{"request_id":'.$request_id.'}',
        ];

        Helper::sendNotification($notif_data);

          return redirect()->route('request-details', ['id'=>$request_detail->id,'status' => "success",'toastType' => '0','message'=>"Request status changed successfully"]); 

        }else{
            return redirect()->route('404-error'); 
        }

    }
}
