<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RequestModel;
use App\Helper\ApiResponse;
use Auth;
use Helper;
use Constants;

class App_RequestController extends Controller
{
    use ApiResponse;
   
    public function submitRequest(Request $request){
       $user_id = $request->user_id;
       $society_id = $request->society_id;
       $title = $request->title;
       $type = $request->type;
       $desc = $request->desc;
       $requestType = $request->requestType;

       $req = new RequestModel();
       $req->user_id = $user_id;
       $req->society_id = $society_id;
       $req->status = '0';
       $req->title = $title;
       $req->type = $type;
       $req->desc = $desc;
       $req->requestType = $requestType;
       $req->adminComment = '';
       $req->save();

       return $this->success([],"success",200);
    }

    public function getRequestList(Request $request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $requestType = $request->requestType;

       $request_data = RequestModel::where('requestType',$requestType)->where('user_id',$user_id)->where('society_id',$society_id)
       ->select('id','title','status','type','created_at','requestType')->orderBy('id','desc')->get();

       $request_array = [];
       foreach($request_data as $row){
         $request_array[] = [
           "id" => $row->id,
           "title" => $row->title,
           "type" => $row->type,
           "status" => $row->status,
           "requestType" => $row->requestType,
           "date" => date("d/m/Y",strtotime($row->created_at)),
         ];
       }

       return $this->success($request_array,"");
    }

    public function getRequestDetails(Request $request){
      $user_id = $request->user_id;
      $society_id = $request->society_id;
      $request_id = $request->request_id;

      $request_data = RequestModel::where('user_id',$user_id)->where('society_id',$society_id)->where('id',$request_id)->get();
      if(count($request_data) > 0){
        $request_data = $request_data[0];

         return $this->success([
          "id" => $request_data->id,
          "title" => $request_data->title,
          "type" => $request_data->type,
          "status" => $request_data->status,
          "desc" => $request_data->desc,
          "adminComment" => $request_data->adminComment,
          "requestType" => $request_data->requestType,
          "date" => date("d/m/Y",strtotime($request_data->created_at)),
         ],"");
      }
      else{
        return $this->error([],Constants::constant("UNAUTHORIZED_ACCESS"),200);
      }
    }

}
