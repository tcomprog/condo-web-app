<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Constants;
use Helper;
use Encryption;
use App\Models\User;
use Auth;
use DB;
use Illuminate\Support\Str;
use App\Mail\VerifyUserEmail;
use App\Mail\ResetPasswordEmail;
use App\Mail\ContactUsMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon; 


class AuthController extends Controller
{
    public function indexPage()  {
        if(Auth::check()){
           return redirect()->route('dashboard');
        }
        else{
            return view('auth.login');
        }
    }

    public function registerPage() : View{
        return view('auth.register');
    }

    public function forgotPasswordPage() : View{
        return view('auth.forgot_password');
    }

    public function logoutUser(Request $request) {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');  
    }

    public function loginUser(Request $request){
       $email = $request->email;
       $password = $request->password;
       $remember_me = $request->remember_me;

       $params = [
        'email' => $email,
        'password' => $password,
       ];

       if(Auth::attempt($params)){
            if($remember_me == 1){
                setcookie("condo_email", Encryption::encrypt($email), time() + (86400 * 360), "/");
                setcookie("condo_password", Encryption::encrypt($password), time() + (86400 * 360), "/");
                setcookie("condo_remember", 1, time() + (86400 * 360), "/");
            }
         
          $user = Auth::user();
          if($user->status == "active"){
            return response()->json(["status"=>'success','message'=>"Credentials verified successfully"]);
          }
          else{
            Auth::logout();
            return response()->json([
                "status"=>'inactive',
                'message'=>"Plase verify your email before accessing the system",
                "u_email" => Crypt::encryptString($email),
                "u_password" => Crypt::encryptString($password),
              ]);
          }

       }
       else{
          return response()->json(["status"=>'invalid','message'=>"Invalid username or password"]);
       }
    }

    public function resendVerificationEmail(Request $request){
        $email = Crypt::decryptString($request->email);
        $password = Crypt::decryptString($request->password);
        
        if(Auth::attempt(['email' => $email, 'password' => $password])){
            $user = Auth::user();

            if($user->status == 'active'){
                Auth::logout();
                return response()->json(["status"=>'already','message'=>"Your email is already authenticated"]);
            }
            else{
                Mail::to($email)->send(new VerifyUserEmail([
                    'email' => $email,
                    'id' => $user->id,
                    'token' => $user->auth_token,
                    'name' => $user->name
                ]));
    
                Auth::logout();
                return response()->json(["status"=>'success','message'=>"Authentication email has been sent successfully"]);
            }
        }
        else{
            return response()->json(["status"=>'invalid','message'=>"Invalid access"]);
        }
    }

    public function registerUser(Request $request){
       $your_name = $request->your_name;
       $society_name = $request->society_name;
       $address = $request->address;
       $phone_no = $request->phone_no;
       $email = $request->email;
       $password = $request->password;
       $remember_token = Str::random(90);
    

       $checkUser = User::where('email',$email)->get();
       if(count($checkUser) > 0){
          return response()->json(["status"=>"exist","message"=>Constants::constant("USER_EXIST")]);
       }
       else{
            $user = new User();
            $user->name = $your_name;
            $user->society_name = $society_name;
            $user->address = $address;
            $user->phone_no = $phone_no;
            $user->email = $email;
            $user->remember_token = $remember_token;
            $user->auth_token = $remember_token;
            $user->status = 'inactive';
            $user->password = Hash::make($password);
            $user->save();

            Mail::to($email)->send(new VerifyUserEmail([
                'email' => $email,
                'id' => $user->id,
                'token' => $remember_token,
                'name' => $your_name
            ]));

            return response()->json(["status"=>"success","message"=>Constants::constant("ACCOUNT_CREATED")]);
      }
    }

    public function forgotPassword(Request $request){
      $email = $request->email;
      $user = User::where('email',$email)->where('status','active')->get();

      if(count($user) > 0){
        $token = Str::random(64);

        DB::table('password_resets')->where('email',$request->email)->delete();

        DB::table('password_resets')->insert([
            'email' => $request->email, 
            'token' => $token, 
            'created_at' => Carbon::now()
        ]);

        $user = $user[0];
        Mail::to($email)->send(new ResetPasswordEmail([
            'email' => $email,
            'id' => $user->id,
            'token' => $token,
            'name' => $user->name
        ]));

        return response()->json(['status'=>'success','message'=>"Reset password email has been sent successfully"]);
      }
      else{
        return response()->json(['status'=>'invalid','message'=>"Invalid email address"]);
      }
     
    }

    public function changePasswordPage($id,string $hash){
       return view('auth.change_password',['token'=>$hash]);
    }

    public function verifyUserEmail($id,string $hash):View{
        $date = date("Y-m-d h:i:s");

        $user = User::where('id',$id)->where('auth_token',$hash)->where('status','inactive')->get();

        if(count($user) > 0){
          $user = $user[0];
          $user->status = 'active';
          $user->email_verified_at = $date;
          $user->update();

           return view('pages.response.email_verification_response',['status'=>'success']);

        }else{
            return view('pages.response.email_verification_response',['status'=>'error']);
        }
    }

    public function updateUserPassword(Request $request){
        $email = $request->email;
        $password = $request->password;
        $token = $request->token;

        $updatePassword = DB::table('password_resets')->where('email',$email)->where('token',$token)->get();

        if(count($updatePassword) > 0){
            $user = User::where('email',$email)->first();
            $user->password = Hash::make($password);
            $user->update();

            DB::table('password_resets')->where('email',$email)->delete();

            return response()->json(['status'=>'success','message'=>"Your password has been changed successfully"]);
        }
        else{
            return response()->json(['status'=>'invalid','message'=>"Invalid token"]);
        }
    }


    public function contactUs(Request $request){
        $firstName = $request->firstName;
        $lastName = $request->lastName;
        $email = $request->email;
        $phoneNumber = $request->phoneNumber;
        $subject = $request->subject;
        $message = $request->message;

        $receiverMail = "mona.hcmg@gmail.com";

        Mail::to($receiverMail)->send(new ContactUsMail([
            'firstName' => $firstName,
            'lastName' => $lastName,
            'email' => $email,
            'phoneNumber' => $phoneNumber,
            'subject' => $subject,
            'messages' => $message
        ]));
       
        return response()->json(['status'=>'success','message'=>"Successfull!"]);
    }
}
