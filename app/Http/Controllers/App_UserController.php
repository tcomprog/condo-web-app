<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\Models\SocietyUser;
use App\Models\AccessToken;
use App\Models\ForgotPasswordOTP;
use App\Models\StremSDKTokenModel;
use App\Mail\ForgotPasswordOTPEmail;
use Illuminate\Support\Facades\Mail;
use App\Helper\ApiResponse;
use Constants;
use Auth;
use DB;
use Helper;
use Illuminate\Support\Facades\Storage;
use App\Models\MessageModel;

//  return $this->success($data=[],$message=null,$code=200);
//  return $this->error($data=[],$message=null,$code);

class App_UserController extends Controller
{
    use ApiResponse;

    public function deleteUserAccount(Request $request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $token = $request->token;

        AccessToken::where('user_id',$user_id)->where('society_id',$society_id)->where('token',$token)->delete();
        SocietyUser::where("id",$user_id)->where('society_id',$society_id)->update(['status'=>'deleted_by_user']);

        return $this->success([],Constants::constant("LOGOUT_MESSAGE"));
    }

    public function loginUser(Request $request){   
        // Just to check that actual developer is working with the Api and can be remove by commenting the if condition
        if(!$request->has('dev_email') || $request->dev_email != env("DEVELOPER_KEY")){
            return $this->error([],Constants::constant("DEVELOPER_ERROR"),200);
        }

         $validated = Validator::make($request->all(), [
         'email' => 'required|email:rfc,dns',
         'password' => 'required',
        ]);

       if($validated->fails()){
          return $this->error([],Constants::constant("INVALID_LOGIN_DATA"),400);
       }

        $email = $request->email;
        $password = $request->password;
        $dev_email = $request->dev_email;
        $platform = $request->platform;
        $fcmToken = $request->fcmToken;
        $latitude = $request->latitude;
        $longitude = $request->longitude;
        $deviceDetails = $request->deviceDetails;
        $deviceDetails = str_replace('"',"'",$deviceDetails);
        $deviceDetails = json_encode($deviceDetails);

        $user = SocietyUser::where("email",$email)->get();

        if(count($user) > 0){
            $user = $user[0];

            if(Hash::check($password,$user->password)){
               if($user->status == 'active'){
                $image = asset("upload/user/".$user->image);
                if(!file_exists(public_path()."/upload/user/".$user->image)){
                    $image = 'null';
                }

                $token_var = $user->id."_".$user->email."_".time()."_".$user->society_id."_".$user->created_at."_".$user->mobile;
                $token_var = Hash::make($token_var);
                $token_var .= Helper::randomString(70);
                $access_token = AccessToken::where("user_id",$user->id)->get();

                if(count($access_token) > 0){
                    $access_token = $access_token[0];
                    $access_token->token = $token_var;
                    $access_token->latitude = $latitude;
                    $access_token->longitude = $longitude;
                    $access_token->deviceDetails = $deviceDetails;
                    $access_token->update();
                }
                else{
                    $access_token = AccessToken::create([
                       'user_id' => $user->id,
                       'token' => $token_var,
                       'society_id' => $user->society_id,
                       'platform' => $platform,
                       'fcmToken' => $fcmToken,
                       'latitude' => $latitude,
                       'longitude' => $longitude,
                       'deviceDetails' => $deviceDetails,
                    ]);
                }

                $client = new \GetStream\StreamChat\Client(env("STREAM_API_KEY"), env("STREAM_API_SECRET"));
                $stream_token = $client->createToken("condo_".$user->id);


                $client->upsertUser([
                    "id" => "condo_".$user->id,
                    "name" => $user->name,
                    "email" => $user->email,
                    "mobile" => $user->mobile,
                    "society_id" => $user->society_id,
                    "fcm_token" => $fcmToken,
                  ]);

                StremSDKTokenModel::where('user_id',$user->id)->where('society_id',$user->society_id)->delete();
                $stream_sdk = new StremSDKTokenModel();
                $stream_sdk->token = $stream_token;
                $stream_sdk->user_id = $user->id;
                $stream_sdk->society_id = $user->society_id;
                $stream_sdk->save();

                 return $this->success([
                    'id' => $user->id,
                    'email' => $user->email,
                    'name' => $user->name,
                    'mobile' => $user->mobile,
                    'flat_details' => $user->flat_details,
                    'user_type' => $user->user_type,
                    'status' => $user->status,
                    'image' => $image,
                    'society_id' => $user->society_id,
                    'created_at' => date('d-m-Y',strtotime($user->created_at)),
                    'token_key' => $token_var,
                    'stream_token' => $stream_token
                 ],Constants::constant("LOGIN_SUCCESS")); 

               }
               elseif($user->status == 'pending' || $user->status == 'inactive'){
                 return $this->error([],Constants::constant("INACTIVE_ACCOUNT"),200); 
               }
               else{
                 return $this->error([],Constants::constant("INACTIVE_ACCOUNT"),200); 
               }
            }else{
                 return $this->error([],Constants::constant("INVALID_PASSWORD"),200); 
            }

            exit;
        }
        else{
            return $this->error([],Constants::constant("INVALID_USERNAME"),200); 
        }
    }
    
    
    public function logoutUser(Request $request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $token = $request->token;
        AccessToken::where('user_id',$user_id)->where('society_id',$society_id)->where('token',$token)->delete();
        StremSDKTokenModel::where('user_id',$user_id)->where('society_id',$society_id)->delete();
        return $this->success([],Constants::constant("LOGOUT_MESSAGE"));
    }

    public function registerUser(Request $request){

        $validated = Validator::make($request->all(), [
            'email' => 'required|email:rfc,dns',
            'name' => 'required',
            'mobile' => 'required',
            'falt_details' => 'required',
            'owner_type' => 'required',
            'owner_type' => 'required',
            'society_id' => 'required',
           ]);
   
          if($validated->fails()){
             return $this->error([],Constants::constant("MISSING_FIELDS"),400);
          }

        $name = $request->name;
        $email = $request->email;
        $mobile = $request->mobile;
        $falt_details = $request->falt_details;
        $owner_type = $request->owner_type;
        $society_id = $request->society_id;

        $check = SocietyUser::where('email',$email)->get();
        if(count($check) > 0){
            return $this->error([],Constants::constant("USER_EXIST"),400);
        }else{
            $user = new SocietyUser();
            $user->name = $name;
            $user->email = $email;
            $user->mobile = $mobile;
            $user->flat_details = $falt_details;
            $user->user_type = $owner_type;
            $user->society_id = $society_id;
            $user->status = 'pending';
            $user->password = '';
            $user->save();
           return $this->success([],Constants::constant("USER_REGISTER_SCCUESS"));
        }
    }

    public function forgotPassword(Request $request){
       $email = $request->email;
       $check = SocietyUser::where('email',$email)->get();

       if(count($check) > 0){
         $user = $check[0];
         $otp = rand(100000, 999999);
         $data = ['email'=>$email,'society_id'=>$user->id];

         $forgotPassword = ForgotPasswordOTP::where('society_id',$user->id)->where('society_email',$email)->delete();
        
         $forgotPassword = new ForgotPasswordOTP();
         $forgotPassword->otp = $otp;
         $forgotPassword->society_id = $user->id;
         $forgotPassword->society_email = $email;
         $forgotPassword->save();

         Mail::to($user->email)->send(new ForgotPasswordOTPEmail([
            'otp' => $otp,
            'name'=> $user->name,
        ]));

         return $this->success($data,Constants::constant("OPT_SENT"),200);
       }
       else{
        return $this->error([],Constants::constant("INVALID_EMAIL"),200);
      }
    }

    public function changePassword(Request $request){
        $society_id = $request->society_id;
        $society_email = $request->society_email;
        $otp = $request->otp;
        $password = $request->password;

        $validated = Validator::make($request->all(), [
            'society_id' => 'required|numeric',
            'society_email' => 'required',
            'otp' => 'required|numeric',
            'password' => 'required',
           ]);
   
          if($validated->fails()){
             return $this->error([],Constants::constant("INVALID_DATA"),200);
          }

          $check = ForgotPasswordOTP::where('society_id',$society_id)->where('society_email',$society_email)->where('otp',$otp)->get();
          if(count($check) > 0){
            $forgotPassword = $check[0];

            $otpDate = strtotime($forgotPassword->created_at." + 10 minute");
            $dateTime = strtotime(date("Y-m-d H:i:s",$otpDate));
            $now = strtotime(date('Y-m-d H:i:s')); 

            if($now <= $dateTime){
                $password = Hash::make($password); 
                SocietyUser::where('id',$society_id)->where('email',$society_email)->update(['password'=>$password]);
                $forgotPassword->delete();
                return $this->success([],Constants::constant("PASSWORD_CHANGED"),200);
            }
            else{
                return $this->error([],Constants::constant("OTP_EXPIRED"),200);
            }
          }
          else{
            return $this->error([],Constants::constant("INVALID_OTP"),200);
          }
    }


    public function streamCallResponse(Request $request){ 

       $request = json_encode($request->all());
       $request = json_decode($request);

        $eventType = $request->type;
        $call = $request->call;
        $customData = $call->custom;
        $setting = $call->settings->video;

        $from_id = $customData->sender;
        $to_id = $customData->receiver;
        $society_id = $customData->society_id;
        $msg_type = ($setting->enabled == 1 && $setting->access_request_enabled == 1) ? 5 : 4;

        $created = date('Y-m-d H:i:s',strtotime($call->created_at));
        $ended = date('Y-m-d H:i:s',strtotime($call->ended_at));
        $from_time = strtotime($created); 
        $to_time = strtotime($ended); 
        $diff_minutes = round(abs($from_time - $to_time) / 60,2);

        if($eventType == 'call.session_ended'){
            $message = new MessageModel();
            $message->from_id = $from_id;
            $message->to_id = $to_id;
            $message->society_id = $society_id;
            $message->msg_type = $msg_type;
            $message->read = '0';
            $message->call_minutes = $diff_minutes;
          //  $message->save();
        }

    }

}




