<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\ApiResponse;
use App\Models\PetitionModel;
use App\Models\SocietyUser;
use App\Models\AccessToken;
use Constants;
use Auth;
use DB;
use Helper;

class PetitionController extends Controller
{
     public function index($status = -1){
      $request_data = PetitionModel::where('society_id',Auth::id());
      if($status >= 0 && $status <= 2){
        $request_data = $request_data->where('status',$status);
      }
      $request_data = $request_data->select('id','title','type','status','created_at')->orderBy("id","desc")->get();

      return view('pages.petition.petition_list',['request_data'=>$request_data]); 
    }

    public function showPetitionDetailsPage($id = 0){
        $request_detail = PetitionModel::where('society_id',Auth::id())->where("id",$id)->get();

        if(count($request_detail) > 0){
             $memeber = SocietyUser::select('name','email')->where('id',$request_detail[0]->user_id)->first();
            return view('pages.petition.petition_details',['request_detail'=>$request_detail[0],'member'=>$memeber]); 
        }else{
            return redirect()->route('404-error'); 
        }
    }

     public function changePetitionStatus(Request $request){
       $request_id = $request->request_id;
       $comment = $request->comment;
       $status = $request->status;
       $status = $status == "approve" ? '1' : '2';
       $status_text = $status == "1" ? 'approved' : 'rejected';

       $request_detail = PetitionModel::where('society_id',Auth::id())->where("id",$request_id)->get();

       if(count($request_detail) > 0){
          $request_detail = $request_detail[0];
          $request_detail->status = $status;
          $request_detail->adminComment = $comment;
          $request_detail->update();

          $mobile_login = AccessToken::where('user_id',$request_detail->user_id)->where("society_id",Auth::id())->get();
          if(count($mobile_login) > 0){
           $mobile_login = $mobile_login[0];
           $fcmToken = [$mobile_login->fcmToken];
           $data = [
              'notification' => 'true',
              'action' => 'petition_status_changed',
              'title' => "Your petition status changeed",
              'body' => 'Your petition '.$status_text.' by admin.',
              'type' => '2'
           ];
           Helper::sendFCM($data,$fcmToken);
         } 
         
         $notif_data = [
            'title' => 'Your petition status changed!', 
            'body' => 'Your petition '.$status_text.' by admin.', 
            'page' => 'petitiondetailscreen', 
            'from_type' => '0', 
            'society_id' => Auth::id(), 
            'to_id' => $request_detail->user_id, 
            'type' => '2', 
            'from_id' => '0', 
            'page_payload' => '{"request_id":'.$request_id.'}',
        ];

        Helper::sendNotification($notif_data);

          return redirect()->route('petition-details', ['id'=>$request_detail->id,'status' => "success",'toastType' => '0','message'=>"File petition status changed successfully"]); 

        }else{
            return redirect()->route('404-error'); 
        }

    }
}
