<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VotingModel;
use App\Models\VotingOptionsModel;
use App\Models\MemberVotingModel;
use App\Models\AccessToken;
use Auth;
use Helper;
use Constants;
use DB;

class VotingController extends Controller
{
    public function index(){
        return view('pages.voting.add_voting'); 
    }

    public function addVoting(Request $request){
        $question = $request->question;
        $options = $request->options;

        $voting = new VotingModel();
        $voting->question = $question;
        $voting->society_id = Auth::id();
        $voting->user_id = '0';
        $voting->status = '1';
        $voting->addedBy = '0';
        $voting->save();

        foreach ($options as $key => $value) {
            $option = new VotingOptionsModel();
            $option->voting_id = $voting->id;
            $option->title = $value;
            $option->society_id = Auth::id();
            $option->save();
        }

        // sending notification to all user for voting
        return redirect()->route('add-voting', ['status' => "success",'toastType' => '0','message'=>"Voting poll added successfully."]);
    }

    public function deleteVotingPoll($id){
         $voting = VotingModel::where('id',$id)->where("society_id",Auth::id())->get();
        if(count($voting) > 0){
            $voting[0]->delete();
            VotingOptionsModel::where('society_id',Auth::id())->where('voting_id',$id)->delete();
            MemberVotingModel::where('society_id',Auth::id())->where('voting_id',$id)->delete();
            return redirect()->route('voting-list', ['status' => "success",'toastType' => '0','message'=>"Voting poll deleted successfully."]);
        }
        else{
           return redirect()->route('404-error'); 
        }
    }

    public function completeVotingPoll($id){
        $voting = VotingModel::where('id',$id)->where("society_id",Auth::id())->get();
        if(count($voting) > 0){
            $voting[0]->update(['status'=>'2']);
            return redirect()->route('voting-details', ['id'=>$id,'status' => "success",'toastType' => '0','message'=>"Voting poll completed successfully."]);
        }
        else{
           return redirect()->route('404-error'); 
        }
    }

    public function changeVotingPollStatus($id,$status){
       $voting = VotingModel::where('id',$id)->where("society_id",Auth::id())->get();
        if(count($voting) > 0){
          //  $voting[0]->update(['status'=>$status]);
            $msg = $status == 1 ? 'accepted' : 'rejected';
            
            $active_member = AccessToken::select('user_id','fcmToken')->where('society_id',Auth::id())->where('user_id',$voting[0]->user_id)->get();
            $fcmToken = [];
            foreach($active_member as $row){
                $fcmToken[] = $row->fcmToken;
            }

            $data = [
                'notification' => 'true',
                'action' => 'voting_poll_status',
                'title' => "Your voting poll status changed!",
                'body' => 'Your voting poll has been '.$msg.' by society admin.',
                'type' => '2'
              ];
              Helper::sendFCM($data,$fcmToken);

              $notif_data = [
                'title' => 'Your voting poll status changed!', 
                'body' => 'Your voting poll has been '.$msg.' by society admin.', 
                'page' => 'votingdetailscreen', 
                'from_type' => '0', 
                'society_id' => Auth::id(), 
                'to_id' => $voting[0]->user_id, 
                'type' => '2', 
                'from_id' => '0', 
                'page_payload' => '{"voting_id":'.$id.'}',
            ];
    
            Helper::sendNotification($notif_data);

            return redirect()->route('voting-details', ['id'=>$id,'status' => "success",'toastType' => '0','message'=>"Voting poll ".$msg." successfully."]);
        }
        else{
           return redirect()->route('404-error'); 
        }
    } 

    public function votingList($status = -1){
        if($status >= 0 && $status <= 3){
            $voting = VotingModel::where("society_id",Auth::id())->where('status',$status)->orderBy('id','desc')->get();
        }
        else{
            $voting = VotingModel::where("society_id",Auth::id())->orderBy('id','desc')->get();
        }
        return view('pages.voting.voting_list',['voting'=>$voting]); 
    }

    public function showVotingDetailsPage($id) {
        $voting = VotingModel::where('id',$id)->where("society_id",Auth::id())->get();

        $colors = ['#fed23e','#ca6146','#993e4c','#4c7571','#12363f'];
    
        if(count($voting) > 0){
            $voting = $voting[0];
            $options = VotingOptionsModel::where('voting_id',$voting->id)->where('society_id',Auth::id())->get();
            $op_array = [];
            $grap_votes = [];
            $graph_tags = [];
            $graph_colors = [];

            $voter_list = DB::table('votes_of_member')
            ->join('society_user', 'votes_of_member.user_id', '=', 'society_user.id')
            ->join('voting_option', 'voting_option.id', '=', 'votes_of_member.option_id')
            ->select('society_user.id','society_user.name','society_user.email', 
            'voting_option.title','votes_of_member.created_at')->where('votes_of_member.society_id',Auth::id())
            ->where('votes_of_member.voting_id',$id)->get();

            $total_votes = 0;

            foreach($options as $ind => $op){
              $votes = MemberVotingModel::select('id')->where('society_id',Auth::id())->where('voting_id',$id)->where('option_id',$op->id)->count();
              $total_votes += $votes;

              if($votes > 0){
                $grap_votes[] = $votes;
                $graph_tags[] = $op->title;
                $graph_colors[] = $colors[$ind];
              }
              
              $op_array[] = [
                'title' => $op->title,
                'votes' => $votes
              ];
            }

            return view('pages.voting.voting_details',[
                'voting' => $voting,
                'options' => $op_array,
                'total_votes' => $total_votes,
                'grap_votes' => $grap_votes,
                'graph_tags' => $graph_tags,
                'graph_colors' => $graph_colors,
                'voter_list' => $voter_list,
            ]); 
        }
        else{
            return redirect()->route('404-error'); 
        }

    }
}
