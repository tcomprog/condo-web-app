<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\SocietyUser;
use App\Models\EventModel;
use App\Models\AccessToken;
use App\Models\User;
use App\Helper\ApiResponse;
use App\Models\NotificationModal;
use App\Models\MessageModel;
use Constants;
use Auth;
use DB;
use Helper;

class App_DashboardController extends Controller
{
    use ApiResponse;

    public function index(Request $request){
        $searchQuery = $request->searchQuery;
 
        if($request->has('searchQuery') && !empty($searchQuery)){
          // Searching data here
          return $this->getSearchingData($request);
        }
        else{
          // Incase of normal data
          return $this->getDashboardData($request);
        }
    }

    public function getSearchingData($request) {
      $user_id = $request->user_id;
      $society_id = $request->society_id;
      $query = $request->searchQuery;
      
      $user = SocietyUser::where("id",$user_id)->where("society_id",$society_id)->get();
      if(count($user) > 0){
         $user = $user[0];
         $q = "SELECT id,name,image,email FROM `tbl_society_user` WHERE (name LIKE '%$query%' OR email LIKE '%$query%' OR mobile LIKE '%$query%') AND `society_id` = '$society_id' AND `status` = 'active' AND id != '$user_id' ORDER BY name asc;";
         $members = DB::select($q);
       
         $member_array = [];

         foreach($members as $row){
            $image = asset("upload/user/".$row->image);

            if(!file_exists(public_path()."/upload/user/".$row->image) || empty($row->image)){
              $image = asset("images/no_image.jpg");
            }

           array_push($member_array,[
             'id' => $row->id,
             'name' => $row->name,
             'image' => $image,
             'email' => $row->email,
           ]);
         }

         $date = date("Y-m-d");
         $q = "SELECT * FROM `tbl_events` WHERE `society_id` = '$society_id' AND (`name` LIKE '%$query%' OR `short_desc` LIKE '%$query%' OR `des` LIKE '%$query%') AND `event_date` >= '$date' ORDER BY event_date asc;";
         $events = DB::select($q);

         $event_array = [];
         foreach($events as $row){
             $image = asset("upload/event/".$row->image);

             if(!file_exists(public_path()."/upload/event/".$row->image) || empty($row->image)){
               $image = asset("images/no_event_image.jpg");
             }

             $eventDate = explode('-',$row->event_date);
             $eventDate = $eventDate[2]." ".Helper::getMonths($eventDate[1])." ".$eventDate[0]." ".$row->eventTime;

            array_push($event_array,[
              'id' => $row->id,
              'name' => $row->name,
              'short_des' => $row->short_desc,
              'date' => $eventDate,
              'image' => $image,
            ]);
          }

          $q = "SELECT id,`title`,`body`,`created_at` FROM `tbl_notification` WHERE society_id = '$society_id' AND `type` = '1' AND to_id = '$user_id' AND (`title` LIKE '%$query%' OR `body` = '%$query%') ORDER BY `created_at`;";

          $alerts = DB::select($q);
          $alert_array = [];
          foreach($alerts as $row){
             array_push($alert_array,[
               'id' => $row->id,
               'title' => $row->title,
               'body' => $row->body,
               'date' => date('d/m/Y',strtotime($row->created_at)),
             ]);
          }
          
         return $this->success([
          'member' => $member_array,
          'events' => $event_array,
          'alerts' => $alert_array,
      ],'Successfull');

      }
      else{
        return $this->error([],Constants::constant("INVALID_USER"),200);
      }
    }

    public function getDashboardData($request){
      $user_id = $request->user_id;
      $society_id = $request->society_id;
      $latitude = $request->latitude;
      $longitude = $request->longitude;

      $user = SocietyUser::where("id",$user_id)->where("society_id",$society_id)->get();

        if(count($user) > 0){
            $user = $user[0];

            AccessToken::where('user_id',$user_id)->where("society_id",$society_id)->update(['latitude'=>$latitude,'longitude'=>$longitude]);

            $image = asset("upload/user/".$user->image);
            if(!file_exists(public_path()."/upload/user/".$user->image)){
                $image = 'null';
            }

            $user_record = [
             'id' => $user->id,
             'email' => $user->email,
             'name' => $user->name,
             'mobile' => $user->mobile,
             'flat_details' => $user->flat_details,
             'user_type' => $user->user_type,
             'status' => $user->status,
             'image' => $image,
             'society_id' => $user->society_id,
             'created_at' => date('d-m-Y',strtotime($user->created_at)),
            ];

            $members = SocietyUser::select('id','name','image','email')->where("society_id",$society_id)->where('status','active')
            ->where("id",'!=',$user_id)->orderBy('id', 'desc')->limit(5)->get();
            $member_array = [];

            foreach($members as $row){
               $image = asset("upload/user/".$row->image);

               if(!file_exists(public_path()."/upload/user/".$row->image) || empty($row->image)){
                 $image = asset("images/no_image.jpg");
               }

              array_push($member_array,[
                'id' => $row->id,
                'name' => $row->name,
                'image' => $image,
                'email' => $row->email,
              ]);
            }

            $date = date("Y-m-d");
            $events = EventModel::where('society_id',$society_id)->where('event_date','>=',$date)->orderBy('id','desc')->limit(10)->get();
            $event_array = [];
            foreach($events as $row){
                $image = asset("upload/event/".$row->image);
 
                if(!file_exists(public_path()."/upload/event/".$row->image) || empty($row->image)){
                  $image = asset("images/no_event_image.jpg");
                }

                $eventDate = explode('-',$row->event_date);
                $eventDate = $eventDate[2]." ".Helper::getMonths($eventDate[1])." ".$eventDate[0]." ".$row->eventTime;
 
               array_push($event_array,[
                 'id' => $row->id,
                 'name' => $row->name,
                 'short_des' => $row->short_desc,
                 'date' => $eventDate,
                 'image' => $image,
               ]);
             }
             
            $alerts = NotificationModal::select('id','title','body','created_at')->where('society_id',$society_id)->where('to_id',$user_id)->where("type","1")->orderBy('id','desc')->limit(10)->get();
            $alert_array = [];
            foreach($alerts as $row){
               array_push($alert_array,[
                 'id' => $row->id,
                 'title' => $row->title,
                 'body' => $row->body,
                 'date' => date('d/m/Y',strtotime($row->created_at)),
               ]);
             }

            $society_user = User::select('id','society_name','email','address','phone_no','latitude','longitude')->where('id',$society_id)->get()[0]; 
            $society_user = [
                'id' => $society_user["id"],
                'society_name' => $society_user["society_name"],
                'email' => $society_user["email"],
                'address' => $society_user["address"],
                'phone_no' => $society_user["phone_no"],
                'latitude' => $society_user["latitude"],
                'longitude' => $society_user["longitude"],
            ];

           
            $notification_cnt = NotificationModal::where('to_id',$user_id)->where("status","0")->count();
            $message_count = MessageModel::where('to_id',$user_id)->where("read","0")->where('society_id',$society_id)->where('isDeleted','not like','%,'.$user_id.'%')->count();
            $tab_no = $message_count > 0 ? 3 : 2;
            $notification_cnt = $notification_cnt + $message_count;

            return $this->success([
                'user' => $user_record,
                'member' => $member_array,
                'society' => $society_user,
                'events' => $event_array,
                'alerts' => $alert_array,
                'notification_cnt' => $notification_cnt,
                'tab_no' => $tab_no,
            ],'Successfull');
        }
        else{
            return $this->error([],Constants::constant("INVALID_USER"),200);
        }
    }

    public function getSocietyPlicy(Request $request){
        $society_id = $request->society_id;
        $policy = DB::select("SELECT * FROM `tbl_privacy_policy` WHERE `society_id` = '$society_id';");

        if(count($policy) > 0){
           return $this->success([
                'policy' => $policy[0]->privacy,
            ],'Successfull');
        }
        else{
            return $this->success([
                'policy' => 'null',
            ],'Successfull');
        }
    }
}
