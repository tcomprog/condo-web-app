<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SocietyUser;
use Illuminate\Support\Str;
use Auth;
use App\Mail\AddNewSocietMember;
use App\Mail\SendUserEmail;
use App\Models\AccessToken;
use App\Models\RequestModel;
use App\Mail\AccountRequestAccept;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

use Helper;

class UserController extends Controller
{
    public function userList($type = 'active'){
        if($type != 'active' && $type != 'inactive'){
            return redirect('404-error');
        }
        $society_user = SocietyUser::where("society_id",Auth::user()->id)->where('status',$type)->get();
        $active_user = [];
        $active = AccessToken::select('user_id')->where('society_id',Auth::id())->get();
        foreach($active as $id){
            $active_user[] = $id->user_id;
        }
        return view('pages.user.user_list',[
            'user_list' => $society_user,'type'=>$type,'active_user' => $active_user
        ]);
    }

    public function addUser(){
        return view('pages.user.add_user');
    }

    public function addNewUser(Request $request){
       $email = $request->email;
       $name = $request->name;
       $mobile = $request->mobile;
       $flat_details = $request->flat_details;
       $user_type = $request->user_type;
       $profile_image = $request->profile_image;
       $society_id = Auth::user()->id;
       $password = Str::random(8);

      $validated = $request->validate([
        'email' => 'required|email:rfc,dns|max:100',
        'name' => 'required|max:200',
        'mobile' => 'required|max:20',
        'flat_details' => 'required|max:200',
        'user_type' => 'required',
        'profile_image' => 'image|mimes:jpg,png,jpeg,gif|max:2048',
      ]);

        $check_user = SocietyUser::where('email',$email)->get();
        if(count($check_user) > 0){
            return redirect()->route('add-user', ['status' => "error",'toastType' => '1','message'=>"Society user already exist with provided email"]); 
        }

        $user = new SocietyUser();
        $user->email = $email;      
        $user->name = $name;      
        $user->mobile = $mobile;      
        $user->flat_details = $flat_details;      
        $user->user_type = $user_type;      
        $user->society_id = $society_id;    
        $user->password = Hash::make($password);      
        $user->status = "active"; 

        if($request->has("profile_image") && !empty($profile_image)){    
            $file_name = Auth::user()->id."_".time()."_".Str::random(50).'.'.$request->profile_image->extension();
            $request->profile_image->move(public_path('upload/user'), $file_name);
            $user->image = $file_name;
        }

        $user->save();  

        Mail::to($email)->send(new AddNewSocietMember([
            'username' => $email,
            'password' => $password,
            'name' => $name,
            'flat_details' => $flat_details,
            'society_name' => Auth::user()->society_name,
            'created_by' => Auth::user()->name,
            'subject'=> 'Your account created in '.Auth::user()->society_name." society",
            "message" => "Your account has been created and login details are listed below."
           ]));

        return redirect()->route('add-user', ['status' => "success",'toastType' => '0','message'=>"Account created successfully and credentials has been sent on the provided email"]);
   }

   public function reSendUserAuthEmail($id){
      $user = SocietyUser::where('id',$id)->where("society_id",Auth::id())->get();
      if(count($user) > 0){
         $user = $user[0];
         $password = Str::random(8);

         $user->password = Hash::make($password);
         $user->update();

        Mail::to($user->email)->send(new AddNewSocietMember([
            'username' => $user->email,
            'password' => $password,
            'name' => $user->name,
            'flat_details' => $user->flat_details,
            'society_name' => Auth::user()->society_name,
            'created_by' => Auth::user()->name,
            'subject'=> 'Your password change by - '.Auth::user()->name,
            "message" => "Your password has been changed and new login are listed below."
           ]));

           $mobile_login = AccessToken::where('user_id',$user->id)->where("society_id",Auth::id())->get();
           if(count($mobile_login) > 0){
               $mobile_login = $mobile_login[0];
               $fcmToken = [$mobile_login->fcmToken];
               $data = [
                  'notification' => 'true',
                  'action' => 'user_account_deleted',
                  'title' => "Account deleted",
                  'body' => 'Your account has been deleted by admin.',
                  'type' => '1',
               ];
               $mobile_login->delete();
               Helper::sendFCM($data,$fcmToken);
           }   

          return redirect()->route('user-profile', ["id"=>$user->id,'status' => "success",'toastType' => '0','message'=>"New username and password email has been sent"]);
      }
      else{
          return redirect()->route('user-list', ['status' => "error",'toastType' => '1','message'=>"Invalid access"]);
      }
   }

   public function userProfile($id){
    $check_user = SocietyUser::where('id',$id)->where("society_id",Auth::id())->get();
    if(count($check_user) <= 0){
        return redirect()->route('user-list', ['status' => "error",'toastType' => '1','message'=>"Invalid access"]); 
    }

    $mobile_login = AccessToken::where('user_id',$id)->where("society_id",Auth::id())->get();
    if(count($mobile_login) > 0){
       $isMobileLogin = true;
       $mobile_login = $mobile_login[0];
    }
    else{
      $isMobileLogin = false;
    }

    $request_data = RequestModel::where('society_id',Auth::id())->where('user_id',$id)->select('id','title','type','status','created_at')->orderBy("id","desc")->get();

      $user = $check_user[0];
      return view("pages.user.user_profile",
      [
        "user"=>$user,
        'isMobileLogin'=>$isMobileLogin,
        "mobileLoginData"=>$mobile_login,
        'request_data'=>$request_data
    ]);
   }

   public function deleteUser($id){
        $check_user = SocietyUser::where('id',$id)->where("society_id",Auth::id())->get();
        if(count($check_user) <= 0){
            return redirect()->route('user-profile', ['status' => "error",'toastType' => '1','message'=>"Invalid access"]); 
        }

        $user = $check_user[0];
        $user->status = 'inactive';
        $user->update();

        $mobile_login = AccessToken::where('user_id',$user->id)->where("society_id",Auth::id())->get();
        if(count($mobile_login) > 0){
            $mobile_login = $mobile_login[0];
            $fcmToken = [$mobile_login->fcmToken];
            $data = [
               'notification' => 'true',
               'action' => 'user_account_deleted',
               'title' => "Account deleted",
               'body' => 'Your account has been deleted by admin.',
               'type' => '1'
            ];
            $mobile_login->delete();
            Helper::sendFCM($data,$fcmToken);
        }

        return redirect()->route('user-profile', ['id'=>$user->id,'status' => "success",'toastType' => '0','message'=>"User account delete successfully"]); 
   }

   public function reactiveUser($id){
        $check_user = SocietyUser::where('id',$id)->where("society_id",Auth::id())->get();
        if(count($check_user) <= 0){
            return redirect()->route('user-profile', ['status' => "error",'toastType' => '1','message'=>"Invalid access"]); 
        }

        $user = $check_user[0];
        $user->status = 'active';
        $user->update();

        return redirect()->route('user-profile', ['id'=>$user->id,'status' => "success",'toastType' => '0','message'=>"User account activited successfully"]); 
   }

   public function updateUserPage($id){
        $check_user = SocietyUser::where('id',$id)->where("society_id",Auth::id())->get();
        if(count($check_user) <= 0){
            return redirect()->route('user-profile', ['status' => "error",'toastType' => '1','message'=>"Invalid access"]); 
        }

        $user = $check_user[0];
        return view('pages.user.update-user',['user'=>$user]);
   }

   public function updateUser(Request $request){
        $id = $request->id;
        $email = $request->email;
        $name = $request->name;
        $mobile = $request->mobile;
        $flat_details = $request->flat_details;
        $user_type = $request->user_type;
        $profile_image = $request->profile_image;
        $society_id = Auth::id();

        $validated = $request->validate([
            'email' => 'required|email:rfc,dns|max:100',
            'name' => 'required|max:200',
            'mobile' => 'required|max:20',
            'flat_details' => 'required|max:200',
            'user_type' => 'required',
            'profile_image' => 'image|mimes:jpg,png,jpeg,gif|max:2048',
        ]);

        $check_user = SocietyUser::where('id',$id)->get();
        if(count($check_user) <= 0){
            return redirect()->route('user-list', ['status' => "error",'toastType' => '1','message'=>"Invalid access, try again"]); 
        }

        $user = $check_user[0];
        if($user->email != $email){
            $check_email = SocietyUser::where('email',$email)->get(); 
            if(count($check_email) > 0){
               return redirect()->route('update-user-page', ['id'=>$user->id,'status' => "error",'toastType' => '1','message'=>"User alreay exist with the given email"]); 
            }
            $user->email = $email;
        }

        if($request->has("profile_image") && !empty($profile_image)){
            if(!empty($user->image)){
               $deltePath = public_path('upload/user/'.$user->image);

               if(file_exists($deltePath)){
                 unlink($deltePath);
               }
            }
            
            $file_name = Auth::user()->id."_".time()."_".Str::random(50).'.'.$request->profile_image->extension();
            $request->profile_image->move(public_path('upload/user'), $file_name);
            $user->image = $file_name;
        }

        $user->name = $name;      
        $user->mobile = $mobile;      
        $user->flat_details = $flat_details;      
        $user->user_type = $user_type;    
        $user->update();  

        $mobile_login = AccessToken::where('user_id',$user->id)->where("society_id",Auth::id())->get();
        if(count($mobile_login) > 0){
            $mobile_login = $mobile_login[0];
            $fcmToken = [$mobile_login->fcmToken];
            $data = [
               'notification' => 'true',
               'action' => 'profile_updated_admin',
               'type' => '2',
               'title' => 'Profile Updated',
               'body' => "Your profile has been updated by admin."
            ];

            $notif_data = [
                'title' => 'Profile updated by admin', 
                'body' => 'Your profile has been updated by society admin.', 
                'page' => 'null', 
                'from_type' => '0', 
                'society_id' => Auth::id(), 
                'to_id' => $user->id, 
                'type' => '2', 
                'from_id' => '0', 
            ];

            Helper::sendFCM($data,$fcmToken);
            Helper::sendNotification($notif_data);
        }

        return redirect()->route('update-user-page', ['id'=>$user->id,'status' => "success",'toastType' => '0','message'=>"Profile updated successfully"]); 
   }

   public function sendEmailToUserPage($id){
        $check_user = SocietyUser::select(["name","email","id"])->where('id',$id)->where("society_id",Auth::id())->get();
        if(count($check_user) <= 0){
            return redirect()->route('user-profile', ['status' => "error",'toastType' => '1','message'=>"Invalid access"]); 
        }

        $user = $check_user[0];
        return view('pages.user.send_user_email',['user'=>$user]);
   }

   public function sendUserEmail(Request $request){
        $id = $request->user_id;
        $subject = $request->subject;
        $message = $request->message;

        $validated = $request->validate([
            'subject' => 'required|max:255',
            'message' => 'required|max:5000',
        ]);
        
        $check_user = SocietyUser::select(["name","email","id"])->where('id',$id)->where("society_id",Auth::id())->get();
        if(count($check_user) <= 0){
            return redirect()->route('user-profile', ['status' => "error",'toastType' => '1','message'=>"Invalid access"]); 
        }

        $user = $check_user[0];
        Mail::to($user->email)->send(new SendUserEmail([
            'name' => $user->name,
            'subject'=> $subject,
            "message" => $message
        ]));

        $notif_data = [
            'title' => 'New email from admin', 
            'body' => 'Your society admin has sent you an email on your registered email.', 
            'page' => 'null', 
            'from_type' => '0', 
            'society_id' => Auth::id(), 
            'to_id' => $user->id, 
            'type' => '1', 
            'from_id' => '0', 
        ];

        Helper::sendNotification($notif_data);

        return redirect()->route('user-profile', ['id'=>$user->id,'status' => "success",'toastType' => '0','message'=>"Email sent successfully"]); 
   }

   public function pendingList(){
        $society_user = SocietyUser::where("society_id",Auth::user()->id)->where('status','pending')->get();
        return view('pages.user.pending_user_list',[
            'user_list' => $society_user
        ]);
   }

   public function deletePendingRequest($id){
    $check_user = SocietyUser::where('id',$id)->where("society_id",Auth::id())->get();
    if(count($check_user) <= 0){
        return redirect()->route('pending-user', ['status' => "error",'toastType' => '1','message'=>"Invalid access"]); 
    }
    $user = $check_user[0];
    $user->delete();
    return redirect()->route('pending-user', ['status' => "success",'toastType' => '0','message'=>"User account delete successfully"]); 
   }

   public function acceptAccountRequest($id){
        $check_user = SocietyUser::where('id',$id)->where("society_id",Auth::id())->get();

        if(count($check_user) <= 0){
            return redirect()->route('pending-user', ['status' => "error",'toastType' => '1','message'=>"Invalid access"]); 
        }

        $password = Str::random(8);
        $user = $check_user[0];
        $user->status = 'active';
        $user->password = Hash::make($password);
        $user->update();

        Mail::to($user->email)->send(new AccountRequestAccept([
            'email' => $user->email,
            'password' => $password,
            'name' => $user->name,
            'society_name' => Auth::user()->society_name,
           ]));

        return redirect()->route('pending-user', ['status' => "success",'toastType' => '0','message'=>"Request accepted successfully"]); 
   }

}
