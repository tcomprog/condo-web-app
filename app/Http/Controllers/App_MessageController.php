<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Helper;
use DB;
use App\Helper\ApiResponse;
use Constants;
use App\Models\NotificationModal;
use App\Models\User;
use App\Models\SocietyUser;
use App\Models\MessageModel;
use App\Models\AccessToken;
use App\Models\StremSDKTokenModel;


class App_MessageController extends Controller
{
    use ApiResponse;

    public function getTabScreenListing(Request $request){
        // $user_id = $request->user_id;
        // $society_id = $request->society_id;
        // $activeNotification = $request->activeNotification;
        $activeTab = $request->activeTab;

        $response = [];

        if($activeTab == 1){
            // Delete all alerts
            $response = $this->getAlertList($request);
        }
        elseif($activeTab == 2){
            // Delete all notification
            $response = $this->getNotificationList($request);
        }
        elseif($activeTab == 3){
            // Delete all message
            $response = $this->getMessageTabList($request);
        }
      
        return $this->success($response,'Successfull response',200);
    }

    public function deleteSingleRow(Request $request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $id = $request->id;
        $activeTab = $request->activeTab;

        if($activeTab == 1){
            // Delete all alerts
            NotificationModal::where('to_id',$user_id)->where('society_id',$society_id)
            ->where('type','1')->where('id',$id)->delete();
        }
        elseif($activeTab == 2){
            // Delete all notification
            NotificationModal::where('to_id',$user_id)->where('society_id',$society_id)
            ->where('type','2')->where('id',$id)->delete();
        }
        elseif($activeTab == 3){
            // Delete all message
            DB::select("UPDATE tbl_messages SET isDeleted = CONCAT(`isDeleted`,',',$user_id) WHERE `society_id` = '$society_id' AND (`from_id` = '$user_id' OR `to_id` = '$user_id') AND (`from_id` = '$id' OR `to_id` = '$id');");
        }

        return $this->success([],'Successfull response',200);
    }

    public function deleteAllRows(Request $request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $activeTab = $request->activeTab;

        if($activeTab == 1){
            // Delete all alerts
            NotificationModal::where('to_id',$user_id)->where('society_id',$society_id)->where('type','1')->delete();
        }
        elseif($activeTab == 2){
            // Delete all notification
            NotificationModal::where('to_id',$user_id)->where('society_id',$society_id)->where('type','2')->delete();
        }
        elseif($activeTab == 3){
            // Delete all message
            DB::select("UPDATE tbl_messages SET isDeleted = CONCAT(`isDeleted`,',',$user_id) WHERE `society_id` = '$society_id' AND (`from_id` = '$user_id' OR `to_id` = '$user_id');");
        }

        return $this->success([],'Successfull response',200);
    }

    public function getMessageTabList($request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $response = [];
        $q = "SELECT `to_id`,`from_id`,created_at  FROM `tbl_messages` WHERE `society_id` = '$society_id' AND (`from_id` = '$user_id' OR `to_id` = '$user_id') AND isDeleted NOT LIKE '%,$user_id%' GROUP BY to_id ORDER BY id DESC;";
        $group_message = DB::select($q);

        $to_array = [];

        foreach($group_message as $row){
            $to_id = $row->to_id == $user_id ? $row->from_id : $row->to_id;
            $to_array[] = $to_array;

          if(!in_array($to_id,$to_array)){
            $user = SocietyUser::select('image','name')->where('society_id',$society_id)->where('id',$to_id)->first();
            $imageLink = asset("upload/user/".$user->image);
            if(!file_exists(public_path()."/upload/user/".$user->image) || empty($user->image)){
              $imageLink = asset("images/no_image.jpg");
            }

            $message_count = MessageModel::where('to_id',$user_id)->where("read","0")->where('society_id',$society_id)->where('from_id',$to_id)->count();
        
            if($message_count > 0){
                $last_message = DB::select("SELECT message,created_at  FROM `tbl_messages` WHERE `society_id` = '$society_id' AND `read` = '0' AND (`from_id` = '$user_id' OR `to_id` = '$user_id') ORDER BY id DESC LIMIT 1;
                ");
            }
            else{
                $last_message = DB::select("SELECT message,created_at  FROM `tbl_messages` WHERE `society_id` = '$society_id' AND (`from_id` = '$user_id' OR `to_id` = '$user_id') ORDER BY id DESC LIMIT 1;
                ");
            }

            $last_message = $last_message[0];

            $response[] = [
             'id' => $to_id,
             'title' => $user->name,
             'image' => $imageLink,
             'from_id' => $to_id,
             'unread' => $message_count,
             'body' => $last_message->message,
             'date' => date('d/m/Y',strtotime($last_message->created_at))
            ];

            $to_array[] = $to_id;
          }
        }
        
        return $response;
    }

    public function getAlertList($request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;

        $alertsArray = [];
        $alerts = NotificationModal::where('to_id',$user_id)->select('id','title','body','status','created_at')
        ->where('society_id',$society_id)->where('type','1')->orderBy('id','desc')->get();

        foreach($alerts as $row){
           $alertsArray[] = [
             'id' => $row->id,
             'title' => $row->title,
             'body' => $row->body,
             'status' => $row->status,
             'date' => date('d/m/Y',strtotime($row->created_at)),
           ];
        }
        
        NotificationModal::where('to_id',$user_id)->where('society_id',$society_id)
        ->where('type','1')->where('status','0')->update(['status'=>'1']);

        return $alertsArray;
    }

    public function getNotificationList($request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $activeNotification = $request->activeNotification;

        $society_data = User::select('profile_image')->where('id',$society_id)->first();

        $notifsArray = [];
        $notif = NotificationModal::where('to_id',$user_id)->select('id','title','body','status','created_at','page','page_payload','from_type','from_id')
        ->where('society_id',$society_id)->where('type','2');

        if($activeNotification == 1){
          $notif->orderBy('id','desc');
        }
        elseif($activeNotification == 2){
            $notif->where('status','1')->orderBy('id','desc');
        }
        elseif($activeNotification == 3){
            $notif->where('status','0')->orderBy('id','desc');
        }
        elseif($activeNotification == 4){
            $notif->orderBy('id','asc');
        }


        $notif = $notif->get();

        foreach($notif as $row){

            if($row->from_type == 0){
                $imageLink = asset("upload/admin/".$society_data->profile_image);
                if(!file_exists(public_path()."/upload/admin/".$society_data->profile_image) || empty($society_data->profile_image)){
                    $imageLink = asset("images/no_image.jpg");
                }
            }else{
                if($row->from_id > 0){
                   $user = SocietyUser::select('image')->where('id',$row->from_id)->first();
                   $imageLink = asset("upload/user/".$user->image);
                   if(!file_exists(public_path()."/upload/user/".$user->image) || empty($user->image)){
                     $imageLink = asset("images/no_image.jpg");
                   }
                }
                else{
                    $imageLink = asset("images/no_image.jpg");
                }
            }
            
           $notifsArray[] = [
             'id' => $row->id,
             'title' => $row->title,
             'body' => $row->body,
             'status' => $row->status,
             'page' => $row->page,
             'from_id' => $row->from_id,
             'page_payload' => $row->page_payload,
             'date' => date('d/m/Y',strtotime($row->created_at)),
             'image' => $imageLink
           ];
        }
        
        NotificationModal::where('to_id',$user_id)->where('society_id',$society_id)
        ->where('type','2')->where('status','0')->update(['status'=>'1']);

        return $notifsArray;
    }

    public function sendMessage(Request $request) {
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $msg_type = $request->msg_type;
        $to_id = $request->to_id;

        if($msg_type == 0){
         // Text Message
          return $this->sendTextMessage($request);
        }
        elseif($msg_type == 1){
          // Text & file message

        }
        elseif($msg_type == 2){
          // Multiple file 

        }
        elseif($msg_type == 3){
          // Voice message

        }
        elseif($msg_type == 4){
          // Audio call  

        }
        elseif($msg_type == 5){
          // Video call

        }
        elseif($msg_type == 6){
          // Multiple Images 
          
        }

    }

    public function sendTextMessage($request){
       $user_id = $request->user_id;
       $society_id = $request->society_id;
       $msg_type = $request->msg_type;
       $to_id = $request->to_id;
       $message = $request->message;

       $msg = new MessageModel();
       $msg->message = $message;
       $msg->msg_type = $msg_type;
       $msg->from_id = $user_id;
       $msg->society_id = $society_id;
       $msg->to_id = $to_id;
       $msg->save();

       $mobile_login = AccessToken::where('user_id',$to_id)->where("society_id",$society_id)->get();
       if(count($mobile_login) > 0){
        $society_user = SocietyUser::select('name')->where("id",$user_id)->where('society_id',$society_id)->first();
        $mobile_login = $mobile_login[0];
        $fcmToken = [$mobile_login->fcmToken];
        $data = [
           'notification' => 'true',
           'action' => 'message_received',
           'title' => "New message",
           'body' => 'You have received new message from '.$society_user->name,
           'type' => '4',
           'data' => [
             'from_id' => $user_id,
             'msg_type' => $msg_type,
             'message_id' => $msg->id
           ]
        ];
        Helper::sendFCM($data,$fcmToken);
      } 

       return $this->success(['id'=>$msg->id],'Success');
    }


  public function submitFileMessage(Request $request){
      $user_id = $request->user_id;
      $society_id = $request->society_id;
      $message = $request->message;
      $msg_type = $request->msg_type;
      $to_id = $request->to_id;
      $call_minutes = $request->call_minutes;
      $attachment = [];

      $subFolder = date("m_Y");

      $folder = public_path('upload/message');
      if(!file_exists($folder)){
        mkdir($folder);
      } 

      $folder .= '/'.$subFolder;
      if(!file_exists($folder)){
          mkdir($folder);
      }
    
    if($request->has("frmFiles")){
      $files = $request->file('frmFiles');
      foreach($files as $item){
        $orgName = $item->getClientOriginalName();
        $extension = $item->extension();
        $fileName = "from_".$user_id."_to_".$to_id.'_society_'.$society_id.'_'.time().'_'.Helper::randomString(40).'_.'.$extension;
        $item->move($folder, $fileName);
        $attachment[$orgName] = $subFolder.'/'.$fileName;
      }
    }

    $fldAttachment = json_encode($attachment);

    $msg = new MessageModel();
    $msg->message = $message;
    $msg->msg_type = $msg_type;
    $msg->from_id = $user_id;
    $msg->society_id = $society_id;
    $msg->to_id = $to_id;
    $msg->attachment = $fldAttachment;
    $msg->call_minutes = $call_minutes;
    $msg->save();

    // type => [photo,file,video]

    $msg = MessageModel::where('id',$msg->id)->first();

    $user_attachment = [];
    $msg_attach = json_decode($msg->attachment,true);
    foreach($msg_attach as $name => $path){
      $extension = explode('.',$name)[1];
      $type = $this->getFileType(trim($extension));
      $user_attachment[] = [
        'id' => $msg->id."-1",
        'name' => $name,
        'path' => asset("upload/message/".$path),
        'extension' => $extension,
        'type' => $type
      ];
    }
    
    $response = [
      'id' => $msg->id,
      'msg_type' => $msg->msg_type,
      'read' => $msg->read,
      'message' => $msg->message,
      'call_minutes' => $msg->call_minutes,
      'from_id' => $msg->from_id,
      'to_id' => $msg->to_id,
      'attachment' => $user_attachment,
      'date' => date('d/m/Y',strtotime($msg->created_at)),
      'temp_id' => 0,
      'own_msg' => 0,
      'message_status' => '1',
    ];

    return $this->success($response);

  }

  public function getFileType($extension) {
    $images_extension = ['jpg','jpeg','png','gif'];
    $video_extension = ['mp4','flv','avi','3gp','mkv'];

    if(in_array($extension, $images_extension)){
      return 'photo';
    }elseif(in_array($extension, $video_extension)){
      return 'video';
    }else{
      return 'file';
    }
  }

    public function getMessageDetailsListing(Request $request){
      $user_id = $request->user_id;
      $society_id = $request->society_id;
      $showLoading = $request->showLoading;
      $from_id = $request->from_id;
      $response = [];

      $q = "SELECT * FROM `tbl_messages` WHERE `society_id` = '$society_id' AND ((`from_id` = '$user_id' OR `to_id` = '$user_id') AND (`from_id` = '$from_id' OR `to_id` = '$from_id')) AND isDeleted NOT LIKE '%,$user_id%' ORDER BY id asc;";
      $messages = DB::select($q);

      $i = 0;
   
      foreach($messages as $row){
        $attachment = [];
        $own_msg = $user_id == $row->from_id ? 0 : 1;

        if(!empty($row->attachment)){
           $attach = json_decode($row->attachment,true);
           foreach($attach as $fname => $fpath){
            $extension = explode('.',$fname)[1];
            $type = $this->getFileType(trim($extension));

             $attachment[] = [
               'id' => $row->id."-".$i,
               'name' => $fname,
               'path' => asset("upload/message/".$fpath),
               'extension' => $extension,
               'type' => $type
             ];

             $i++;
           }
        }

        $response[] = [
          'id' => $row->id,
          'msg_type' => $row->msg_type,
          'read' => $row->read,
          'message' => $row->message,
          'call_minutes' => $row->call_minutes,
          'from_id' => $row->from_id,
          'to_id' => $row->to_id,
          'attachment' => $attachment,
          'date' => date('d/m/Y',strtotime($row->created_at)),
          'temp_id' => 0,
          'own_msg' => $own_msg,
          'message_status' => '1',
        ];
      }
      
      if(count($messages) > 0) {
        MessageModel::where('society_id',$society_id)->where('from_id',$from_id)->where('to_id',$user_id)->update(['read'=>'1']);
      }

      $from_user = [];
      if($showLoading == 1){
         $user = SocietyUser::select('name','image','status','email')->where("id",$from_id)->where('society_id',$society_id)->first();
         
         $image = asset("upload/user/".$user->image);
         if(!file_exists(public_path()."/upload/user/".$user->image)){
           $image = asset("images/no_image.jpg");
         }

         $stream_token = "null";
         $stream_data = StremSDKTokenModel::where("user_id",$from_id)->where("society_id",$society_id)->get();

         if(count($stream_data) > 0){
          $stream_token = $stream_data[0]->token;
         }

         $from_user = [
            'name' => $user->name,
            'image' => $image,
            'status' => $user->status,
            'email' => $user->email,
            'stream_token' => $stream_token
         ];
      }

      return $this->success(['messages'=>$response,'user'=>$from_user],'Success');
    }
}
