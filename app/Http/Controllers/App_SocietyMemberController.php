<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\ApiResponse;
use App\Models\SocietyUser;
use App\Models\StremSDKTokenModel;
use App\Models\AccessToken;

use Constants;
use Auth;
use DB;
use Helper;

class App_SocietyMemberController extends Controller
{
   use ApiResponse;

   public function membersList(Request $request){
      $user_id = $request->user_id;
      $society_id = $request->society_id;

      $member = SocietyUser::select(['id','email','name','image'])->where('id','!=',$user_id)
      ->where("society_id",$society_id)->where("status","active")->orderBy('id', 'desc')->get();

      $memberList = [];
      foreach($member as $row){

        $image = asset("upload/user/".$row->image);
        if(!file_exists(public_path()."/upload/user/".$row->image) || empty($row->image)){
            $image = asset("images/no_image.jpg");
          }

        array_push($memberList,[
          'id' => $row->id,
          'email' => $row->email,
          'name' => $row->name,
          'image' => $image,
        ]);
      }

      return $this->success($memberList,'Successfully loaded member list');
   } 

   public function getMemberProfileData(Request $request){
     $member_id = $request->member_id;
     $user_id = $request->user_id;
     $society_id = $request->society_id;

    $member = SocietyUser::select(['id','email','name','image'])->where('id',$member_id)
    ->where("society_id",$society_id)->where("status","active")->get();

    if(count($member) > 0){
       $user = $member[0];

       $image = asset("upload/user/".$user->image);
       if(!file_exists(public_path()."/upload/user/".$user->image) || empty($user->image)){
           $image = asset("images/no_image.jpg");
        }

       $stream_token = "null";

       $stream_data = StremSDKTokenModel::where("user_id",$member_id)->where("society_id",$society_id)->get();
       if(count($stream_data) > 0){
        $stream_token = $stream_data[0]->token;
       }

       $response = [
        'id' => $user->id,
        'email' => $user->email,
        'name' => $user->name,
        'image' => $image,
        'stream_token' => $stream_token
       ];

       return $this->success($response,"Successfull",200);
    }
    else{
      return $this->error([],"User data not found",200);
    }
   }

   public function getMemberLocationList(Request $request){
    $user_id = $request->user_id;
    $society_id = $request->society_id;

    $user_locations = DB::table('user_token')
            ->join('society_user', 'society_user.id', '=', 'user_token.user_id')
            ->select('society_user.name','society_user.flat_details','society_user.id','user_token.latitude','user_token.longitude')
            ->where('user_token.user_id','!=',$user_id)->where('user_token.society_id',$society_id)
            ->get();

    $response = [];
    foreach($user_locations as $row){
      $response[] = [
        'name' => $row->name,
        'flat_details' => $row->flat_details,
        'id' => $row->id,
        'latitude' => (float) $row->latitude,
        'longitude' => (float) $row->longitude,
      ];
    }
   
    return $this->success($response,'Successfull response');
    
   }
}
