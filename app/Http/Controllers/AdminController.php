<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Auth;
use App\Models\User;
use App\Models\PrivacyPolicy;
use App\Models\VotingModel;
use App\Models\SocietyUser;
use App\Models\PetitionModel;
use App\Models\NotificationModal;
use App\Models\RequestModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    public function index(): View{
        $society_id = Auth::id();
        $active = SocietyUser::select('id')->where('society_id',$society_id)->where('status','active')->count();
        $inactive = SocietyUser::select('id')->where('society_id',$society_id)->where('status','inactive')->count();
        $pending = SocietyUser::select('id')->where('society_id',$society_id)->where('status','pending')->count();

        $pending_request = RequestModel::select('id')->where('society_id',$society_id)->where('status','0')->count();
        $approved_request = RequestModel::select('id')->where('society_id',$society_id)->where('status','1')->count();
        $rejected_request = RequestModel::select('id')->where('society_id',$society_id)->where('status','2')->count();

        $pending_voting = VotingModel::select('id')->where('society_id',$society_id)->where('status','0')->count();
        $active_voting = VotingModel::select('id')->where('society_id',$society_id)->where('status','1')->count();
        $complete_voting = VotingModel::select('id')->where('society_id',$society_id)->where('status','2')->count();
        $reject_voting = VotingModel::select('id')->where('society_id',$society_id)->where('status','3')->count();

        $pending_petition = PetitionModel::select('id')->where('society_id',$society_id)->where('status','0')->count();
        $active_petition = PetitionModel::select('id')->where('society_id',$society_id)->where('status','1')->count();
        $reject_petition = PetitionModel::select('id')->where('society_id',$society_id)->where('status','2')->count();

        return view("pages.dashboard",[
          'active' => $active,
          'inactive' => $inactive,
          'pending' => $pending,
          'requests' => [$pending_request,$approved_request,$rejected_request],
          'voting_grp' => [$pending_voting,$active_voting,$complete_voting,$reject_voting],
          'p_pending' => $pending_petition,
          'p_active' => $active_petition,
          'p_reject' => $reject_petition,
        ]);
    }
    
    public function adminProfile(): View{
        return view('pages.admin.admin_profile');
    }

    public function updateProfile(Request $request){
      //  $email = $request->email;
        $name = $request->name;
        $society_name = $request->society_name;
        $phone_no = $request->phone_no;
        $address = $request->address;
        $profile_image = $request->profile_image;
        $latitude = $request->latitude;
        $longitude = $request->longitude;

        $user = User::find(Auth::user()->id);

        if($request->has("profile_image") && !empty($profile_image)){
            $this->validate($request, [
                'profile_image' => 'image|mimes:jpg,png,jpeg,gif|max:2048',
            ]);

            if(!empty($user->profile_image)){
               $deltePath = public_path('upload/admin/'.$user->profile_image);

               if(file_exists($deltePath)){
                 unlink($deltePath);
               }
            }
            
            $file_name = Auth::user()->id."_".time()."_".Str::random(50).'.'.$request->profile_image->extension();
            $request->profile_image->move(public_path('upload/admin'), $file_name);
            $user->profile_image = $file_name;
        }

        $user->name = $name;
        $user->society_name = $society_name;
        $user->phone_no = $phone_no;
        $user->address = $address;     
        $user->latitude = $latitude;     
        $user->longitude = $longitude;     
        $user->update();

        return back();
    }

    public function changeAdminPassword(Request $request){
        $oldPassword = $request->oldPassword;
        $newPassword = $request->newPassword;
        $rePassword = $request->rePassword;

        $user = User::find(Auth::user()->id);

        if($newPassword != $rePassword){
            return response()->json(['status'=>'not_match','message'=>"Password does not match"]);
        }

        if($newPassword == $oldPassword){
            return response()->json(['status'=>'no_changes','message'=>"New password should not be same as old password"]);
        }

        if(!Hash::check($oldPassword, $user->password)){
           return response()->json(['status'=>'invalid_old','message'=>"Invalid old password, try again later"]);
        }

        $user->password = Hash::make($newPassword);
        $user->update();

        return response()->json(['status'=>'success','message'=>"Password changed successfully"]);
    }

    public function societyPolicy(){
        $society_id = Auth::id();
        $check = PrivacyPolicy::where('society_id',$society_id)->get();
        $policy = count($check) > 0 ? $check[0]->privacy : '';
        return view('pages.admin.society-policy',['policy'=>$policy]);
    }

    public function savePrivacyPolicy(Request $request){
        $privacyPolicy = $request->ckeditorData;
        $society_id = Auth::id();
        $check = PrivacyPolicy::where('society_id',$society_id)->get();

        if(count($check) > 0){
            $privacyTBL = $check[0];
            $privacyTBL->privacy = $privacyPolicy;
            $privacyTBL->update();
        }
        else{
            $privacyTBL = new PrivacyPolicy();
            $privacyTBL->privacy = $privacyPolicy;
            $privacyTBL->society_id = $society_id;
            $privacyTBL->save();
        }

        return redirect()->route('society-policy', ['status' => "success",'toastType' => '0','message'=>"Action performed successfully!"]); 

    }


    public function sendAlertPage(){
        return view('pages.admin.send_alert');
    }

    public function saveAlert(Request $request){
        $title = $request->title;
        $body = $request->body;

        $member = SocietyUser::select('id')->where("society_id",Auth::id())->where('status','active')->get();

        foreach($member as $row){
           $notif = new NotificationModal();
           $notif->title = $title;
           $notif->body = $body;
           $notif->status = '0';
           $notif->type = '1';
           $notif->page = 'null';
           $notif->page_payload = 'null';
           $notif->from_type = '0';
           $notif->from_id = '0';
           $notif->society_id = Auth::id();
           $notif->to_id = $row->id;
           $notif->save();
        }

        return redirect()->route('send-alert-page', ['status' => "success",'toastType' => '0','message'=>"Alert sent successfully!"]); 
    }
}
