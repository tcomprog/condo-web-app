<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PetitionModel;

use App\Helper\ApiResponse;
use Constants;
use Auth;
use DB;
use Helper;

class App_PetitionController extends Controller
{
    use ApiResponse;

    public function submitPetition(Request $request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $title = $request->title;
        $type = $request->type;
        $desc = $request->desc;
        $images_array = [];

        $subFolder = date("m_Y");
        
        $folder = public_path('upload/petition');
        if(!file_exists($folder)){
            mkdir($folder);
        } 

        $folder .= '/'.$subFolder;
        if(!file_exists($folder)){
            mkdir($folder);
        } 
        

        if($request->has('images') && count($request->images) > 0){
           $images = $request->images;
           foreach($images as $file){
             $fileName = $user_id.'_'.$society_id.'_'.time().'_'.Helper::randomString(40).'_'. $file->getClientOriginalName();
             $dbName = $subFolder.'/'.$fileName;
             $file->move($folder, $fileName);
             $images_array[] = $dbName;
           }
        }

        $images_array = count($images_array) > 0 ? json_encode($images_array) : '';
        $petition = new PetitionModel();
        $petition->status = '0';
        $petition->title = $title;
        $petition->type = $type;
        $petition->desc = $desc;
        $petition->user_id = $user_id;
        $petition->society_id = $society_id;
        $petition->images = $images_array;
        $petition->adminComment = '';
        $petition->save();

        return $this->success([],'Success');
    }
    
    public function getPetitionList(Request $request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;

        $request_data = PetitionModel::where('user_id',$user_id)->where('society_id',$society_id)
        ->select('id','title','status','type','created_at')->orderBy('id','desc')->get();

        $request_array = [];
        foreach($request_data as $row){
          $request_array[] = [
            "id" => $row->id,
            "title" => $row->title,
            "type" => $row->type,
            "status" => $row->status,
            "date" => date("d/m/Y",strtotime($row->created_at)),
          ];
        }
 
        return $this->success($request_array,"");
    }

    public function getPetitionDetails(Request $request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $request_id = $request->request_id;

        $request_data = PetitionModel::where('user_id',$user_id)->where('society_id',$society_id)->where('id',$request_id)->get();
      if(count($request_data) > 0){
        $request_data = $request_data[0];

         return $this->success([
          "id" => $request_data->id,
          "title" => $request_data->title,
          "type" => $request_data->type,
          "status" => $request_data->status,
          "desc" => $request_data->desc,
          "adminComment" => $request_data->adminComment,
          "images" => $request_data->images,
          "date" => date("d/m/Y",strtotime($request_data->created_at)),
         ],"");
      }
      else{
        return $this->error([],Constants::constant("UNAUTHORIZED_ACCESS"),200);
      }

    }
}
