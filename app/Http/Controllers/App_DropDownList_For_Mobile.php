<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class App_DropDownList_For_Mobile extends Controller
{
    public function getSocietyList(){
       $societyList = User::select("id","society_name")->where('status','active')->get();
       $response = [];
     
       foreach($societyList as $row){
          $item = ['label'=>$row->society_name,'value'=>$row->id,'id'=>$row->id]; 
          array_push($response,$item);
       }

       return response()->json(['data'=>$response]);
    }
}
