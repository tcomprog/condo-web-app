<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use App\Models\SocietyUser;
use App\Models\User;
use App\Helper\ApiResponse;
use Constants;
use Auth;
use DB;
use Helper;

class App_ProfileController extends Controller
{
    use ApiResponse;

    public function updatePassword(Request $request){
        
        $validated = Validator::make($request->all(), [
            'old_pasword' => 'required',
            'new_password' => 'required',
            'user_id' => 'required',
            'society_id' => 'required',
           ]);
   
          if($validated->fails()){
             return $this->error([],Constants::constant("INVALID_DATA"),200);
          }

        $old_pasword = $request->old_pasword;
        $new_password = $request->new_password;
        $user_id = $request->user_id;
        $society_id = $request->society_id;

        $user = SocietyUser::where("id",$user_id)->where("society_id",$society_id)->get();
        $new_password = Hash::make($new_password);

        if(count($user) > 0){
           $user = $user[0];
           if(Hash::check($old_pasword,$user->password)){
              $user->password = $new_password;
              $user->update();
              return $this->success([],Constants::constant("PASSWORD_CHANGED"),200);

           }else{
             return $this->error([],Constants::constant("INVALID_OLD_PASSWORD"),200);
           }
        }
        else{
            return $this->error([],Constants::constant("INVALID_USER"),200);
        }
    }


    public function getProfileData(Request $request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $user = SocietyUser::where("id",$user_id)->where("society_id",$society_id)->get();

        if(count($user) > 0){
           $user = $user[0];

           $image = asset("upload/user/".$user->image);
           if(!file_exists(public_path()."/upload/user/".$user->image)){
               $image = 'null';
           }

           return $this->success([
            'id' => $user->id,
            'email' => $user->email,
            'name' => $user->name,
            'mobile' => $user->mobile,
            'flat_details' => $user->flat_details,
            'user_type' => $user->user_type,
            'status' => $user->status,
            'image' => $image,
            'society_id' => $user->society_id,
            'created_at' => date('d-m-Y',strtotime($user->created_at)),
           ],Constants::constant("SUCCESS_ACTION")); 
        }
        else{
            return $this->error([],Constants::constant("INVALID_USER"),200);
        }
    }

    public function updateProflieData(Request $request){
       $name = $request->name;
       $mobile = $request->mobile;
       $user_id = $request->user_id;
       $society_id = $request->society_id;

       $user = SocietyUser::where("id",$user_id)->where("society_id",$society_id)->get();
       if(count($user) > 0){
          $user = $user[0];
          $user->name = $name;
          $user->mobile = $mobile;
          $user->update();

          $image = asset("upload/user/".$user->image);
          if(!file_exists(public_path()."/upload/user/".$user->image)){
              $image = 'null';
          }

          $user_data = [
            'id' => $user->id,
            'email' => $user->email,
            'name' => $user->name,
            'mobile' => $user->mobile,
            'flat_details' => $user->flat_details,
            'user_type' => $user->user_type,
            'status' => $user->status,
            'image' => $image,
            'society_id' => $user->society_id,
            'created_at' => date('d-m-Y',strtotime($user->created_at)),
          ];

            $society_user = User::select('id','society_name','email','address','phone_no')->where('id',$society_id)->get()[0]; 
            $society_user = [
                'id' => $society_user["id"],
                'society_name' => $society_user["society_name"],
                'email' => $society_user["email"],
                'address' => $society_user["address"],
                'phone_no' => $society_user["phone_no"],
            ];
        

          return $this->success([
            'user' => $user_data,
            'society' => $society_user,
           ],Constants::constant("SUCCESS_ACTION")); 
       }
       else{
          return $this->error([],Constants::constant("INVALID_USER"),200);
       }
    }

    public function updateProfileImage(Request $request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $image = $request->image;
        $user = SocietyUser::where("id",$user_id)->where("society_id",$society_id)->get();
        if(count($user) > 0){
            $user = $user[0];

            if(file_exists(public_path()."/upload/user/".$user->image)){
               unlink(public_path().'/upload/user/'.$user->image);
            }

            $image_name = $society_id."_".$user_id."_".time()."_".Helper::randomString(40).".jpeg";
            $path = public_path().'/upload/user/' . $image_name;
            $file = base64_decode($image);
            $success = file_put_contents($path, $file);
            $user->image = $image_name;
            $user->update();

            $newPath = asset("upload/user/".$user->image);
            return $this->success(['imagePath'=>$newPath],"Success");
        }
        else{
           return $this->error([],Constants::constant("INVALID_USER"),200);
        }
    }
}
