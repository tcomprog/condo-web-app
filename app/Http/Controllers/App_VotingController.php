<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\VotingModel;
use App\Models\VotingOptionsModel;
use App\Models\MemberVotingModel;
use App\Helper\ApiResponse;
use Constants;
use Auth;
use Helper;

class App_VotingController extends Controller
{
    use ApiResponse;

    public function getVotingList(Request $request){
        $orderBy = $request->orderBy; // 1- All, 2 - New, 3 - Old
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        
        $response = [];

      if($orderBy == 1){
        // All Section
        $response = $this->getAllVoting($request);
      }
      elseif($orderBy == 2){
        // New Section
        $response = $this->getNewSectionVoting($request);
      }
      elseif($orderBy == 3){
        // Old Section
        $response = $this->getOlderVotionSection($request);
      }

        return $this->success($response,"");
    }

    public function completePoll(Request $request) {
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $voting_id = $request->voting_id;

        VotingModel::where('id',$voting_id)->where("society_id",$society_id)->where("user_id",$user_id)->update(['status'=>2]);
        $data = $this->getSingleVoteData($request);
        return $this->success($data,"success");
    }

    public function getVotingDetails(Request $request){
        $data = $this->getSingleVoteData($request);
        return $this->success($data,"success");
    }

    public function getSingleVoteData($request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $voting_id = $request->voting_id;

        $voting = VotingModel::where('id',$voting_id)->where("society_id",$society_id)->where("user_id",$user_id)->get();
        
        if(count($voting) > 0){
           $voting = $voting[0];
           $date = date('d-',strtotime($voting->created_at)).Helper::getMonths(date('m',strtotime($voting->created_at))).date('-Y',strtotime($voting->created_at));
      
           $options = [];
           $total_votes = 0;
           $op_result = VotingOptionsModel::where('voting_id',$voting_id)->where('society_id',$society_id)->get();
           foreach($op_result as $row){
            $votes = MemberVotingModel::select('id')->where('society_id',$society_id)->where('voting_id',$voting_id)->where('option_id',$row->id)->count();
            $total_votes += $votes;
            $options[] = [
                'title' => $row->title,
                'votes' => $votes
            ];
           }

           $data = [
             'voting_id' => $voting->id,
             'question' => $voting->question,
             'status' => $voting->status,
             'date' => $date,
             'options' => $options,
             'total_votes' => $total_votes
           ];

           return $data;
        }
        else{
            return $this->error([],Constants::constant("UNAUTHORIZED_ACCESS"),200);
        }
    }

    public function getOwnVotingList(Request $request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $voting = VotingModel::select('id','question','created_at','status')->where("society_id",$society_id)->where("user_id",$user_id)->get();
        $vote_array =  [];
        foreach($voting as $row){
            $date = date('d-',strtotime($row->created_at)).Helper::getMonths(date('m',strtotime($row->created_at))).date('-Y',strtotime($row->created_at));

            $vote_array[] = [
                'voting_id' => $row->id,
                'question' => $row->question,
                'status' => $row->status,
                'date' => $date,
            ];
        }

        return $this->success($vote_array,"Success");
    }


    public function addVotingPoll(Request $request){
        $question = $request->question;
        $poll_question = $request->options;
        $user_id = $request->user_id;
        $society_id = $request->society_id;

        $voting = new VotingModel();
        $voting->question = $question;
        $voting->society_id = $society_id;
        $voting->user_id = $user_id;
        $voting->status = '0';
        $voting->addedBy = '1';
        $voting->save();

        foreach($poll_question as $op){
            $option = new VotingOptionsModel();
            $option->voting_id = $voting->id;
            $option->title = $op;
            $option->society_id = $society_id;
            $option->save();
        }

        return $this->success([],"Success");
    }


    public function submitVoting(Request $request){
        $voting_id = $request->voting_id;
        $option_id = $request->option_id;
        $user_id = $request->user_id;
        $society_id = $request->society_id;

        $vote = new MemberVotingModel();
        $vote->voting_id = $voting_id;
        $vote->option_id = $option_id;
        $vote->user_id = $user_id;
        $vote->society_id = $society_id;
        $vote->save();
        return $this->success([],'success');
    }

    public function getVotedIds($request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;

        $vote = MemberVotingModel::select('voting_id')->where('user_id',$user_id)->where('society_id',$society_id)->get();
        $ids_arr = [];
        foreach($vote as $op){
            $ids_arr[] = $op->voting_id;
        }
      return $ids_arr;
    }

    public function getSelectedOptionsIds($request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;

        $vote = MemberVotingModel::select('voting_id','option_id')->where('user_id',$user_id)->where('society_id',$society_id)->get();
        $ids_arr = [];

        foreach($vote as $op){
            $ids_arr[$op->voting_id] = $op->option_id;
        }
      return $ids_arr;
    }

    public function getOlderVotionSection($request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        $response = [];

        $voted_ids = $this->getVotedIds($request); 
        $select_option_arr = $this->getSelectedOptionsIds($request); 
        $new_voting = VotingModel::select('id','question','created_at')->whereIn('id',$voted_ids)->where("society_id",$society_id)->where("user_id","<>",$user_id)->get();
        
        
        foreach($new_voting as $row){
            $item = [];
            $date = date('d-',strtotime($row->created_at)).Helper::getMonths(date('m',strtotime($row->created_at))).date('-Y',strtotime($row->created_at));
            $item['voting_id'] = $row->id;
            $item['question'] = $row->question;
            $item['date'] = $date;
            $item['old_row'] = 1;
            $select_op = isset($select_option_arr[$row->id]) ? $select_option_arr[$row->id] : 0;
            $item['select_op_id'] = $select_op;
           
            $options_array = [];
            $voting_option = VotingOptionsModel::select('id','title')->where('voting_id',$row->id)->where("society_id",$society_id)->get();
            if(count($voting_option) > 0){
                foreach($voting_option as $op){
                  if($op->id == $select_op){
                    $options_array[] = [
                        'id' => $op->id,
                        'label' => $op->title,
                        'value' => $op->title,
                        'disabled' => true,
                        'selected' => true,
                     ];
                  }else{
                    $options_array[] = [
                        'id' => $op->id,
                        'label' => $op->title,
                        'value' => $op->title,
                        'disabled' => true,
                     ];
                  }
                 
                }

                $item['options'] = $options_array;
            }

            $response[] = [
                'title' => $date,
                'data' => [$item]
            ];
        }

        return $response;
    }

    public function getNewSectionVoting($request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;

        $response = [];
        $recent_arr = [];

        $voted_ids = $this->getVotedIds($request); 
        $new_voting = VotingModel::select('id','question','created_at')->whereNotIn('id',$voted_ids)->where("society_id",$society_id)->where('status','1')->where("user_id","<>",$user_id)->orderBy('id','desc')->limit(10)->get();

        foreach($new_voting as $row){
            $item = [];
            $date = date('d-',strtotime($row->created_at)).Helper::getMonths(date('m',strtotime($row->created_at))).date('-Y',strtotime($row->created_at));
            $item['voting_id'] = $row->id;
            $item['question'] = $row->question;
            $item['date'] = $date;
           
            $options_array = [];
            $voting_option = VotingOptionsModel::select('id','title')->where('voting_id',$row->id)->where("society_id",$society_id)->get();
            if(count($voting_option) > 0){
                foreach($voting_option as $op){
                   $options_array[] = [
                      'id' => $op->id,
                      'label' => $op->title,
                      'value' => $op->title,
                   ];
                }

                $item['options'] = $options_array;
                $recent_arr[] = $item;
            }
        }

        $response[] = [
            'title' => "Latest Voting Poll",
            'data' => $recent_arr
        ];

        return $response;
    }

    public function getAllVoting($request){
        $user_id = $request->user_id;
        $society_id = $request->society_id;
        
        $response = [];
        $recent_arr = [];
        $older_arr = [];

        $voted_ids = $this->getVotedIds($request); 

        $from = date('Y-m-d', strtotime('-3 days'));
        $to = date('Y-m-d', strtotime('+1 days'));

        $recent_voting = VotingModel::select('id','question','created_at')->whereNotIn('id',$voted_ids)->whereBetween('created_at', [$from, $to])->where("society_id",$society_id)->where('status','1')->where("user_id","<>",$user_id)->get();

        foreach($recent_voting as $row){
            $item = [];
            $date = date('d-',strtotime($row->created_at)).Helper::getMonths(date('m',strtotime($row->created_at))).date('-Y',strtotime($row->created_at));
            $item['voting_id'] = $row->id;
            $item['question'] = $row->question;
            $item['date'] = $date;
           
            $options_array = [];
            $voting_option = VotingOptionsModel::select('id','title')->where('voting_id',$row->id)->where("society_id",$society_id)->get();
            if(count($voting_option) > 0){
                foreach($voting_option as $op){
                   $options_array[] = [
                      'id' => $op->id,
                      'label' => $op->title,
                      'value' => $op->title,
                   ];
                }

                $item['options'] = $options_array;
                $recent_arr[] = $item;
            }
        }

        $older_voting = VotingModel::select('id','question','created_at')->whereNotIn('id',$voted_ids)->whereNotBetween('created_at', [$from, $to])->where("society_id",$society_id)->where('status','1')->where("user_id","<>",$user_id)->get();
       
        foreach($older_voting as $row){
            $item = [];
            $date = date('d-',strtotime($row->created_at)).Helper::getMonths(date('m',strtotime($row->created_at))).date('-Y',strtotime($row->created_at));
            $item['voting_id'] = $row->id;
            $item['question'] = $row->question;
            $item['date'] = $date;
           
            $options_array = [];
            $voting_option = VotingOptionsModel::select('id','title')->where('voting_id',$row->id)->where("society_id",$society_id)->get();
            if(count($voting_option) > 0){
                foreach($voting_option as $op){
                   $options_array[] = [
                      'id' => $op->id,
                      'label' => $op->title,
                      'value' => $op->title,
                   ];
                }

                $item['options'] = $options_array;
                $older_arr[] = $item;
            }
        }

        $response[] = [
            'title' => "Recent Voting Poll",
            'data' => $recent_arr
        ];

        $response[] = [
            'title' => "Older Voting Poll",
            'data' => $older_arr
        ];

        return $response;
    }
}
