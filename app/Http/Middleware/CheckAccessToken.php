<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Constants;
use App\Helper\ApiResponse;
use App\Models\AccessToken;

class CheckAccessToken
{
    use ApiResponse;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $access_token = $request->bearerToken();
        if(empty($access_token)){
            return $this->error([],Constants::constant("UNAUTHORIZED_ACCESS"),401);
        }
        else{
           $check_token = AccessToken::where("token",$access_token)->get();
           if(count($check_token) > 0){
              $check_token = $check_token[0];
              $request->merge(["user_id"=>$check_token->user_id,"society_id"=>$check_token->society_id,'token'=>$check_token->token]);
              return $next($request);
           }
           else{
              return $this->error([],Constants::constant("UNAUTHORIZED_ACCESS"),401);
           }
        }
    }
}
