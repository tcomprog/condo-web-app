<?php
namespace App\Helper;

class Constant {
   public static function constant(string $key):string{
      return self::values()[$key];
   }

   public static function values(){
      return [
         "APP_NAME" => "Condo App",
         "USER_EXIST" => "User already exist with the provided email address.",
         "ACCOUNT_CREATED" => "Your account has been created. Please verify your email in order to access the system.",
         "DEVELOPER_ERROR" => "You are not verified developer and you don`t have access",
         "INVALID_LOGIN_DATA" => "Invalid data provided for login access",
         "INVALID_USERNAME" => "Invalid username",
         "INVALID_PASSWORD" => "Invalid password",
         "LOGIN_SUCCESS" => "Login successfully",
         "UNAUTHORIZED_ACCESS" => "Unauthorized access",
         "INACTIVE_ACCOUNT" => "Your account is inactive, please contact the society owner to activate your account",
         "MISSING_FIELDS" => "Please fill the form correctly",
         "USER_REGISTER_SCCUESS" => "Your request has been sent to society admin successfully.",
         "INACTIVE_ACCOUNT" => "Your account is inactive, please contact admin to active your account",
         "LOGOUT_MESSAGE" => "Logout successfully!",
         "INVALID_EMAIL" => "Invalid email address",
         "OPT_SENT" => "OTP has been sent to your registered email address",
         "INVALID_OTP" => "Invalid OTP",
         "OTP_EXPIRED" => "You are using an expired OTP.",
         "PASSWORD_CHANGED" => "Your password has been updated successfully.",
         "INVALID_DATA" => "Invalid data provided",
         "INVALID_USER" => "User data does not found",
         "INVALID_OLD_PASSWORD" => "Invalid old password",
         "SUCCESS_ACTION" => "Action performed successfully",
      ];
   }
}

?>