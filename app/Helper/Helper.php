<?php
namespace App\Helper;
use App\Models\NotificationModal;

class Helper {
    public static function randomString($length = 10){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function sendFCM($data = [],$fcmToken){

        // $data = [
        //     'notification' => 'false',
        //     'type' => 'number', 1 - Alert, 2 - Notification, 3 - Message
        //     'action' => 'profile_updated_admin',
        //     'data' => [];
        //  ];

        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array (
            'registration_ids' => $fcmToken,
            'data' => $data
        );

       $fields = json_encode ( $fields );
       $headers = array (
        'Authorization: key=' . env('FIREBASE_ACCESS_KEY'),
        'Content-Type: application/json'
        );

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

        $result = curl_exec ( $ch );
        // echo $result;
        curl_close ( $ch );
    }

    public static function sendNotification($data){      
        $notif = new NotificationModal();
        foreach($data as $key => $value){
            $notif->$key = $value;
        }

        $notif->save();
    }


    public static function getMonths($index){
       $months = [
        '01' => 'January',
        '02' => 'February',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'Jun',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December',
       ];
       return $months[$index];
    }

    public static function getDays($index){
       $days = [
        '1' => 'Monday',
        '2' => 'Tuesday',
        '3' => 'Wednesday',
        '4' => 'Thursday',
        '5' => 'Friday',
        '6' => 'Saturday',
        '7' => 'Sunday',
       ];
       return $months[$days];
    }
}
?>