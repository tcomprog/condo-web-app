<?php
namespace App\Helper;

class Encryption{

    public static function encrypt(string $message,string $key = 'null'):string{
        if($key == 'null'){
          $key = env('ENCRYPTION_KEY');
        }

        $cipher     = 'AES-256-CBC';
        $options    = OPENSSL_RAW_DATA;
        $hash_algo  = 'sha256';
        $sha2len    = 32;
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($message, $cipher, $key, $options, $iv);
        $hmac = hash_hmac($hash_algo, $ciphertext_raw, $key, true);
        return $iv.$hmac.$ciphertext_raw;
    }

    public static function decrypt(string $message,string $key = 'null'):string{
        if($key == 'null'){
            $key = env('ENCRYPTION_KEY');
        }

        $cipher     = 'AES-256-CBC';
        $options    = OPENSSL_RAW_DATA;
        $hash_algo  = 'sha256';
        $sha2len    = 32;
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv = substr($message, 0, $ivlen);
        $hmac = substr($message, $ivlen, $sha2len);
        $ciphertext_raw = substr($message, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options, $iv);
        $calcmac = hash_hmac($hash_algo, $ciphertext_raw, $key, true);
        if(function_exists('hash_equals')) {
            if (hash_equals($hmac, $calcmac)) return $original_plaintext;
        } else {
            if ($this->hash_equals_custom($hmac, $calcmac)) return $original_plaintext;
        }
    }
}
?>