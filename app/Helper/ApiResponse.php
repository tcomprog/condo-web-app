<?php
namespace App\Helper;

trait ApiResponse {

   protected function success($data=[],$message=null,$code=200){
     return response()->json(
        [
            "status" => "success",
            "data"=>$data,
            'message'=>$message,
            'code' => $code
        ],200);
   }

   protected function error($data=[],$message=null,$code){
    return response()->json(
       [
           "status" => "error",
           "data"=>$data,
           'message'=>$message,
           'code' => $code
       ]
       ,200);
  }
}

?>