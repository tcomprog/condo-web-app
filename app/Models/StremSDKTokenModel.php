<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StremSDKTokenModel extends Model
{
    use HasFactory;
    protected $table = 'streamsdk_token';
}
