<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccessToken extends Model
{
    use HasFactory;
    protected $table = 'user_token';
    protected $fillable = ['user_id','token','society_id','platform','fcmToken','latitude','longitude'];

}
