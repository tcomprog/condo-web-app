<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VotingOptionsModel extends Model
{
    use HasFactory;
    protected $table = 'voting_option';
}
