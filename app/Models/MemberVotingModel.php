<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemberVotingModel extends Model
{
    use HasFactory;
    protected $table = 'votes_of_member';
}
