<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PetitionModel extends Model
{
    use HasFactory;
    protected $table = 'petitions';
    protected $fillable = ['status'];
}
